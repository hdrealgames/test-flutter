import 'package:firebase_analytics/observer.dart';
/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

/// Awake app
import 'package:screen/screen.dart';

/// Localization
import 'package:intl/date_symbol_data_local.dart';
import 'package:devicelocale/devicelocale.dart';
//import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:flutter_cupertino_localizations/flutter_cupertino_localizations.dart';
//import 'package:intl/intl.dart';

/// Bloc
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/utils/simple_bloc_delegate.dart';

/// Auth
import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/login/login.dart';

/// Analytics
import 'package:firebase_analytics/firebase_analytics.dart';

/// Pages
import 'package:Getinshoot/pages/home/homePage.dart';
import 'package:Getinshoot/pages/home/splash_screen.dart';

/* ********************************************************* */

UserRepository repositoryUser;
String currentLocale = "fr";
String previousDeepLinkCode = "";
String lastNotificationId;
final storage = new FlutterSecureStorage();

final FirebaseAnalytics analytics   = FirebaseAnalytics();
final FirebaseAnalyticsObserver observer = FirebaseAnalyticsObserver(analytics: analytics);

/* ********************************************************* */

/*
Future<void> _sendAnalyticsEvent(name, parameters) async {
  await MyApp.analytics.logEvent(name: 'awesome_event', parameters: <String, dynamic>{
    'int': 42, // not required?
  });
} */

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  
  // Initialisation
  //Intl.defaultLocale = 'fr_FR';
  initializeDateFormatting("fr_FR", null);

  // App Initialisation
  initApp();

  // Orientation
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  // Blocs
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final UserRepository userRepository = UserRepository();

  // Awake app
  Screen.keepOn(true);

  // Launch application
  runApp(
    BlocProvider(
      builder: (context) => AuthenticationBloc(userRepository: userRepository)
        ..dispatch(AppStarted()),
      child: App(userRepository: userRepository),
    ),
  );
}

/* ********************************************************* */

initApp() async {
  try {
    String storedLanguage = await storage.read(key: "storedLanguage");
    if(storedLanguage != null){
      currentLocale = storedLanguage;
    } else {
      currentLocale = (await Devicelocale.currentLocale).split("_")[0];
    }
      debugPrint(currentLocale);

    lastNotificationId = await storage.read(key: "lastNotificationId");
  } on PlatformException {
    print("Error obtaining current locale");
  }

  

  try {
    //final FirebaseInAppMessaging fiam = FirebaseInAppMessaging();
  } catch(e){
    debugPrint(e.toString());
  }
}

/* ********************************************************* */

class App extends StatelessWidget {
  final UserRepository _userRepository;

  App({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    initApp();

    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.dark) // Or Brightness.dark
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorObservers: [ observer ],
      theme: ThemeData(accentColor: Colors.white, canvasColor: AppColor.blue, brightness: Brightness.light),
      home: BlocBuilder(
        bloc: BlocProvider.of<AuthenticationBloc>(context),
        builder: (BuildContext context, AuthenticationState state) {
          // Loading screen
          if (state is Uninitialized) {
            return SplashScreen();
          }

          if (state is Unauthenticated) {
            return LoginScreen(userRepository: _userRepository);
          }

          if (state is Authenticated) {
            repositoryUser = _userRepository;
            return HomePage();
          }

          return SplashScreen();
        },
      ),
    );
  }
}
