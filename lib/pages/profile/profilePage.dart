/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/utils/app_colors.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:flutter/services.dart';
//import 'package:connectivity/connectivity.dart';

import 'package:Getinshoot/widgets/headerWidget.dart';
//import 'package:Getinshoot/widgets/menuWidget.dart';

/// Auth
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';

/// Database
//import 'package:cloud_firestore/cloud_firestore.dart';

/// Localization
//import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:flutter_cupertino_localizations/flutter_cupertino_localizations.dart';
//import 'package:intl/intl.dart';
//import 'package:intl/date_symbol_data_local.dart';

/// Local settings
//import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> with WidgetsBindingObserver {
  //GlobalKey _keyWidgetHomeMenu = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: AppColor.blue),
      child: SafeArea(
        child: Scaffold(
          appBar: HeaderWidget.get(context),
          //drawer: MenuWidget.get(context),
          body: Container(
            decoration: BoxDecoration(color: AppColor.blue),
            child: Container(
              // TODO : Profile Form
            )
          )
        )
      )
    );
  }
}
