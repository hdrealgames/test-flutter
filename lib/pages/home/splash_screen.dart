import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:Getinshoot/utils/app_colors.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
        color: AppColor.blue,
        ),
        child: SafeArea(
          child: Center(
            child: Padding(padding: EdgeInsets.all(75),
              child: AspectRatio(
                aspectRatio: 1,
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Image.asset('assets/logo/logo.png', width: 170,),
                    ),
                    Container(
                      //child: Text("Connecting to the server ...", style: TextStyle(color: Colors.white)),
                    )
                  ],
                )
              ),
            )
          )
        )
      ),
    );
  }
}
