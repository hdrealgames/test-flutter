import 'dart:io';

import 'package:Getinshoot/widgets/joinInput.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/main.dart';
//import 'package:flutter/services.dart';
//import 'package:connectivity/connectivity.dart';

/// Colors
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';

/// Navigation
import 'package:Getinshoot/widgets/headerWidgetHome.dart';

/// Pages / Widgets
import 'package:Getinshoot/pages/events/widgets/eventWidget.dart';
import 'package:Getinshoot/pages/events/eventDetailPage.dart';
import 'package:Getinshoot/pages/guidelines/guidelinesPage.dart';
import 'package:Getinshoot/pages/contactPage.dart';

/// Launch browser
import 'package:url_launcher/url_launcher.dart';

import '../../pushNotifications.dart';

/// Local settings
//import 'package:shared_preferences/shared_preferences.dart';

/* *************************************************************** */

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver {
  //GlobalKey _keyWidgetHomeMenu = GlobalKey();

  Map<int, Widget> eventsTabs = <int, Widget>{
    0: Padding(
        padding: EdgeInsets.symmetric(vertical: 3, horizontal: 12),
        child: Text(currentLocale == 'fr' ? 'en cours' : 'active',
            style: TextStyle(
                color: Colors.white, fontFamily: "Muli", fontSize: 12))),
    1: Padding(
        padding: EdgeInsets.symmetric(vertical: 3, horizontal: 12),
        child: Text(currentLocale == 'fr' ? 'terminés' : 'stopped',
            style: TextStyle(
                color: Colors.white, fontFamily: "Muli", fontSize: 12))),
  };

  int sharedValue = 0;
  final FirebaseMessaging _fMessaging = FirebaseMessaging();

  bool newNotification = false;
  String lastNotifId;

  void setCurrentScreen() async {
    await analytics.setCurrentScreen(
        screenName: "Home Page", screenClassOverride: 'homePage');
  }

  @override
  void initState() {
    this.setCurrentScreen();
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    if (Platform.isIOS) {
      _fMessaging.requestNotificationPermissions();
      saveDeviceToken(repositoryUser.userId, _fMessaging)
          .whenComplete(() {})
          .catchError((e) => print(e));
    } else {
      saveDeviceToken(repositoryUser.userId, _fMessaging)
          .whenComplete(() {})
          .catchError((e) => print(e));
    }
    Firestore.instance
        .collection("notifications")
        .where("recipients", arrayContains: repositoryUser.userId)
        .limit(1)
        .orderBy("createdDate", descending: true)
        .snapshots()
        .listen((snapshot) {
      if (snapshot.documents.length > 0) {
        print("notif id in db");
        print(lastNotificationId);
        var myLastNotificationId = snapshot.documents[0].documentID;
        setState(() {
          lastNotifId = myLastNotificationId;
        });
        if (lastNotificationId != myLastNotificationId) {
          setState(() {
            newNotification = true;
            lastNotifId = myLastNotificationId;
          });
        }
      }
    });
  }

  Future<void> notificationClickCallback() async {
    setState(() {
      newNotification = false;
    });
    if (this.lastNotifId != lastNotificationId) {
      await storage.write(key: "lastNotificationId", value: this.lastNotifId);
      lastNotificationId = this.lastNotifId;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.detached:
        break;
      default:
        break;
    }
    /*
    setState(() {
      _lastLifecycleState = state;
    });
   */
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderWidgetHome.get(context, notificationClickCallback,
                    unreadNotifications: this.newNotification),
                //drawer: MenuWidget.get(context),
                body: Container(
                    decoration: BoxDecoration(color: AppColor.blue),
                    child: Container(
                        child: StreamBuilder<QuerySnapshot>(
                            stream: Firestore.instance
                                .collection('invitations')
                                .where('userid',
                                    isEqualTo: repositoryUser.userId)
                                .orderBy('created', descending: true)
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> qSnapshot) {
                              if (qSnapshot.hasError)
                                return Text('Error: ${qSnapshot.error}');

                              return Container(
                                child: SingleChildScrollView(
                                    child: Wrap(children: <Widget>[
                                  JoinInput(),
                                  qSnapshot != null &&
                                          qSnapshot.data != null &&
                                          qSnapshot.data.documents != null &&
                                          qSnapshot.data.documents.length > 0
                                      ? Padding(
                                          padding: EdgeInsets.only(
                                              left: 0,
                                              right: 0,
                                              top: 20,
                                              bottom: 2),
                                          child: Row(children: <Widget>[
                                            Expanded(
                                                flex: 1,
                                                child: Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 12,
                                                        right: 5,
                                                        top: 0,
                                                        bottom: 5),
                                                    child: Text(
                                                      currentLocale == "fr"
                                                          ? "participer"
                                                          : "participate",
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontFamily: "Poppins",
                                                          fontSize: 24),
                                                    ))),
                                            CupertinoSegmentedControl<int>(
                                              borderColor: AppColor.red,
                                              pressedColor:
                                                  AppColor.red.withOpacity(0.5),
                                              selectedColor: AppColor.red,
                                              unselectedColor: AppColor.blue,
                                              children: eventsTabs,
                                              onValueChanged: (int val) {
                                                setState(() {
                                                  sharedValue = val;
                                                });
                                              },
                                              groupValue: sharedValue,
                                            ),
                                          ]))
                                      : Container(),
                                  qSnapshot != null &&
                                          qSnapshot.data != null &&
                                          qSnapshot.data.documents != null &&
                                          qSnapshot.data.documents.length > 0
                                      ? Container(
                                          child: Container(
                                              height: 242,
                                              child: ListView.builder(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                itemCount: qSnapshot
                                                    .data.documents.length,
                                                itemBuilder: (context, index) {
                                                  return StreamBuilder<
                                                          DocumentSnapshot>(
                                                      stream: Firestore.instance
                                                          .collection('events')
                                                          .document(qSnapshot
                                                                  .data
                                                                  .documents[
                                                              index]['eventid'])
                                                          .snapshots(),
                                                      builder: (BuildContext
                                                              context,
                                                          AsyncSnapshot<
                                                                  DocumentSnapshot>
                                                              snapshot) {
                                                        if (snapshot.hasError)
                                                          return Text(
                                                              'Error: ${snapshot.error}');
                                                        switch (snapshot.data) {
                                                          case null:
                                                            return Container();
                                                          default:
                                                            try {
                                                              var stopped =
                                                                  false;
                                                              if (snapshot.data[
                                                                      'stopped'] ??
                                                                  true) {
                                                                stopped = true;
                                                              }

                                                              if (snapshot.data[
                                                                      'draft'] ??
                                                                  true) {
                                                                return Container();
                                                              } else if ((stopped &&
                                                                      sharedValue ==
                                                                          0) ||
                                                                  (!stopped &&
                                                                      sharedValue ==
                                                                          1)) {
                                                                return Container();
                                                              } else {
                                                                return Padding(
                                                                    padding: EdgeInsets.symmetric(
                                                                        horizontal:
                                                                            12),
                                                                    child: GestureDetector(
                                                                        child: EventWidget(
                                                                          document:
                                                                              snapshot.data,
                                                                          documentId: snapshot
                                                                              .data
                                                                              .documentID,
                                                                          name: snapshot.data['name'] ??
                                                                              '',
                                                                          host: snapshot.data['host'] ??
                                                                              '',
                                                                          guests:
                                                                              snapshot.data['guests'] ?? 0,
                                                                          image:
                                                                              snapshot.data['image'] ?? null,
                                                                          imageURL:
                                                                              snapshot.data['imageURL'] ?? null,
                                                                          thumbnailURL:
                                                                              snapshot.data['imageURL'] ?? null,
                                                                          joined:
                                                                              qSnapshot.data.documents[index]['joined'] ?? false,
                                                                          participated:
                                                                              qSnapshot.data.documents[index]['participated'] ?? false,
                                                                          invitationId:
                                                                              qSnapshot.data.documents[index].documentID ?? null,
                                                                          mediaorientation:
                                                                              snapshot.data['mediaorientation'] ?? 'both',
                                                                          mediaduration:
                                                                              snapshot.data['mediaduration'] ?? 0,
                                                                          mediabyguest:
                                                                              snapshot.data['mediabyguest'] ?? 0,
                                                                        ),
                                                                        onTap: () {
                                                                          try {
                                                                            analytics.logEvent(
                                                                              name: 'open_project',
                                                                              parameters: <String, dynamic>{
                                                                                'eventid': snapshot.data.documentID,
                                                                                'eventname': snapshot.data['name'] ?? '',
                                                                              },
                                                                            );
                                                                          } catch (e) {}

                                                                          Navigator.of(context)
                                                                              .push(
                                                                            MaterialPageRoute(builder:
                                                                                (context) {
                                                                              return EventDetailPage(
                                                                                document: snapshot.data,
                                                                                documentId: snapshot.data.documentID,
                                                                                name: snapshot.data['name'] ?? '',
                                                                                host: snapshot.data['host'] ?? '',
                                                                                guests: snapshot.data['guests'] ?? 0,
                                                                                image: snapshot.data['image'] ?? null,
                                                                                imageURL: snapshot.data['imageURL'] ?? null,
                                                                                thumbnailURL: snapshot.data['imageURL'] ?? null,
                                                                                joined: qSnapshot.data.documents[index]['joined'] ?? false,
                                                                                participated: qSnapshot.data.documents[index]['participated'] ?? false,
                                                                                invitationId: qSnapshot.data.documents[index].documentID ?? null,
                                                                                mediaOrientation: snapshot.data['mediaorientation'] ?? 'both',
                                                                                mediaDuration: snapshot.data['mediaduration'] ?? 0,
                                                                                mediaByGuest: snapshot.data['mediabyguest'] ?? 0,
                                                                              );
                                                                            }),
                                                                          );
                                                                        }));
                                                              }
                                                            } catch (e) {
                                                              return Container();
                                                            }
                                                        }
                                                      });
                                                },
                                              )))
                                      : Container(),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 15,
                                          right: 15,
                                          top: 5,
                                          bottom: 10),
                                      child: AspectRatio(
                                          aspectRatio: 16 / 7,
                                          child: GestureDetector(
                                            onTap: () {
                                              Navigator.of(context)
                                                  .pushReplacement(
                                                MaterialPageRoute(
                                                    builder: (context) {
                                                  return GuidelinesPage();
                                                }),
                                              );
                                            },
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: BoxDecoration(
                                                  color: AppColor.grey
                                                      .withOpacity(0.25),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/home-guidelines.jpg'),
                                                      fit: BoxFit.cover)),
                                              child: Stack(children: <Widget>[
                                                Positioned(
                                                  bottom: 14,
                                                  left: 18,
                                                  child: Text(
                                                    currentLocale == "fr"
                                                        ? "conseils de tournage"
                                                        : "guidelines",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: 'Poppins',
                                                      fontSize: 20,
                                                      shadows: [
                                                        Shadow(
                                                          blurRadius: 8.0,
                                                          color: AppColor.blue
                                                              .withOpacity(0.5),
                                                          offset:
                                                              Offset(2.0, 2.0),
                                                        ),
                                                        Shadow(
                                                          blurRadius: 4.0,
                                                          color: AppColor.blue
                                                              .withOpacity(
                                                                  0.25),
                                                          offset:
                                                              Offset(1.0, 1.0),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ]),
                                            ),
                                          ))),
                                  Padding(
                                      padding: EdgeInsets.only(
                                          left: 15,
                                          right: 15,
                                          top: 10,
                                          bottom: 10),
                                      child: AspectRatio(
                                          aspectRatio: 16 / 4,
                                          child: GestureDetector(
                                            onTap: () async {
                                              Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) {
                                                  return ContactPage();
                                                }),
                                              );
                                            },
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              decoration: BoxDecoration(
                                                  color: AppColor.grey
                                                      .withOpacity(0.25),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          15.0),
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/home-contact.jpg'),
                                                      fit: BoxFit.cover)),
                                              child: Stack(children: <Widget>[
                                                Positioned(
                                                  bottom: 14,
                                                  left: 18,
                                                  child: Text(
                                                    currentLocale == "fr"
                                                        ? "a propos de getinshoot"
                                                        : "about getinshoot",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontFamily: 'Poppins',
                                                      fontSize: 20,
                                                      shadows: [
                                                        Shadow(
                                                          blurRadius: 8.0,
                                                          color: AppColor.blue
                                                              .withOpacity(0.5),
                                                          offset:
                                                              Offset(2.0, 2.0),
                                                        ),
                                                        Shadow(
                                                          blurRadius: 4.0,
                                                          color: AppColor.blue
                                                              .withOpacity(
                                                                  0.25),
                                                          offset:
                                                              Offset(1.0, 1.0),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ]),
                                            ),
                                          ))),
                                  Container(height: 5),
                                ])),
                              );
                            }))))));
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 2, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 5.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color.withOpacity(0.4)),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
