import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

/// Widgets

//import 'package:provider/provider.dart';

/// Camera
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';
import 'package:native_device_orientation/native_device_orientation.dart';
import 'package:video_player/video_player.dart';
//import 'package:Getinshoot/pages/events/events_bloc/camera_bloc.dart';
//import 'package:exif/exif.dart';

/// Analysis
//import 'package:firebase_ml_vision/firebase_ml_vision.dart';

/// Status and modes
enum CameraOrientation {
  landscape,
  portrait,
  both,
}
enum CameraMode {
  picture,
  video,
}
enum CameraStatus {
  previewing,
  recording,
  validating,
}

class CameraPage extends StatefulWidget {
  /// Camera orientation (landscape, portrait, both)
  final CameraOrientation orientation;

  /// Camera mode (picture, video)
  final CameraMode mode;

  /// Video duration in seconds, -1 is infinite
  final int videoDuration;

  /// Text shown as a bubble on camera preview
  final String tooltip;

  /// Information about the project and the user
  final String eventId;
  final String invitationId;
  final String userID;

  CameraPage({
    @required this.mode,
    this.orientation = CameraOrientation.both,
    this.videoDuration = -1,
    this.tooltip,
    this.eventId,
    this.invitationId,
    this.userID,
  }) : super();

  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  /// Camera status
  CameraStatus _status = CameraStatus.previewing;

  /// Camera controller
  CameraController _cameraController;

  ///try zoom
  double scale = 1.0;

  /// Video controller
  VideoPlayerController _videoController;

  /// Screen orientation state
  int _rotation = 0;

  /// The record time
  int _recordTime = 0;

  /// The time remaining for video record
  int _timeRemaining = -1;

  /// This timer is used to control video time duration
  Timer _timer;

  /// Message
  String _tooltipMsg;

  /// Store picture or video output file
  String _output;
  String _filename;

  /// Informations about the file
  double length = 0; // Size of the file

  List<CameraDescription> cameras = [];
  int selectedCameraIndex;

  /*
   * Orientation managers
   */

  void _unlockOrientation() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
  }

  void _lockOrientation(CameraOrientation orientation) {
    if (orientation == CameraOrientation.landscape) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
      ]);
    } else if (orientation == CameraOrientation.portrait) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    } else {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeRight,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    }
  }

  void _lockOrientationFromRotation(int rotation) {
    // switch (rotation) {
    //   case 0:
    //     SystemChrome.setPreferredOrientations([
    //       DeviceOrientation.portraitUp,
    //     ]);
    //     break;
    //   case 1:
    //     SystemChrome.setPreferredOrientations([
    //       DeviceOrientation.landscapeRight,
    //     ]);
    //     break;
    //   case -1:
    //     SystemChrome.setPreferredOrientations([
    //       DeviceOrientation.landscapeLeft,
    //     ]);
    //     break;
    //   case 2:
    //     SystemChrome.setPreferredOrientations([
    //       DeviceOrientation.portraitDown,
    //     ]);
    //     break;
    //   default:
    //     break;
    // }
  }

  /*
   * Camera & files managers
   */

  Future<String> _getPath(CameraMode mode) async {
    String timestamp = DateTime.now().millisecondsSinceEpoch.toString();
    Directory extDir = await getApplicationDocumentsDirectory();

    String dirPath =
        '${extDir.path}/'; //+ (mode == CameraMode.picture ? 'photos' : 'videos');
    //await Directory(dirPath).create(recursive: true);
    _filename = widget.eventId +
        '-' +
        widget.userID +
        '-' +
        '$timestamp.' +
        (mode == CameraMode.picture ? 'jpg' : 'mp4');
    debugPrint("FILENAME : " + _filename);
    debugPrint("DIR PATH :" + dirPath);
    return '$dirPath/' + _filename;
  }

  void _launchRecord() async {
    if (_isRecording()) {
      // TODO
      return;
    }

    _lockOrientationFromRotation(_rotation);

    var timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (_isInfinite()) {
        setState(() {
          _recordTime++;
        });
      } else {
        if (_timeRemaining <= 0) {
          _stopRecord();
        } else {
          setState(() {
            _recordTime++;
            _timeRemaining--;
          });
        }
      }
    });

    try {
      var path = await _getPath(widget.mode);
      await _cameraController.startVideoRecording(path);

      setState(() {
        _status = CameraStatus.recording;
        //_tooltipMsg = _status.toString();

        _timer = timer;
        _output = path;
      });
    } catch (ex) {
      debugPrint(ex.toString());
      // TODO
    }
  }

  void _stopRecord() async {
    debugPrint("STOP RECORD");
    if (!_isRecording()) {
      // TODO
      return;
    }

    debugPrint("STOP VIDEO RECORDING");
    try {
      await _cameraController.stopVideoRecording();
    } catch (e) {
      debugPrint(e.toString());
    }

    debugPrint("STOP RECORD : dispose videocontroller");
    if (_videoController != null) {
      await _videoController.dispose();
      _videoController = null;
    }

    debugPrint("STOP RECORD : Videoplayer init");

    _videoController = VideoPlayerController.file(File(_output));
    await _videoController.initialize();

    debugPrint("CAMERASTATUS VALIDATING");
    setState(() {
      _status = CameraStatus.validating;
      //_tooltipMsg = _status.toString();
      debugPrint("STOP RECORD : stopping timer");
      _timer?.cancel();
      _timer = null;
      _recordTime = 0;
      if (!_isInfinite()) _timeRemaining = 0;
      debugPrint("STOP RECORD : replay video");
      _videoController.play();
    });
  }

  void _takePicture() async {
    if (_cameraController.value.isTakingPicture) {
      // TODO
      return;
    }

    try {
      var path = await _getPath(widget.mode);
      await _cameraController.takePicture(path);

      // TODO : retrieve : image/video size, orientation and resolution

      // Analysis
      /*
      FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(File(path));
      ImageLabeler labeler = FirebaseVision.instance.imageLabeler();
      List<ImageLabel> labels = await labeler.processImage(visionImage);

      debugPrint("=============================================");

      for (ImageLabel label in labels) {
        String text = label.text;
        String entityId = label.entityId;
        double confidence = label.confidence;

        debugPrint("LABEL : "+text+" - "+entityId+" - "+confidence.toString());
        debugPrint("=============================================");
      }
      */
      debugPrint("=============================================");

      setState(() {
        _status = CameraStatus.validating;
        //_tooltipMsg = _status.toString();
        _output = path;
      });
    } catch (ex) {
      debugPrint(ex.toString());
      // TODO
    }
  }

  void _onConfirm(BuildContext context) {
    // GO TO TAG SELECTOR PAGE -- OLD
    // Navigator.of(context).push(
    //   MaterialPageRoute(
    //     builder: (context) {
    //       //var cameraBloc = Provider.of<CameraBloc>(context);
    //       //int requestID = cameraBloc.upload(_output);
    //       _unlockOrientation();
    //       return TagSelector(1, _filename, _output,  widget.mode.toString(), widget.eventId, widget.invitationId, widget.userID);
    //     },
    //   ),
    // );
    _unlockOrientation();
    Navigator.pop(context, _output);
  }

  /*
   * States checkers
   */

  bool _isLandscape() {
    return _rotation == 1 || _rotation == -1;
  }

  bool _isInfinite() {
    return _timeRemaining == -1;
  }

  bool _isRecording() {
    return _cameraController != null &&
        _cameraController.value.isRecordingVideo;
  }

  /*
   * Interface builders
   */

  Widget _backButton(BuildContext context) {
    return _status == CameraStatus.validating
        ? Ink(
            decoration: BoxDecoration(
              color: Color.fromARGB(77, 0, 0, 0),
              shape: BoxShape.circle,
            ),
            child: InkWell(
              borderRadius: BorderRadius.circular(90.0),
              onTap: () {
                if (_status != CameraStatus.validating) {
                  Navigator.of(context).pop();
                } else {
                  setState(() {
                    _status = CameraStatus.previewing;
                    //_tooltipMsg = _status.toString();
                    _timeRemaining = widget.videoDuration;
                    _output = null;
                  });

                  _lockOrientation(widget.orientation);
                }
              },
              child: Padding(
                padding: EdgeInsets.all(7),
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          )
        : null;
  }

  Widget _clock() {
    if (widget.mode == CameraMode.picture) return Container();

    DateTime dt = DateTime(
        0, 0, 0, 0, 0, _isInfinite() ? _recordTime : _timeRemaining, 0);

    return Center(
      child: Text(
        DateFormat('ms').format(dt),
        style: TextStyle(
          color: Colors.white.withOpacity(0.95),
          fontSize: 20,
          fontFamily: "Muli",
          shadows: [
            Shadow(
              blurRadius: 3,
              color: Colors.black.withOpacity(0.5),
            ),
          ],
        ),
      ),
    );
  }

  Widget _tooltip() {
    if (_tooltipMsg == null || _tooltipMsg == "") return Container();

    var text = Text(
      _tooltipMsg,
      style: TextStyle(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        shadows: [
          Shadow(
            blurRadius: 14,
            color: Colors.black,
          ),
        ],
      ),
    );

    if (_isLandscape()) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Container(
              child: text,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          ),
        ],
      );
    } else {
      return text;
    }
  }

  Widget _mainButton() {
    return Ink(
      width: 75,
      height: 75,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
            color: Colors.white.withOpacity(_isRecording() ? 0.0 : 0.9),
            width: 14.0),
      ),
      child: GestureDetector(
        onTap: () {
          if (widget.mode == CameraMode.picture) {
            _takePicture();
          } else {
            if (_isRecording()) {
              _stopRecord();
            } else {
              _launchRecord();
            }
          }
        },
        child: Transform.scale(
          scale: _isRecording() ? 0.7 : 1,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(_isRecording() ? 15 : 90),
            child: Container(
                color: (widget.mode == CameraMode.picture
                    ? AppColor.green
                    : AppColor.red)),
          ),
        ),
      ),
    );
  }

  Widget _confirmButton(BuildContext context) {
    return Opacity(
        opacity: 0.9,
        child: RaisedButton(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: Text('CONFIRM'),
          onPressed: () {
            _onConfirm(context);
          },
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ));
  }

  Widget _body(BuildContext context) {
    List<Widget> stack = [
      RotatedBox(
        quarterTurns: (() {
          if (_status == CameraStatus.validating) {
            if (widget.mode == CameraMode.picture)
              return 0;
            else if (_rotation == 1 && !Platform.isIOS)
              return 2;
            else if (_rotation == 1 && Platform.isIOS)
              return 1;
            else
              return 0;
          }
          return _rotation;
        })(),
        child: (() {
          if (_status == CameraStatus.validating) {
            if (widget.mode == CameraMode.picture) {
              return Center(child: Image.file(File(_output)));
            } else {
              return VideoPlayer(_videoController);
            }
          } else {
            return Stack(
              children: <Widget>[
                CameraPreview(_cameraController),
                Center(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Image.asset("assets/images/camera-overlay.png"),
                ))
              ],
            );
          }
        })(),
      ),
    ];

    // Validation
    if (_status != CameraStatus.validating) {
      stack.addAll([
        Align(
          alignment: Alignment.topCenter,
          child: widget.mode == CameraMode.video
              ? Container(
                  width: widget.orientation == CameraOrientation.landscape
                      ? 130
                      : double.infinity,
                  height: 50,
                  child: Material(
                    color: Colors.transparent,
                    child: _clock(),
                  ),
                  color: Color.fromRGBO(22, 16, 50, 0.5),
                )
              : null,
        ),
        Align(
          alignment:
              _isLandscape() ? Alignment.bottomLeft : Alignment.topCenter,
          child: Container(
            margin: _isLandscape()
                ? EdgeInsets.only(
                    bottom: 21,
                    left: 21,
                  )
                : EdgeInsets.only(top: 80),
            child: Material(
              color: Colors.transparent,
              child: _tooltip(),
            ),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: EdgeInsets.only(bottom: 21),
            child: Material(
              color: Colors.transparent,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if (!_isRecording()) {
                        if (_status != CameraStatus.validating) {
                          Navigator.of(context).pop();
                        } else {
                          setState(() {
                            _status = CameraStatus.previewing;
                            //_tooltipMsg = _status.toString();
                            print("TIME REMAINING");
                            print(widget.videoDuration);
                            _timeRemaining = widget.videoDuration;
                            _output = null;
                          });

                          _lockOrientation(widget.orientation);
                        }
                      }
                    },
                    child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: !_isRecording()
                            ? Image.asset(
                                "assets/icons/back-camera.png",
                                width: 30,
                                height: 30,
                              )
                            : null),
                  ),
                  _mainButton(),
                  GestureDetector(
                    onTap: () async {
                      if (!_isRecording()) {
                        await toggleCamera();
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 30.0),
                      child: !_isRecording()
                          ? Image.asset(
                              "assets/icons/reverse-camera.png",
                              width: 30,
                              height: 30,
                            )
                          : null,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ]);
    } else {
      /**
       * Show these widget when validating
       */
      stack.addAll([
        Align(
          alignment: Alignment.bottomRight,
          child: Container(
            margin: EdgeInsets.only(
              right: 30,
              bottom: 30,
            ),
            child: Material(
              color: Colors.transparent,
              child: _confirmButton(context),
            ),
          ),
        )
      ]);
    }

    // Back button
    stack.add(
      Align(
        alignment: Alignment.bottomLeft,
        child: Container(
          margin: EdgeInsets.only(
            left: 30,
            bottom: 30,
          ),
          child: Material(
            color: Colors.transparent,
            child: _backButton(context),
          ),
        ),
      ),
    );

    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: AspectRatio(
          aspectRatio: _isLandscape()
              ? _cameraController.value.previewSize.width /
                  _cameraController.value.previewSize.height
              : _cameraController.value.previewSize.height /
                  _cameraController.value.previewSize.width,
          child: Stack(
            children: stack,
          ),
        ),
      ),
    );
  }

  /* ***************************** */

  /*
   * Native functions
   */

  Future<void> toggleCamera() {
    selectedCameraIndex = selectedCameraIndex == 0 ? 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIndex];
    return initCamera(selectedCamera);
  }

  Future<void> initCamera(CameraDescription selectedCamera) async {
    _cameraController = CameraController(
      selectedCamera,
      ResolutionPreset.veryHigh, // 1920x1080 or 4K
      enableAudio: widget.mode == CameraMode.picture ? false : true,
    );
    await _cameraController.initialize();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _timeRemaining = widget.videoDuration;
    _tooltipMsg = widget.tooltip;

    // Initialize DateTime formatting
    initializeDateFormatting();
    Intl.defaultLocale = 'fr';

    SystemChrome.setEnabledSystemUIOverlays([]);
    _lockOrientation(widget.orientation);

    availableCameras().then((availableCameras) async {
      cameras = [...availableCameras];
      selectedCameraIndex = 0;
      await initCamera(cameras[selectedCameraIndex]);
    });
  }

  @override
  Widget build(BuildContext context) {
    // If the controller isn't ready or initialized
    if (_cameraController == null || !_cameraController.value.isInitialized) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    return NativeDeviceOrientationReader(
      useSensor: true,
      builder: (context) {
        NativeDeviceOrientation orientation =
            NativeDeviceOrientationReader.orientation(context);
        double screenWidth = MediaQuery.of(context).size.width;
        double screenHeight = MediaQuery.of(context).size.height;

        if (screenWidth >= screenHeight) {
          // Landscape
          if (orientation == NativeDeviceOrientation.landscapeLeft) {
            _rotation = -1;
          } else if (orientation == NativeDeviceOrientation.landscapeRight) {
            _rotation = 1;
          }
        } else {
          // Portrait
          if (orientation == NativeDeviceOrientation.portraitUp) {
            _rotation = 0;
          } else if (orientation == NativeDeviceOrientation.portraitDown) {
            _rotation = 2;
          }
        }

        return _body(context);
      },
    );
  }

  @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    _unlockOrientation();

    _cameraController?.dispose();
    _videoController?.dispose();
    _timer?.cancel();
    super.dispose();
  }
}
