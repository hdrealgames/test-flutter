import 'dart:io';

/// Packages
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:vector_math/vector_math_64.dart' as vector;
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:percent_indicator/percent_indicator.dart';

/// Widget
import 'package:Getinshoot/main.dart';
import 'package:Getinshoot/pages/events/widgets/tag.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

/// Provider
//import 'package:provider/provider.dart';
//import 'package:camera_widget/blocs/camera_bloc.dart';

class TagData {
  String name;
  IconData icon;
  bool selected;

  TagData({
    @required this.name,
    @required this.icon,
    @required this.selected,
  });
}

class TagSelector extends StatefulWidget {
  /// The output file, where is stored the media (picture, video)
  final int requestID; // TODO

  // The output file (temp)
  final String _path;
  final String filename;

  /// Information about the project and the user
  final String eventId;
  final String invitationId;
  final String userID;
  final String mode;

  TagSelector(this.requestID, this.filename, this._path, this.mode, this.eventId, this.invitationId, this.userID, {Key key}) : super(key: key);

  _TagSelectorState createState() => _TagSelectorState();
}

class _TagSelectorState extends State<TagSelector> {
  bool custom = false;
  double progress = 0;

  /// ScaffoldKey of the page
  GlobalKey<ScaffoldState> _keyScaffoldTagSelectorPage = GlobalKey<ScaffoldState>();

  /// Tags
  Map<String, TagData> _whoTags = {
    'one_people': TagData(name: 'One People', icon: MdiIcons.account, selected: true,),
    'group': TagData(name: 'Group', icon: MdiIcons.accountGroup, selected: false,),
    'landscape': TagData(name: 'Landscape', icon: Icons.landscape, selected: false,),
  };
  Map<String, TagData> _whatTags = {
    'cocktail': TagData(name: 'Cocktail', icon: MdiIcons.glassCocktail, selected: true,),
    'interview': TagData(name: 'Interview', icon: MdiIcons.microphoneVariant, selected: false,),
    'party': TagData(name: 'Party', icon: MdiIcons.beach, selected: false,),
    'meeting': TagData(name: 'Meeting', icon: MdiIcons.presentation, selected: false,),
    'animation': TagData(name: 'Animation', icon: MdiIcons.ticket, selected: false,),
    'other': TagData(name: 'Other', icon: MdiIcons.dotsHorizontal, selected: false,),
  };

  /// Upload status
  bool uploadProgressing = false;
  bool uploadCompleted = false;
  double uploadProgressBar = 0;
  bool uploadFailed = false;
  bool uploadSaved = false;

  /// Tag status
  bool tagSkipped = false;
  bool tagsSelected = false;

  /// Propose to retry
  bool uploadRetry = true;

  /// Contribution info
  bool contributionSaved = false;
  String contributionID;

  @override
  void initState() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    super.initState();
    this.launchUpload();
  }

  /* ************************************* */

  void launchUpload(){
    //this.uploadSaved = false;
    //this.uploadRetry = true;
    //this.uploadCompleted = false;
    //this.uploadFailed = false;
    //this.uploadProgressing = false;
    //this.progress = 0;
    //this.uploadProgressBar = 0;

    String storagePath = "participations/" + widget.eventId;

    if(widget.mode=='video') storagePath+= "/videos/";
    else storagePath+= "/photos/";

    storagePath += repositoryUser.userId + "/" + widget.filename;

    double length = 0;

    try {
      length = File(widget._path).lengthSync().toDouble();
    } catch (e) {}
    length = ((length / 1024) / ~1014);
    if (length < 0) length = length * -1;

    var datas = {
      'userid': repositoryUser.userId,
      'from': repositoryUser.name,
      'eventid': widget.eventId,
      'type': widget.mode=='video' ? 'video' : 'photo',
      'format': widget.mode=='vdeo' ? 'mp4' : 'jpg',
      'file': widget.filename,
      'length' : 0,
      'created': DateTime.now(),
      'modified': DateTime.now(),
      'fileURL': '',
      //'fileThumbURL': urlThumb,
    };

    if(length != null && length > 0) datas['size'] = length.toStringAsFixed(2);

    final StorageReference storageRef = FirebaseStorage
        .instance.ref()
        .child(storagePath);
    final StorageUploadTask task = storageRef.putFile(File(widget._path));

    if(_keyScaffoldTagSelectorPage!=null && _keyScaffoldTagSelectorPage.currentState!=null) {
      _keyScaffoldTagSelectorPage.currentState
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("Connecting to server ...")],
            ),
          ),
        );
    }

    task.events.listen((event) {
      setState(() {
        this.progress =
            event.snapshot.bytesTransferred.toDouble() /
                event.snapshot.totalByteCount.toDouble();

        if(this.progress<0) this.progress = 0;
        if(this.progress>100) this.progress = 100;
      });

      /**
      _keyScaffoldTagSelectorPage.currentState
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            duration: Duration(seconds: 240),
            content: Row(
              mainAxisAlignment: MainAxisAlignment
                  .spaceBetween,
              children: [
                _progress == 0
                    ? Text(".... preparing file to transfert ....")
                    : Text(".... upload " +
                        (_progress * 100)
                            .ceil()
                            .toString() + "%" +
                        " ....."),
              ],
            ),
          ),
        );
        **/
    }).onError((error) {
      if(_keyScaffoldTagSelectorPage!=null && _keyScaffoldTagSelectorPage.currentState!=null) {
        _keyScaffoldTagSelectorPage.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment
                    .spaceBetween,
                children: [
                  Text('An unexpected error occured'),
                ],
              ),
            ),
          );
      }
    });

    task.onComplete.then((snapshot) {
      snapshot.ref.getDownloadURL().then((url) {
        setState(() {
          this.progress = 100;
          this.uploadCompleted = true;
        });


        datas['fileURL'] = url.toString();

        Firestore.instance.collection('contributions')
            .add(datas)
            .catchError((e) {

          setState(() {
            this.uploadFailed = true;
          });

          if(_keyScaffoldTagSelectorPage!=null && _keyScaffoldTagSelectorPage.currentState!=null) {
            _keyScaffoldTagSelectorPage.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment
                        .spaceBetween,
                    children: [
                      Text('An unexpected error occured'),
                    ],
                  ),
                ),
              );
          }
        }).whenComplete(() {
          setState(() {
            this.uploadSaved = true;
          });

          if(_keyScaffoldTagSelectorPage!=null && _keyScaffoldTagSelectorPage.currentState!=null) {
            _keyScaffoldTagSelectorPage.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [Text('Thank you for your contribution'),],
                  ),
                ),
              );
          }

          // TODO : Delete local file ...

          /**await VideoThumbnail.thumbnailData(
              video: _videoCamera.path,
              imageFormat: ImageFormat.JPEG,
              maxHeightOrWidth: 640,
              quality: 75,
              );**/

          // TODO : Display a message to the user
        });
      });
    });
  }

  void relaunchUpload(){
    this.uploadSaved = false;
    this.uploadRetry = true;
    this.uploadCompleted = false;
    this.uploadFailed = false;
    this.uploadProgressing = false;
    this.progress = 0;
    this.uploadProgressBar = 0;


    // TODO
  }

  /*
   * Event fucntions
   */

  void _onSelected(String id) {
    if (id == 'other') {
      setState(() {
        custom = true;
      });

      return;
    }

    if (_whoTags.containsKey(id)) {
      setState(() {
        for (var id in _whoTags.keys) {
          _whoTags[id].selected = false;
        }
        _whoTags[id].selected = true;
      });
    } else {
      setState(() {
        for (var id in _whatTags.keys) {
          _whatTags[id].selected = false;
        }
        _whatTags[id].selected = true;
      });
    }
  }

  void _onSubmit(BuildContext context) {
    //var cameraBloc = Provider.of<CameraBloc>(context);
    List<String> whoTags = [];
    List<String> whatTags = [];

    _whoTags.forEach((key, value) {
      if (value.selected) whoTags.add(key);
    });

    _whatTags.forEach((key, value) {
      if (value.selected) whatTags.add(key);
    });

    //cameraBloc.setTags(widget.requestID, whoTags, whatTags);

    setState(() {
      this.tagsSelected = true;
    });

    if(this.tagsSelected){ //&& this.uploadCompleted && this.uploadSaved){
      // Save Tags
      // TODO

      // Redirect
      Navigator.of(context).popUntil(
        ModalRoute.withName('/'),
      );
    }
  }

  void _onSkip(BuildContext context) {
    //var cameraBloc = Provider.of<CameraBloc>(context);
    //cameraBloc.setTags(widget.requestID, [], []);

    setState(() {
      this.tagSkipped = true;
    });

    // TODO

    if(this.tagSkipped){ //&& this.uploadCompleted && this.uploadSaved){
      // Redirect
      Navigator.of(context).popUntil(
        ModalRoute.withName('/'),
      );
    }
  }

  /* ************************************* */

  /*
   * Interface builders
   */

  Widget _title(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 13, top: 11, right: 13,),
      child: Row(
        children: <Widget>[
          this.uploadCompleted || this.uploadSaved ? Container()
            : this.uploadFailed ? Container(child: new CircularPercentIndicator(
              radius: 76.0,
              lineWidth: 8.0,
              percent: 0,
              center: Icon(Icons.error, size: 32, color: AppColor.red.withOpacity(0.75),),
              backgroundColor: AppColor.grey.withOpacity(0.25),
              progressColor: AppColor.green.withOpacity(0.25),
            )) : this.uploadRetry ? Container(child: new CircularPercentIndicator(
              radius: 76.0,
              lineWidth: 8.0,
              percent: 0,
              center: Icon(Icons.refresh, size: 32, color: AppColor.grey.withOpacity(0.75),),
              backgroundColor: AppColor.grey.withOpacity(0.25),
              progressColor: AppColor.green.withOpacity(0.25),
            )) : this.uploadProgressing  ? Container(child: new CircularPercentIndicator(
              radius: 76.0,
              lineWidth: 8.0,
              percent: 0.8,
              center: new Text(progress.toStringAsPrecision(0)+"%", style: TextStyle(color: AppColor.green)),
              backgroundColor: AppColor.grey.withOpacity(0.25),
              progressColor: AppColor.green,
            )) : Container(child: new CircularPercentIndicator(
              radius: 76.0,
              lineWidth: 8.0,
              percent: 0,
              center: Icon(MdiIcons.earth, size: 32, color: AppColor.green.withOpacity(0.5),),
              backgroundColor: AppColor.grey.withOpacity(0.25),
              progressColor: AppColor.green,
            ),
          ),
          /* ClipRRect(
            borderRadius: BorderRadius.circular(21),
            child: Image.network('https://i0.wp.com/zblogged.com/wp-content/uploads/2019/02/FakeDP.jpeg?resize=567%2C580&ssl=1',
              width: 60,height: 60,
            ),
          ), */
          SizedBox(
            width: 18,
          ),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RichText(
                  text: TextSpan(
                    style: Theme.of(context).textTheme.title.apply(color: Colors.white),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Uploading ...',
                        style: Theme.of(context).textTheme.title.apply(
                          color: AppColor.green.withOpacity(1),
                          fontWeightDelta: 300,
                        ),
                      ),
                      this.tagsSelected || this.tagSkipped ? TextSpan(text: '') :
                        widget.mode=="video" ? TextSpan(text: '\n\nHelp this project and tag your video :', style: TextStyle(color: Colors.white, fontSize: 14))
                          : TextSpan(text: '\n\nHelp this project and tag your photo :', style: TextStyle(color: Colors.white, fontSize: 14)),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _input(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        children: <Widget>[
          TextField(
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white),
            textCapitalization: TextCapitalization.sentences,
            keyboardType: TextInputType.text,
            autofocus: true,
            decoration: InputDecoration(
              labelText: 'Custom Tag',
              labelStyle: TextStyle(color: Colors.white.withOpacity(0.85)),
              fillColor: Colors.white,
              focusColor: Colors.white,
              border: UnderlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: Colors.white.withOpacity(0.7),
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: Colors.white.withOpacity(0.7),
                ),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  width: 2,
                  color: Colors.white.withOpacity(1)
                ),
              ),
            ),

          ),
          Align(
            alignment: Alignment.bottomRight,
            child: FlatButton(
              onPressed: () {
                setState(() {
                  custom = false;
                });
              },
              child: Text(
                'cancel',
                style: Theme.of(context).textTheme.button.apply(
                  color: Colors.white.withOpacity(0.9),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _tagsWidgets(Map<String, TagData> tags) {
    int len = tags.keys.length;

    assert(len > 0);
    assert(len % 3 == 0);
    List<Row> rows = [];
    List<Tag> cells;

    for (int i = 0; i < len; i++) {
      if (i % 3 == 0) cells = [];
      TagData data = tags.values.elementAt(i);

      cells.add(Tag(
        id: tags.keys.elementAt(i),
        name: data.name,
        icon: data.icon,
        selected: data.selected,
        action: _onSelected,
      ));

      if (i % 3 == 2) {
        rows.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround, children: cells,
        ));
      }
    }

    return Column(children: rows,);
  }

  /* ************************************* */

  Widget _mainMenu(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 24, bottom: 0, right: 16, left: 16),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(top: 14,bottom: 2),
            color: AppColor.red,
            child: Column(
              children: <Widget>[
                Container(
                  child: Text('Who',
                    style: Theme.of(context).textTheme.display1.apply(color: Colors.white.withOpacity(0.65)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0, bottom: 8),
                  child: _tagsWidgets(_whoTags),
                ),
                Container(
                  margin: EdgeInsets.only(top: 14),
                  child: Text('What',
                    style: Theme.of(context).textTheme.display1.apply(color: Colors.white.withOpacity(0.65)),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 0, bottom: 18),
                  child: custom ? _input(context) : _tagsWidgets(_whatTags),
                ),
              ],
            ),
          ),
          Container(
            transform: Matrix4.translation(vector.Vector3(0, -1, 0)),
            height: 48,
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: AppColor.red,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        color: AppColor.blue,
                      ),
                    )
                  ],
                ),
                Align(
                  alignment: Alignment.center,
                  child: RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    color: AppColor.green,
                    child: Text(
                      'SUBMIT',
                      style: Theme.of(context).textTheme.title.apply(color: AppColor.blue),
                    ),
                    onPressed: () {
                      _onSubmit(context);
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _skip(BuildContext context) {
    return Container(
      transform: Matrix4.translation(vector.Vector3(0, -20, 0)),
      margin: EdgeInsets.only(right: 8),
      child: Align(
        alignment: Alignment.centerRight,
        child: FlatButton(
          onPressed: () {
            _onSkip(context);
          },
          child: Text('skip >',
            style: Theme.of(context).textTheme.button.apply(color: AppColor.grey.withOpacity(0.9)),
          ),
        ),
      ),
    );
  }

  /*
   * Native functions
   */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _keyScaffoldTagSelectorPage,
      backgroundColor: AppColor.blue,
        body: WillPopScope(onWillPop: () async {
          return Future.value(false); // Disallow back button on this page
        },
        child: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    _title(context),
                    this.tagsSelected || this.tagSkipped ? Container(
                      child: Column(children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(left: 8, right: 8, top: 18),
                            child: Text("",
                              textAlign: TextAlign.justify,
                              style: TextStyle(color: Colors.white, fontFamily: "Muli", fontSize: 18),
                            )),
                        Center(
                          child: AspectRatio(
                            aspectRatio: 1.85,
                            child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 0),
                                child:Container(
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.contain,
                                          image: AssetImage('assets/images/GetHome.png')
                                      )
                                  ),
                                )
                            ),
                          ),
                        ),
                        /*Align(
                          alignment: Alignment.center,
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                            color: AppColor.green,
                            child: Text(
                              'CONTINUE',
                              style: Theme.of(context).textTheme.title.apply(color: AppColor.blue),
                            ),
                            onPressed: () {
                              _onSubmit(context);
                            },
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),*/
                      ],),
                    ) : _mainMenu(context),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 0),
                  child: _skip(context),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
