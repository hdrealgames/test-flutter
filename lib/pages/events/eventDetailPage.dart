/// Packages
import 'dart:io';
import 'dart:async';
import 'package:Getinshoot/pages/events/cameraPage.dart';
import 'package:Getinshoot/pages/events/mediaViewerPage.dart';
import 'package:Getinshoot/widgets/HeaderWidgetHome.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
//import 'package:connectivity/connectivity.dart';

/// Styles
import 'package:flutter/cupertino.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:intl/intl.dart';
//import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:vector_math/vector_math_64.dart' as vector;

/// Orientation
import 'package:native_device_orientation/native_device_orientation.dart';

/// Progression
import 'package:percent_indicator/percent_indicator.dart';

/// Widgets
import 'package:Getinshoot/main.dart';
import 'package:Getinshoot/widgets/headerWidget.dart';
//import 'package:Getinshoot/pages/guidelines/guidelinesPage.dart';
//import 'package:Getinshoot/pages/events/widgets/contributionWidget.dart';
//import 'package:Getinshoot/widgets/menuWidget.dart';
//import 'package:Getinshoot/pages/events/widgets/dialerWidget.dart';
//import 'package:Getinshoot/pages/events/cameraPage.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

/// Auth
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';

/// Camera
import 'package:image_picker/image_picker.dart';
import 'package:exif/exif.dart';

/// Localization
//import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:flutter_cupertino_localizations/flutter_cupertino_localizations.dart';
//import 'package:intl/intl.dart';
//import 'package:intl/date_symbol_data_local.dart';

/// Local settings
//import 'package:shared_preferences/shared_preferences.dart';

class EventDetailPage extends StatefulWidget {
  final String name;
  final String image;
  final String host; // name of the host
  final String startDate;
  final String endDate;
  final int guests; // counter

  final DocumentSnapshot document;
  final String documentId;
  final String imageURL;
  final String thumbnailURL;

  final bool joined;
  final bool participated;
  final String invitationId;

  final String mediaOrientation;
  final int mediaByGuest;
  final int mediaDuration;

  EventDetailPage(
      {Key key,
      @required this.name,
      this.image,
      this.host,
      this.startDate,
      this.endDate,
      this.guests,
      this.document,
      @required this.documentId,
      this.imageURL,
      this.thumbnailURL,
      this.invitationId,
      this.joined,
      this.participated,
      @required this.mediaOrientation,
      @required this.mediaDuration,
      @required this.mediaByGuest})
      : super(key: key);

  @override
  _EventDetailPageState createState() => _EventDetailPageState();
}

class _EventDetailPageState extends State<EventDetailPage>
    with WidgetsBindingObserver {
  //GlobalKey _keyWidgetDetailPage = GlobalKey();
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _keyScaffoldDetailPage = GlobalKey<ScaffoldState>();
  //GlobalKey<ScaffoldState> _keyScaffoldDetailUploadScreen = GlobalKey<ScaffoldState>();

  /// Project status
  bool joined = false;
  bool reachLimit = false;

  /// Form field controllers
  final TextEditingController _commentController = TextEditingController();

  /// Form field focus nodes
  final FocusNode _commentFocus = FocusNode();

  /// Comment status
  bool commentScreen = false;
  bool commentSubmitting = false;
  String commentText = "";
  int commentLengthMax = 140;
  bool _error = false;

  /// Upload status
  bool uploadScreenBlank = true;
  bool uploadScreen = false;
  double uploadPourCent = 0;
  String uploadFileName = "";
  bool uploading = false;
  bool uploadSaved = false;
  bool uploadFailed = false;

  bool uploadCompleted = false;

  bool databaseSaving = false;
  bool databaseToSave = false;
  bool databaseSaved = false;

  /// Propose to retry
  bool uploadRetry = false;

  /// Upload status
  bool uploadProgressing = false;
  double uploadProgressBar = 0;
  double progress = 0.0;

  /// Display instructions
  bool displayInstructions = false;
  bool videoInstructions = false;

  //String _imageUrl;

  /// Initialisation status
  bool initialized = false;

  List<String> currentUserContributions = [];

  void setCurrentScreen() async {
    await analytics.setCurrentScreen(
        screenName: "Project Page", screenClassOverride: 'eventDetailPage');
  }

  /// function initState()
  @override
  void initState() {
    reachLimit = false;
    this.setCurrentScreen();
    super.initState();
  }

  /// function dispose()
  @override
  void dispose() {
    super.dispose();
  }

  /// function timestamp()
  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void displayUploadScreen() {
    uploadScreenBlank = false;
    uploadScreen = true;
    setState(() {
      uploadScreenBlank = false;
      uploadScreen = true;
    });
  }

  void displayUploadBlankScreen() {
    uploadScreenBlank = true;
    uploadScreen = true;
    setState(() {
      uploadScreenBlank = true;
      uploadScreen = true;
    });
  }

  void hideUploadScreen() {
    uploadScreen = false;
    uploadScreenBlank = false;
    setState(() {
      uploadScreenBlank = false;
      uploadScreen = false;
    });
  }

  void setUploadProgress(value) {
    uploadProgressing = true;
    uploadProgressBar = value;

    setState(() {
      uploadProgressing = true;
      uploadProgressBar = value;
    });
  }

  /// Function joinProject()
  bool joinProject() {
    //debugPrint("joinProject()");
    // Save the status "joined" for this project
    if (widget.invitationId != null) {
      _keyScaffoldDetailPage.currentState
        ..hideCurrentSnackBar()
        ..showSnackBar(
          SnackBar(
            duration: Duration(seconds: 2),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(currentLocale == "fr"
                    ? "Connexion au serveur ..."
                    : "Connecting to the server ..."),
                CircularProgressIndicator(),
              ],
            ),
          ),
        );

      Firestore.instance.collection('invitationactions').add({
        'invitationid': widget.invitationId,
        'userid': repositoryUser.userId,
        'eventid': widget.documentId,
        'joined': true,
        'created': DateTime.now(),
        'modified': DateTime.now(),
      }).then((doc) {
        _keyScaffoldDetailPage.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(currentLocale == "fr"
                      ? "Nous ouvrons votre accès à ce projet"
                      : "Allowing the access to this project"),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(AppColor.red),
                  ),
                ],
              ),
            ),
          );
        return true;
      }).catchError((e) {
        _keyScaffoldDetailPage.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(currentLocale == 'fr'
                      ? "Erreur inattendue. Veuillez réessayer"
                      : "Unexpected error. Please retry.")
                ],
              ),
            ),
          );
      });
    }
    return false;
  }

  void setReachLimit(bool limit) {
    reachLimit = limit;

    new Future.delayed(
        Duration.zero,
        () => setState(() {
              reachLimit = limit;
            }));

    return;
  }

  /* ** */
  Future<bool> imageUpload(File imageFile) async {
    displayUploadBlankScreen();
    setState(() {
      displayInstructions = false;
      uploadProgressing = false;
      uploadRetry = false;
      uploadFailed = false;
      uploadCompleted = false;
      uploadProgressBar = 0;
    });
    // File _photoCamera = await ImagePicker.pickImage(source: ImageSource.camera);

    String eventName = widget.documentId;
    try {
      eventName = widget.document.data['name'];
    } catch (e) {}
    // Log event "pick image"
    try {
      await analytics.logEvent(
        name: 'add_contribution',
        parameters: <String, dynamic>{
          'eventid': widget.documentId,
          'eventname': eventName,
          'type': 'photo'
        },
      );
    } catch (e) {}

    if (imageFile != null) {
      displayUploadScreen();

      String fileName = widget.documentId +
          '-' +
          repositoryUser.userId +
          '-' +
          '${timestamp()}.jpg';
      String storagePath = "participations/" +
          widget.documentId +
          "/photos/" +
          repositoryUser.userId +
          "/" +
          fileName;

      var datas = {
        'userid': repositoryUser.userId,
        'from': repositoryUser.name,
        'eventid': widget.documentId,
        'type': 'photo',
        'format': 'jpg',
        'file': fileName,
        'length': 0,
        'created': DateTime.now(),
        'modified': DateTime.now(),
        'fileURL': '', //url.toString(),
      };

      readExifFromBytes(await imageFile.readAsBytes()).then((data) {
        debugPrint(data.toString());
        for (String key in data.keys) {
          datas['meta-' +
                  key.trim().toLowerCase().replaceAll(RegExp(' +'), '_')] =
              (data[key]).toString();
          debugPrint(key.trim().toLowerCase().replaceAll(RegExp(' +'), '_') +
              "=${data[key]}");
        }
      }).catchError((onError) {
        debugPrint(onError.toString());
      });

      debugPrint(datas.toString());

      final StorageReference storageRef =
          FirebaseStorage.instance.ref().child(storagePath);
      final StorageUploadTask task = storageRef.putFile(imageFile);

      task.events.listen((event) {
        SystemChrome.setEnabledSystemUIOverlays([]);
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);

        double _progress;
        _progress = event.snapshot.bytesTransferred.toDouble() /
            event.snapshot.totalByteCount.toDouble();

        setState(() {
          updateProgressBar(_progress);
        });

        setUploadProgress(_progress);

        debugPrint("====================================================");
        debugPrint("             ==== PROGRESS : " +
            _progress.toString() +
            " ====             ");
        debugPrint("====================================================");
      }).onError((error) {
        _keyScaffoldDetailPage.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              duration: Duration(seconds: 10),
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(currentLocale == 'fr'
                      ? 'Erreur inattendue. Veuillez réessayer'
                      : 'An unexpected error occured'),
                ],
              ),
            ),
          );
        debugPrint(
            "============================================================== ERROR : " +
                error.toString());
      });

      task.onComplete.then((snapshot) {
        setState(() {
          uploadProgressing = true;
        });
        debugPrint(
            "========================================== UPLOAD COMPLETE");
        snapshot.ref.getDownloadURL().then((url) {
          double length = 0;
          try {
            length = imageFile.lengthSync().toDouble();
          } catch (e) {}
          length = ((length / 1024) / ~1014);
          if (length < 0) length = length * -1;

          if (length != null && length > 0)
            datas['size'] = length.toStringAsFixed(2);
          datas['fileURL'] = url.toString();

          Firestore.instance
              .collection('contributions')
              .add(datas)
              .catchError((e) {
            _keyScaffoldDetailPage.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  duration: Duration(seconds: 10),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(currentLocale == 'fr'
                          ? 'Erreur inattendue. Veuillez réessayer'
                          : 'An unexpected error occured'),
                    ],
                  ),
                ),
              );
          }).whenComplete(() {
            setState(() {
              uploadCompleted = true;
            });
            hideUploadScreen();

            try {
              imageFile.delete();
            } catch (e) {
              debugPrint("Delete FILE ERROR : " + e.toString());
            }
            hideUploadScreen();
          });
        });
      });
    } else {
      hideUploadScreen();
    }
    return true;
  }

  void updateProgressBar(value) {
    if (value != null) {
      try {
        double pourCent = value;
        if (pourCent < 0) pourCent = 0;
        if (pourCent > 1) pourCent = 1;

        setState(() {
          progress =
              uploadProgressBar = uploadPourCent = uploadProgressBar = pourCent;
        });
      } catch (e) {}
    }
  }

  Future<bool> videoUpload(File videoFile) async {
    displayUploadBlankScreen();
    setState(() {
      displayInstructions = false;
      uploadProgressing = false;
      uploadRetry = false;
      uploadFailed = false;
      uploadCompleted = false;
      uploadProgressBar = 0;
    });

    String eventName = widget.documentId;
    try {
      eventName = widget.document.data['name'];
    } catch (e) {}
    // Log event "pick video"
    try {
      await analytics.logEvent(
        name: 'add_contribution',
        parameters: <String, dynamic>{
          'eventid': widget.documentId,
          'eventname': eventName,
          'type': 'video'
        },
      );
    } catch (e) {}

    if (videoFile != null) {
      displayUploadScreen();

      String fileName = widget.documentId +
          '-' +
          repositoryUser.userId +
          '-' +
          '${timestamp()}.mp4';

      String storagePath = "participations/" +
          widget.documentId +
          "/videos/" +
          repositoryUser.userId +
          "/" +
          fileName;

      var datas = {
        'userid': repositoryUser.userId,
        'from': repositoryUser.name,
        'eventid': widget.documentId,
        'type': 'video',
        'format': 'mp4',
        'file': fileName,
        'length': 0,
        'created': DateTime.now(),
        'modified': DateTime.now(),
        'fileURL': '',
        //'fileThumbURL': urlThumb,
      };

      readExifFromBytes(await videoFile.readAsBytes()).then((data) {
        debugPrint(data.toString());
        for (String key in data.keys) {
          datas['meta-' +
                  key.trim().toLowerCase().replaceAll(RegExp(' +'), '_')] =
              (data[key]).toString();
          debugPrint(key.trim().toLowerCase().replaceAll(RegExp(' +'), '_') +
              " (${data[key].tagType}): ${data[key]}");
        }
      }).catchError((onError) {
        debugPrint(onError.toString());
      });

      debugPrint(datas.toString());

      final StorageReference storageRef =
          FirebaseStorage.instance.ref().child(storagePath);
      final StorageUploadTask task = storageRef.putFile(videoFile);

      task.events.listen((event) {
        SystemChrome.setEnabledSystemUIOverlays([]);
        SystemChrome.setPreferredOrientations(
            [DeviceOrientation.portraitUp, DeviceOrientation.landscapeLeft]);

        double _progress;

        _progress = (event.snapshot.bytesTransferred.toDouble() /
                event.snapshot.totalByteCount.toDouble())
            .toDouble();

        setState(() {
          displayUploadScreen();
          updateProgressBar(_progress);
          uploadProgressing = true;
        });

        setUploadProgress(_progress);

        debugPrint("====================================================");
        debugPrint("             ==== PROGRESS : " +
            _progress.toString() +
            " ====             ");
        debugPrint("====================================================");
      }).onError((error) {
        setState(() {
          uploadFailed = true;
        });

        _keyScaffoldDetailPage.currentState
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              duration: Duration(seconds: 10),
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(currentLocale == 'fr'
                      ? 'Erreur inattendue. Veuillez réessayer'
                      : 'An unexpected error occured'),
                ],
              ),
            ),
          );
        debugPrint(
            "============================================================== ERROR : " +
                error.toString());
      });

      task.onComplete.then((snapshot) {
        debugPrint(
            "========================================== UPLOAD COMPLETE");

        snapshot.ref.getDownloadURL().then((url) {
          double length = 0;
          try {
            length = videoFile.lengthSync().toDouble();
          } catch (e) {}
          length = ((length / 1024) / ~1014);
          if (length < 0) length = length * -1;

          if (length != null && length > 0)
            datas['size'] = length.toStringAsFixed(2);
          datas['fileURL'] = url.toString();

          Firestore.instance
              .collection('contributions')
              .add(datas)
              .catchError((e) {
            //debugPrint(e);
            _keyScaffoldDetailPage.currentState
              ..hideCurrentSnackBar()
              ..showSnackBar(
                SnackBar(
                  duration: Duration(seconds: 10),
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(currentLocale == 'fr'
                          ? 'Erreur inattendue. Veuillez réessayer'
                          : 'An unexpected error occured'),
                    ],
                  ),
                ),
              );
          }).whenComplete(() {
            hideUploadScreen();
            setState(() {
              uploadCompleted = true;
            });

            try {
              videoFile.delete();
            } catch (e) {
              debugPrint("Delete FILE ERROR : " + e.toString());
            }
            hideUploadScreen();
          });
        });
      });
    } else {
      hideUploadScreen();
    }

    return true;
  }

  /* ********************************************************************** */

  @override
  Widget build(BuildContext context) {
    return NativeDeviceOrientationReader(
        useSensor: true,
        builder: (context) {
          NativeDeviceOrientation orientation =
              NativeDeviceOrientationReader.orientation(context);

          //int turns;
          String orientationInfo = "";
          bool nativeOrientationVertical = true;

          switch (orientation) {
            case NativeDeviceOrientation.landscapeRight:
              nativeOrientationVertical = false;
              orientationInfo = "landscapeRight";
              //turns = 1;
              break;
            case NativeDeviceOrientation.portraitDown:
              nativeOrientationVertical = true;
              orientationInfo = "portraitDown";
              //turns = 2;
              break;
            case NativeDeviceOrientation.landscapeLeft:
              nativeOrientationVertical = false;
              orientationInfo = "landscapeLeft";
              //turns = 3;
              break;
            default:
              nativeOrientationVertical = true;
              orientationInfo = "portraitUp";
              //turns = 0;
              break;
          }

          //debugPrint(orientationInfo);

          return Container(
              child: StreamBuilder<DocumentSnapshot>(
                  stream: Firestore.instance
                      .collection('invitations')
                      .document(widget.invitationId)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<DocumentSnapshot> qSnapshot) {
                    if (qSnapshot.hasError)
                      return Text('Error: ${qSnapshot.error}');
                    switch (qSnapshot.data) {
                      case null:
                        return Container(
                            decoration: BoxDecoration(color: AppColor.blue),
                            child: SafeArea(
                                child: Scaffold(
                                    key: _keyScaffoldDetailPage,
                                    body: Container())));

                      default:
                        if (widget.document.data['stopped'] != null &&
                            widget.document.data['stopped']) {
                          return Container(
                              decoration: BoxDecoration(color: AppColor.blue),
                              child: SafeArea(
                                  child: Scaffold(
                                      key: _keyScaffoldDetailPage,
                                      appBar: HeaderWidget.get(context),
                                      //drawer: MenuWidget.get(context),
                                      body: Container(
                                          decoration: BoxDecoration(
                                              color: AppColor.blue),
                                          child: Column(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 2,
                                                  child: Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 15,
                                                        right: 15,
                                                        top: 30,
                                                        bottom: 10),
                                                    child: Text(
                                                        widget.document
                                                                .data['name'] ??
                                                            "",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontFamily: 'Muli',
                                                            fontSize: 26.0),
                                                        textAlign:
                                                            TextAlign.left),
                                                  )),
                                              Expanded(
                                                  flex: 8,
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                      image: AssetImage(
                                                          'assets/images/finish.png'),
                                                      fit: BoxFit.contain,
                                                    )),
                                                  )),
                                              Expanded(
                                                  flex: 3,
                                                  child: Padding(
                                                      padding:
                                                          EdgeInsets.symmetric(
                                                              horizontal: 10),
                                                      child: Container(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          child: Text(
                                                            currentLocale ==
                                                                    "fr"
                                                                ? "\n\rLe projet est terminé.\n\rMerci de votre participation !"
                                                                : "\n\rProject completed.\n\rThanks for your participation !",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 20.0),
                                                            textAlign: TextAlign
                                                                .center,
                                                          )))),
                                            ],
                                          )))));
                        } else if (qSnapshot.data.data['joined'] != null &&
                            !qSnapshot.data.data['joined']) {
                          /// Display Project informations
                          return Container(
                              decoration: BoxDecoration(color: AppColor.blue),
                              child: SafeArea(
                                  child: Scaffold(
                                      key: _keyScaffoldDetailPage,
                                      appBar: uploadScreen
                                          ? null
                                          : HeaderWidget.get(context),
                                      //drawer: uploadScreen ? null : MenuWidget.get(context),
                                      body: Container(
                                          decoration: BoxDecoration(
                                            color: AppColor.blue,
                                          ),
                                          child: Column(
                                            children: <Widget>[
                                              /**Container(
																				height: 90,
																				decoration: BoxDecoration(
																						image: DecorationImage(
																							image: widget.imageURL != null
																									? NetworkImage(
																									widget.imageURL)
																									: AssetImage(
																									'assets/images/default.jpg'),
																							fit: BoxFit.cover,
																							alignment: widget.imageURL !=
																									null
																									? Alignment.center
																									: Alignment.bottomCenter,
																						)
																				),
																			),**/
                                              Padding(
                                                padding: EdgeInsets.only(
                                                    left: 10,
                                                    right: 10,
                                                    top: 10,
                                                    bottom: 10),
                                                child: Text(
                                                    widget.document
                                                            .data['name'] ??
                                                        "",
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontFamily: 'Muli',
                                                        fontSize: 30.0),
                                                    textAlign:
                                                        TextAlign.center),
                                              ),
                                              ConstrainedBox(
                                                  constraints: BoxConstraints(
                                                      maxHeight:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height *
                                                              0.175),
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 15,
                                                          right: 15,
                                                          top: 0,
                                                          bottom: 15),
                                                      child: IntrinsicHeight(
                                                          child:
                                                              SingleChildScrollView(
                                                                  scrollDirection:
                                                                      Axis
                                                                          .vertical,
                                                                  child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                            widget.document.data['description'] ??
                                                                                "",
                                                                            style: TextStyle(
                                                                                color: Colors.white,
                                                                                fontFamily: 'Muli',
                                                                                fontSize: 14.0),
                                                                            textAlign: TextAlign.left)
                                                                      ]))))),
                                              Expanded(
                                                  flex: 5,
                                                  child: Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 15,
                                                          right: 15,
                                                          top: 5.0,
                                                          bottom: 5),
                                                      child: Container(
                                                          decoration: BoxDecoration(
                                                              color:
                                                                  Colors.white,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5.0),
                                                              border: Border.all(
                                                                  color: Colors.white
                                                                      .withOpacity(
                                                                          0.1),
                                                                  width: 1)),
                                                          child:
                                                              SingleChildScrollView(
                                                                  scrollDirection:
                                                                      Axis
                                                                          .vertical,
                                                                  child:
                                                                      Padding(
                                                                          padding: EdgeInsets.only(
                                                                              left: 10,
                                                                              right: 10,
                                                                              top: 0,
                                                                              bottom: 5),
                                                                          child: Column(
                                                                            children: <Widget>[
                                                                              Container(
                                                                                  height: 145,
                                                                                  decoration: BoxDecoration(
                                                                                    image: DecorationImage(
                                                                                      image: AssetImage('assets/images/terms.png'),
                                                                                      fit: BoxFit.contain,
                                                                                      alignment: Alignment.topCenter,
                                                                                    ),
                                                                                  )),
                                                                              Text(currentLocale == "fr" ? "En cochant cette case, vous accordez au Propriétaire du Projet un droit exclusif et non cessible à tous les Contenus que vous avez réalisés, permettant au Propriétaire d’utiliser, de modifier, et de diffuser vos Contenus visuels par tous les moyens connus ; presse-écrite, audiovisuel, informatique, Internet ; et inconnus à ce jour, et ce pour une durée de 15 ans à compter de la date de début du projet." : "En cochant cette case, vous accordez au Propriétaire du Projet un droit exclusif et non cessible à tous les Contenus que vous avez réalisés, permettant au Propriétaire d’utiliser, de modifier, et de diffuser vos Contenus visuels par tous les moyens connus ; presse-écrite, audiovisuel, informatique, Internet ; et inconnus à ce jour, et ce pour une durée de 15 ans à compter de la date de début du projet.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Cette cession de droits s’effectue à titre gratuit, et le Participant ne pourra réclamer aucune rémunération au Propriétaire, ni à Getinshoot." : "Cette cession de droits s’effectue à titre gratuit, et le Participant ne pourra réclamer aucune rémunération au Propriétaire, ni à Getinshoot.", style: TextStyle(color: Colors.black, height: 1.0, fontFamily: 'Muli', fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "En cochant cette case, vous vous engagez à respecter la vie privée des personnes que vous filmer. En outre, vous vous engagez à ne pas les filmer à leur insu. Par conséquent, vous vous engagez à communiquer le cadre du Projet auquel vous participez, à toutes les personnes que vous filmez, dont le visage serait clairement identifiable, et ce dans le but d’obtenir une autorisation tacite des personnes en toute connaissance de cause." : "En cochant cette case, vous vous engagez à respecter la vie privée des personnes que vous filmer. En outre, vous vous engagez à ne pas les filmer à leur insu. Par conséquent, vous vous engagez à communiquer le cadre du Projet auquel vous participez, à toutes les personnes que vous filmez, dont le visage serait clairement identifiable, et ce dans le but d’obtenir une autorisation tacite des personnes en toute connaissance de cause.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "En contre-partie, Le Propriétaire s’engage à informer au préalable tous les Participants sur les conditions et la finalité du Projet : Description du Projet, objectif, moyens de diffusions envisagés, territorialité de la diffusion du Projet." : "En contre-partie, Le Propriétaire s’engage à informer au préalable tous les Participants sur les conditions et la finalité du Projet : Description du Projet, objectif, moyens de diffusions envisagés, territorialité de la diffusion du Projet.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Getinshoot garantie qu’aucune données personnelles ne sera revendu à des tiers et qu’elle resteront confidentielles. Toutes les données collectées, à l’exception des Contenus, seront uniquement utilisés par l’équipe Getinshoot dans l’objectif d’améliorer le service. En conséquence, Le Propriétaire du Projet ne pourra pas accédez à vos données personnelles." : "Getinshoot garantie qu’aucune données personnelles ne sera revendu à des tiers et qu’elle resteront confidentielles. Toutes les données collectées, à l’exception des Contenus, seront uniquement utilisés par l’équipe Getinshoot dans l’objectif d’améliorer le service. En conséquence, Le Propriétaire du Projet ne pourra pas accédez à vos données personnelles.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Le terme « Projet » désigne le cadre dans lequel vous avez été invité à participer par le Propriétaire du Projet. Le Projet est définit par un nom spécifique et des conditions précisés dans l’invitation par le Propriétaire. Les Contenus que vous avez réalisés seront délimités aux conditions précisées dans le Projet." : "Le terme « Projet » désigne le cadre dans lequel vous avez été invité à participer par le Propriétaire du Projet. Le Projet est définit par un nom spécifique et des conditions précisés dans l’invitation par le Propriétaire. Les Contenus que vous avez réalisés seront délimités aux conditions précisées dans le Projet.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Le terme « Propriétaire » désigne la personne responsable du Projet sur lequel vous avez été invité. Il est le cessionnaire de tous les Contenus récupéré grâce à Getinshoot, ou à défaut, le représentant légal de la personne moral cessionnaire des Contenus. Le Propriétaire est responsable de la définition du cadre du Projet ainsi que du nombre, identité et des droits d’utilisation de chaque Participants." : "Le terme « Propriétaire » désigne la personne responsable du Projet sur lequel vous avez été invité. Il est le cessionnaire de tous les Contenus récupéré grâce à Getinshoot, ou à défaut, le représentant légal de la personne moral cessionnaire des Contenus. Le Propriétaire est responsable de la définition du cadre du Projet ainsi que du nombre, identité et des droits d’utilisation de chaque Participants.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Le terme « Participant » désigne toute les personnes qui ont été invitées par le Propriétaire à produire des contenus dans le cadre du Projet." : "Le terme « Participant » désigne toute les personnes qui ont été invitées par le Propriétaire à produire des contenus dans le cadre du Projet.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Le terme « Contenus » s’applique à tout les Contenus que vous avez réalisé dans le cadre du Projet : Images, Vidéos, Enregistrement sonore, Document écrit, Création graphique." : "Le terme « Contenus » s’applique à tout les Contenus que vous avez réalisé dans le cadre du Projet : Images, Vidéos, Enregistrement sonore, Document écrit, Création graphique.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                              Text(currentLocale == "fr" ? "Le terme « Getinshoot » désigne le service d’organisation et de collecte des Contenus à l’attention du Propriétaire. Getinshoot est un outil collaboratif de création de media. Getinshoot n’est en aucun cas responsable des droits à l’image, propriété intellectuelle, auteur, qui incombe au propriétaire du projet." : "Le terme « Getinshoot » désigne le service d’organisation et de collecte des Contenus à l’attention du Propriétaire. Getinshoot est un outil collaboratif de création de media. Getinshoot n’est en aucun cas responsable des droits à l’image, propriété intellectuelle, auteur, qui incombe au propriétaire du projet.", style: TextStyle(color: Colors.black, fontFamily: 'Muli', height: 1.0, fontSize: 12.0), textAlign: TextAlign.justify),
                                                                              SizedBox(height: 10),
                                                                            ],
                                                                          )))))),
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                    child: Padding(
                                                  padding: EdgeInsets.all(4.0),
                                                  child: new Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: <Widget>[
                                                        new Text(
                                                            (currentLocale ==
                                                                    "fr"
                                                                ? "J'accepte les conditions"
                                                                : "I accept the terms"),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white)),
                                                        Container(
                                                            alignment: Alignment
                                                                .center,
                                                            child: Checkbox(
                                                              value: false,
                                                              checkColor:
                                                                  Colors.white,
                                                              onChanged:
                                                                  (bool value) {
                                                                if (value) {
                                                                  this.joinProject();
                                                                }
                                                              },
                                                            ))
                                                      ]),
                                                )),
                                              )
                                            ],
                                          )))));
                          /***********************/
                        } else {
                          if (widget.document.data['pause'] != null &&
                              widget.document.data['pause']) {
                            return Container(
                                decoration: BoxDecoration(color: AppColor.blue),
                                child: SafeArea(
                                    child: Scaffold(
                                        key: _keyScaffoldDetailPage,
                                        appBar: HeaderWidget.get(context),
                                        //drawer: MenuWidget.get(context),
                                        body: Container(
                                            decoration: BoxDecoration(
                                                color: AppColor.blue),
                                            child: Column(
                                              children: <Widget>[
                                                Expanded(
                                                    flex: 2,
                                                    child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 10,
                                                                  right: 10,
                                                                  top: 30,
                                                                  bottom: 10),
                                                          child: Text(
                                                              widget.document
                                                                          .data[
                                                                      'name'] ??
                                                                  "",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontFamily:
                                                                      'Muli',
                                                                  fontSize:
                                                                      26.0),
                                                              textAlign:
                                                                  TextAlign
                                                                      .left),
                                                        ))),
                                                Expanded(
                                                    flex: 8,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                          image:
                                                              DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/pause.png'),
                                                        fit: BoxFit.contain,
                                                      )),
                                                    )),
                                                Expanded(
                                                    flex: 2,
                                                    child: Padding(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10),
                                                        child: Container(
                                                            alignment: Alignment
                                                                .topCenter,
                                                            child: Text(
                                                              currentLocale ==
                                                                      "fr"
                                                                  ? "Les contributions sont suspendues pour le moment"
                                                                  : "Contributions suspended for the moment",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize:
                                                                      20.0),
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                            )))),
                                              ],
                                            )))));

                            /** ******************************************************** **/
                          } else {
                            return displayInstructions
                                ? Container(
                                    decoration:
                                        BoxDecoration(color: AppColor.blue),
                                    child: SafeArea(
                                        child: Scaffold(
                                      key: _keyScaffoldDetailPage,
                                      body: RotatedBox(
                                          quarterTurns: (widget
                                                          .mediaOrientation !=
                                                      null &&
                                                  (widget.mediaOrientation ==
                                                      "vertical"))
                                              ? (nativeOrientationVertical
                                                  ? (orientationInfo ==
                                                          'landscapeRight'
                                                      ? 0
                                                      : (orientationInfo ==
                                                              'landscapeLeft'
                                                          ? 1
                                                          : 0))
                                                  : 0)
                                              : (!nativeOrientationVertical
                                                  ? (orientationInfo ==
                                                          'landscapeRight'
                                                      ? -1
                                                      : (orientationInfo ==
                                                              'landscapeLeft'
                                                          ? 1
                                                          : 0))
                                                  : -1),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                color: AppColor.blue),
                                            child: Column(
                                              children: <Widget>[
                                                Expanded(
                                                  flex: 1,
                                                  child: (widget.mediaOrientation !=
                                                              null &&
                                                          (widget.mediaOrientation ==
                                                              "vertical"))
                                                      ? Column(
                                                          children: <Widget>[
                                                            videoInstructions &&
                                                                    widget.mediaDuration >
                                                                        0
                                                                ? Expanded(
                                                                    flex: 1,
                                                                    child: Center(
                                                                        child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .timer,
                                                                          color: AppColor
                                                                              .green
                                                                              .withOpacity(0.5),
                                                                          size:
                                                                              75,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        Text(
                                                                          currentLocale == 'fr'
                                                                              ? "Durée maximale"
                                                                              : "Max. Duration",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style: TextStyle(
                                                                              fontSize: 20,
                                                                              fontFamily: 'Muli',
                                                                              color: AppColor.green),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                5),
                                                                        Text(
                                                                          currentLocale == 'fr'
                                                                              ? widget.mediaDuration.toString() + " secondes"
                                                                              : widget.mediaDuration.toString() + " seconds",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                28,
                                                                            fontFamily:
                                                                                'Muli',
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                      ],
                                                                    )))
                                                                : Container(),
                                                            widget.mediaOrientation !=
                                                                        null &&
                                                                    (widget.mediaOrientation ==
                                                                            'horizontal' ||
                                                                        widget.mediaOrientation ==
                                                                            'vertical')
                                                                ? Expanded(
                                                                    flex: 1,
                                                                    child: Center(
                                                                        child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                        Icon(
                                                                          widget.mediaOrientation == 'horizontal'
                                                                              ? Icons.panorama_horizontal
                                                                              : Icons.panorama_vertical,
                                                                          color: AppColor
                                                                              .green
                                                                              .withOpacity(0.5),
                                                                          size:
                                                                              75,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        Text(
                                                                          "Orientation",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style: TextStyle(
                                                                              fontSize: 20,
                                                                              fontFamily: 'Muli',
                                                                              color: AppColor.green),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                5),
                                                                        Text(
                                                                          widget
                                                                              .mediaOrientation,
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                28,
                                                                            fontFamily:
                                                                                'Muli',
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                      ],
                                                                    )))
                                                                : Container(),
                                                          ],
                                                        )
                                                      : Row(
                                                          children: <Widget>[
                                                            videoInstructions &&
                                                                    widget.mediaDuration >
                                                                        0
                                                                ? Expanded(
                                                                    flex: 1,
                                                                    child: Center(
                                                                        child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                        Icon(
                                                                          Icons
                                                                              .timer,
                                                                          color: AppColor
                                                                              .green
                                                                              .withOpacity(0.5),
                                                                          size:
                                                                              75,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        Text(
                                                                          currentLocale == 'fr'
                                                                              ? "Durée maximale"
                                                                              : "Max. Duration",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style: TextStyle(
                                                                              fontSize: 20,
                                                                              fontFamily: 'Muli',
                                                                              color: AppColor.green),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                5),
                                                                        Text(
                                                                          currentLocale == 'fr'
                                                                              ? widget.mediaDuration.toString() + " secondes"
                                                                              : widget.mediaDuration.toString() + " seconds",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                28,
                                                                            fontFamily:
                                                                                'Muli',
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                      ],
                                                                    )))
                                                                : Container(),
                                                            widget.mediaOrientation !=
                                                                        null &&
                                                                    (widget.mediaOrientation ==
                                                                            'horizontal' ||
                                                                        widget.mediaOrientation ==
                                                                            'vertical')
                                                                ? Expanded(
                                                                    flex: 1,
                                                                    child: Center(
                                                                        child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                        Icon(
                                                                          widget.mediaOrientation == 'horizontal'
                                                                              ? Icons.panorama_horizontal
                                                                              : Icons.panorama_vertical,
                                                                          color: AppColor
                                                                              .green
                                                                              .withOpacity(0.5),
                                                                          size:
                                                                              75,
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                10),
                                                                        Text(
                                                                          "Orientation",
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style: TextStyle(
                                                                              fontSize: 20,
                                                                              fontFamily: 'Muli',
                                                                              color: AppColor.green),
                                                                        ),
                                                                        SizedBox(
                                                                            height:
                                                                                5),
                                                                        Text(
                                                                          widget
                                                                              .mediaOrientation,
                                                                          textAlign:
                                                                              TextAlign.center,
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                28,
                                                                            fontFamily:
                                                                                'Muli',
                                                                          ),
                                                                        ),
                                                                        Expanded(
                                                                          flex:
                                                                              1,
                                                                          child:
                                                                              Container(),
                                                                        ),
                                                                      ],
                                                                    )))
                                                                : Container(),
                                                          ],
                                                        ),
                                                ),
                                              ],
                                            ),
                                          )),
                                    )))
                                : commentScreen && !commentSubmitting
                                    ? Container(
                                        decoration:
                                            BoxDecoration(color: AppColor.blue),
                                        child: SafeArea(
                                            child: Scaffold(
                                                key: _keyScaffoldDetailPage,
                                                appBar: HeaderWidgetHome.get(
                                                    context, () {}),
                                                body: Container(
                                                    decoration: BoxDecoration(
                                                      color: Color.fromRGBO(
                                                          255, 255, 255, 0.1),
                                                    ),
                                                    child: Stack(
                                                        children: <Widget>[
                                                          SingleChildScrollView(
                                                              child:
                                                                  ConstrainedBox(
                                                                      constraints:
                                                                          BoxConstraints(
                                                                        minHeight: MediaQuery.of(context)
                                                                            .size
                                                                            .height,
                                                                        minWidth: MediaQuery.of(context)
                                                                            .size
                                                                            .width,
                                                                      ),
                                                                      child: Wrap(
                                                                          children: <
                                                                              Widget>[
                                                                            Column(
                                                                              children: <Widget>[
                                                                                SizedBox(height: 1),
                                                                                Padding(
                                                                                    padding: EdgeInsets.only(left: 22, right: 28, top: 17, bottom: 4),
                                                                                    child: Row(mainAxisAlignment: MainAxisAlignment.end, crossAxisAlignment: CrossAxisAlignment.end, children: <Widget>[
                                                                                      Expanded(
                                                                                          flex: 1,
                                                                                          child: Container(
                                                                                            alignment: Alignment.topLeft,
                                                                                            child: Padding(
                                                                                              padding: EdgeInsets.only(top: 0),
                                                                                              child: Text(currentLocale == 'fr' ? "ajouter un message" : "add a message", style: TextStyle(color: Colors.white, fontFamily: 'Poppins', fontSize: 23.0, fontWeight: FontWeight.normal), textAlign: TextAlign.left),
                                                                                            ),
                                                                                          )),
                                                                                      Container(
                                                                                        alignment: Alignment.bottomRight,
                                                                                        width: 80,
                                                                                        child: Text(currentLocale == 'fr' ? "140 car. max" : "140 char. max", style: TextStyle(color: Colors.white, fontFamily: 'Muli', fontSize: 9.0, fontWeight: FontWeight.normal), textAlign: TextAlign.right),
                                                                                      )
                                                                                    ])),
                                                                                Padding(
                                                                                    padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 15),
                                                                                    child: Container(
                                                                                        child: Form(
                                                                                            key: _formKey,
                                                                                            child: Wrap(children: <Widget>[
                                                                                              Padding(
                                                                                                  padding: EdgeInsets.only(bottom: 0),
                                                                                                  child: Container(
                                                                                                      decoration: BoxDecoration(
                                                                                                        color: AppColor.grey.withOpacity(0.2),
                                                                                                        borderRadius: BorderRadius.circular(15),
                                                                                                      ),
                                                                                                      child: Padding(
                                                                                                          padding: EdgeInsets.only(bottom: 5),
                                                                                                          child: Column(
                                                                                                            children: <Widget>[
                                                                                                              !commentSubmitting
                                                                                                                  ? Padding(
                                                                                                                      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 25),
                                                                                                                      child: TextFormField(
                                                                                                                        cursorColor: AppColor.green,
                                                                                                                        style: new TextStyle(color: AppColor.green),
                                                                                                                        keyboardType: TextInputType.multiline,
                                                                                                                        maxLength: 140,
                                                                                                                        minLines: 5,
                                                                                                                        maxLines: 5,
                                                                                                                        controller: _commentController,
                                                                                                                        focusNode: _commentFocus,
                                                                                                                        decoration: InputDecoration(
                                                                                                                          labelText: null,
                                                                                                                          labelStyle: TextStyle(color: AppColor.grey),
                                                                                                                          hintStyle: TextStyle(color: AppColor.grey),
                                                                                                                          focusedBorder: new UnderlineInputBorder(borderSide: new BorderSide(color: Colors.transparent)),
                                                                                                                          border: new UnderlineInputBorder(borderSide: new BorderSide(color: Colors.transparent)),
                                                                                                                          enabledBorder: new UnderlineInputBorder(borderSide: new BorderSide(color: Colors.transparent)),
                                                                                                                        ),
                                                                                                                        autovalidate: true,
                                                                                                                        validator: (value) {
                                                                                                                          if (_error && value.isEmpty) {
                                                                                                                            return currentLocale == 'fr' ? 'Veuillez saisir votre message' : 'Enter your message';
                                                                                                                          }
                                                                                                                          return null;
                                                                                                                          //return _error && _firstNameController.text.length<=0 ?  currentLocale =='fr' ? 'Veuillez saisir votre nom' : 'Please enter your firstname' : null;
                                                                                                                        },
                                                                                                                      ))
                                                                                                                  : Container(),
                                                                                                            ],
                                                                                                          )))),
                                                                                              commentSubmitting
                                                                                                  ? Center(
                                                                                                      child: Padding(
                                                                                                      padding: EdgeInsets.only(top: 10, bottom: 18, right: 25, left: 25),
                                                                                                      child: CircularProgressIndicator(),
                                                                                                    ))
                                                                                                  : Container(),
                                                                                              SizedBox(height: 2),
                                                                                              !commentSubmitting
                                                                                                  ? Row(crossAxisAlignment: CrossAxisAlignment.end, mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
                                                                                                      Center(
                                                                                                        child: Padding(
                                                                                                            padding: EdgeInsets.only(top: 10, bottom: 5, right: 10, left: 5),
                                                                                                            child: Container(
                                                                                                              child: Center(
                                                                                                                  child: GestureDetector(
                                                                                                                      onTap: () async {
                                                                                                                        setState(() {
                                                                                                                          _commentFocus.unfocus();
                                                                                                                          commentSubmitting = false;
                                                                                                                          commentScreen = false;
                                                                                                                        });

                                                                                                                        String eventName = widget.documentId;
                                                                                                                        try {
                                                                                                                          eventName = widget.document.data['name'];
                                                                                                                        } catch (e) {}
                                                                                                                        // Log event "pick image"
                                                                                                                        try {
                                                                                                                          analytics.logEvent(
                                                                                                                            name: 'add_contribution',
                                                                                                                            parameters: <String, dynamic>{
                                                                                                                              'eventid': widget.documentId,
                                                                                                                              'eventname': eventName,
                                                                                                                              'type': 'comment'
                                                                                                                            },
                                                                                                                          );
                                                                                                                        } catch (e) {}
                                                                                                                      },
                                                                                                                      child: Container(
                                                                                                                        decoration: BoxDecoration(border: Border.all(color: AppColor.blue), color: Colors.white, borderRadius: BorderRadius.circular(15)),
                                                                                                                        child: Container(
                                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 12),
                                                                                                                          child: Text(
                                                                                                                            currentLocale == 'fr' ? 'annuler'.toUpperCase() : 'cancel'.toUpperCase(),
                                                                                                                            style: TextStyle(fontSize: 13, color: AppColor.blue),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ))),
                                                                                                            )),
                                                                                                      ),
                                                                                                      Center(
                                                                                                        child: Padding(
                                                                                                            padding: EdgeInsets.only(top: 10, bottom: 5, right: 0, left: 5),
                                                                                                            child: Container(
                                                                                                              child: Center(
                                                                                                                  child: GestureDetector(
                                                                                                                      onTap: () {
                                                                                                                        if (_commentController.text.isEmpty) {
                                                                                                                          // TODO
                                                                                                                        } else {
                                                                                                                          setState(() {
                                                                                                                            uploadProgressBar = 0;
                                                                                                                            commentSubmitting = true;
                                                                                                                          });

                                                                                                                          commentText = _commentController.text;

                                                                                                                          Firestore.instance.collection('contributions').add({
                                                                                                                            'userid': repositoryUser.userId,
                                                                                                                            'from': repositoryUser.name,
                                                                                                                            'eventid': widget.documentId,
                                                                                                                            'type': 'comment',
                                                                                                                            'comment': commentText,
                                                                                                                            'length': 0,
                                                                                                                            'size': 0,
                                                                                                                            'created': DateTime.now(),
                                                                                                                            'modified': DateTime.now(),
                                                                                                                          }).catchError((e) {
                                                                                                                            _keyScaffoldDetailPage.currentState
                                                                                                                              ..hideCurrentSnackBar()
                                                                                                                              ..showSnackBar(
                                                                                                                                SnackBar(
                                                                                                                                  duration: Duration(seconds: 10),
                                                                                                                                  content: Row(
                                                                                                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                                                                    children: [
                                                                                                                                      Text(currentLocale == 'fr' ? 'Erreur inattendue. Veuillez réessayer' : 'An unexpected error occured'),
                                                                                                                                    ],
                                                                                                                                  ),
                                                                                                                                ),
                                                                                                                              );

                                                                                                                            setState(() {
                                                                                                                              commentSubmitting = false;
                                                                                                                            });
                                                                                                                          }).whenComplete(() {
                                                                                                                            _commentController.text = "";

                                                                                                                            setState(() {
                                                                                                                              uploadProgressBar = 1.0;
                                                                                                                            });

                                                                                                                            new Future.delayed(
                                                                                                                                Duration(seconds: 1),
                                                                                                                                () => setState(() {
                                                                                                                                      commentSubmitting = commentScreen = false;
                                                                                                                                    }));
                                                                                                                          });
                                                                                                                        }
                                                                                                                      },
                                                                                                                      child: Container(
                                                                                                                        decoration: BoxDecoration(color: AppColor.green, borderRadius: BorderRadius.circular(15)),
                                                                                                                        child: Container(
                                                                                                                          padding: const EdgeInsets.symmetric(horizontal: 21, vertical: 12),
                                                                                                                          child: Text(
                                                                                                                            currentLocale == 'fr' ? 'valider'.toUpperCase() : 'send'.toUpperCase(),
                                                                                                                            style: TextStyle(
                                                                                                                              fontSize: 13,
                                                                                                                              color: AppColor.blue,
                                                                                                                            ),
                                                                                                                          ),
                                                                                                                        ),
                                                                                                                      ))),
                                                                                                            )),
                                                                                                      )
                                                                                                    ])
                                                                                                  : Container(),
                                                                                            ])))),
                                                                              ],
                                                                            )
                                                                          ])))
                                                        ])))))
                                    : Container(
                                        decoration:
                                            BoxDecoration(color: AppColor.blue),
                                        child: SafeArea(
                                          child: Scaffold(
                                              key: _keyScaffoldDetailPage,
                                              appBar: HeaderWidget.get(context),
                                              //drawer: MenuWidget.get(context),
                                              body: Container(
                                                decoration: BoxDecoration(
                                                    color: AppColor.blue),
                                                child: Stack(
                                                  children: <Widget>[
                                                    SingleChildScrollView(
                                                        child: ConstrainedBox(
                                                      constraints:
                                                          BoxConstraints(
                                                        minHeight:
                                                            MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .height +
                                                                130,
                                                        minWidth: MediaQuery.of(
                                                                context)
                                                            .size
                                                            .width,
                                                      ),
                                                      child: Wrap(
                                                        children: <Widget>[
                                                          Column(
                                                              children: <
                                                                  Widget>[
                                                                SizedBox(
                                                                    height: 5),
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left:
                                                                            15,
                                                                        right:
                                                                            15,
                                                                        top: 11,
                                                                        bottom:
                                                                            0),
                                                                    child: Text(
                                                                        currentLocale == 'fr'
                                                                            ? "Bienvenue"
                                                                                .toUpperCase()
                                                                            : "Welcome to"
                                                                                .toUpperCase(),
                                                                        style: TextStyle(
                                                                            color: AppColor
                                                                                .grey,
                                                                            fontFamily:
                                                                                'Muli',
                                                                            letterSpacing:
                                                                                1,
                                                                            fontWeight: FontWeight
                                                                                .bold,
                                                                            fontSize:
                                                                                13.0),
                                                                        textAlign:
                                                                            TextAlign.center),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left:
                                                                            10,
                                                                        right:
                                                                            10,
                                                                        top: 5,
                                                                        bottom:
                                                                            10),
                                                                    child: Text(
                                                                        widget.document.data['name'] ??
                                                                            "",
                                                                        style: TextStyle(
                                                                            color: Colors
                                                                                .white,
                                                                            fontFamily:
                                                                                'Poppins',
                                                                            fontSize:
                                                                                20.0,
                                                                            fontWeight: FontWeight
                                                                                .bold),
                                                                        textAlign:
                                                                            TextAlign.center),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                    padding: EdgeInsets.only(
                                                                        bottom:
                                                                            5,
                                                                        left:
                                                                            5),
                                                                    child: Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment
                                                                                .start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment
                                                                                .start,
                                                                        children: <
                                                                            Widget>[
                                                                          Expanded(
                                                                              flex: 50,
                                                                              child: Padding(
                                                                                  padding: EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
                                                                                  child: AspectRatio(
                                                                                      aspectRatio: 1,
                                                                                      child: Container(
                                                                                        decoration: BoxDecoration(
                                                                                            boxShadow: [
                                                                                              BoxShadow(blurRadius: 11.0, color: AppColor.blue.withOpacity(0.2), offset: Offset(0.0, 0.0))
                                                                                            ],
                                                                                            borderRadius: BorderRadius.circular(15),
                                                                                            image: DecorationImage(
                                                                                              image: widget.imageURL != null ? NetworkImage(widget.imageURL) : AssetImage('assets/images/default.jpg'),
                                                                                              fit: BoxFit.cover,
                                                                                              alignment: widget.imageURL != null ? Alignment.center : Alignment.bottomCenter,
                                                                                            )),
                                                                                      )))),
                                                                          Expanded(
                                                                              flex: 45,
                                                                              child: Column(children: <Widget>[
                                                                                SizedBox(height: 10),
                                                                                Container(
                                                                                  alignment: Alignment.topLeft,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(left: 5, right: 1, top: 0, bottom: 0),
                                                                                    child: Text(currentLocale == 'fr' ? "créé par".toUpperCase() : "created by".toUpperCase(), style: TextStyle(color: AppColor.grey, fontFamily: 'Muli', fontWeight: FontWeight.bold, letterSpacing: 1, fontSize: 10.0), textAlign: TextAlign.left),
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  alignment: Alignment.topLeft,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(left: 5, right: 1, top: 0, bottom: 15),
                                                                                    child: Text(widget.document.data['host'] ?? "", style: TextStyle(color: Colors.white, fontFamily: 'Muli', fontSize: 15.0), textAlign: TextAlign.left),
                                                                                  ),
                                                                                ),
                                                                                SizedBox(height: 3),
                                                                                Container(
                                                                                  alignment: Alignment.topLeft,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(left: 5, right: 1, top: 0, bottom: 0),
                                                                                    child: Text(currentLocale == 'fr' ? "depuis".toUpperCase() : "since".toUpperCase(), style: TextStyle(color: AppColor.grey, fontFamily: 'Muli', fontWeight: FontWeight.bold, letterSpacing: 1, fontSize: 10.0), textAlign: TextAlign.left),
                                                                                  ),
                                                                                ),
                                                                                Container(
                                                                                  alignment: Alignment.topLeft,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(left: 5, right: 1, top: 0, bottom: 10),
                                                                                    child: Text(currentLocale == 'fr' ? (DateFormat("dd/MM/yyyy").format(widget.document.data['created'].toDate()).toString() ?? "") : (DateFormat("yyyy/MM/dd").format(widget.document.data['created'].toDate()).toString() ?? ""), style: TextStyle(color: Colors.white, fontFamily: 'Muli', fontSize: 15.0), textAlign: TextAlign.left),
                                                                                  ),
                                                                                ),
                                                                                SizedBox(height: 2),
                                                                                widget.documentId == "8WKYVTSVtLRitRmXxYcK" || widget.documentId == ""
                                                                                    ? Container()
                                                                                    : Container(
                                                                                        alignment: Alignment.topLeft,
                                                                                        child: Padding(
                                                                                            padding: EdgeInsets.only(left: 5, right: 1, top: 0, bottom: 15),
                                                                                            child: StreamBuilder<DocumentSnapshot>(
                                                                                                stream: Firestore.instance.collection('eventstats').document(widget.documentId).snapshots(),
                                                                                                builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> eSnapshot) {
                                                                                                  int nbUsers = 0;
                                                                                                  int cPhotos = 0;
                                                                                                  int cVideos = 0;
                                                                                                  int cComments = 0;

                                                                                                  if (eSnapshot.hasError) {
                                                                                                  } else {
                                                                                                    switch (eSnapshot.data) {
                                                                                                      case null:
                                                                                                        break;
                                                                                                      default:
                                                                                                        if (eSnapshot.data != null && eSnapshot.data.data != null && eSnapshot.data.data['joined'] != null) {
                                                                                                          nbUsers = eSnapshot.data.data['joined'];
                                                                                                        }

                                                                                                        if (eSnapshot.data != null && eSnapshot.data.data != null && eSnapshot.data.data['invited'] != null) {
                                                                                                          if (eSnapshot.data.data['invited'] > nbUsers) nbUsers = eSnapshot.data.data['invited'];
                                                                                                        }

                                                                                                        if (eSnapshot.data != null && eSnapshot.data.data != null && eSnapshot.data.data['nbvideos'] != null) {
                                                                                                          cVideos = eSnapshot.data.data['nbvideos'];
                                                                                                        }

                                                                                                        if (eSnapshot.data != null && eSnapshot.data.data != null && eSnapshot.data.data['nbphotos'] != null) {
                                                                                                          cPhotos = eSnapshot.data.data['nbphotos'];
                                                                                                        }

                                                                                                        if (eSnapshot.data != null && eSnapshot.data.data != null && eSnapshot.data.data['nbcomments'] != null) {
                                                                                                          cComments = eSnapshot.data.data['nbcomments'];
                                                                                                        }
                                                                                                    }
                                                                                                  }

                                                                                                  return Row(children: <Widget>[
                                                                                                    Expanded(
                                                                                                        flex: 1,
                                                                                                        child: Container(
                                                                                                            child: Column(children: <Widget>[
                                                                                                          Text(
                                                                                                            " " + nbUsers.toString(),
                                                                                                            style: TextStyle(fontSize: 19, fontFamily: 'Muli', color: AppColor.grey, fontWeight: FontWeight.w200),
                                                                                                          ),
                                                                                                          Opacity(
                                                                                                              opacity: 1,
                                                                                                              child: Container(
                                                                                                                alignment: Alignment.bottomCenter,
                                                                                                                height: 35,
                                                                                                                width: 35,
                                                                                                                decoration: BoxDecoration(
                                                                                                                  image: DecorationImage(
                                                                                                                    image: AssetImage('assets/images/projects/st-user.png'),
                                                                                                                    fit: BoxFit.contain,
                                                                                                                    alignment: Alignment.bottomCenter,
                                                                                                                  ),
                                                                                                                ),
                                                                                                              )),
                                                                                                        ]))),
                                                                                                    Container(width: 5),
                                                                                                    Expanded(
                                                                                                        flex: 1,
                                                                                                        child: Container(
                                                                                                            child: Column(children: <Widget>[
                                                                                                          Row(
                                                                                                            children: <Widget>[
                                                                                                              Opacity(
                                                                                                                opacity: 1,
                                                                                                                child: Container(
                                                                                                                  alignment: Alignment.bottomCenter,
                                                                                                                  height: 25,
                                                                                                                  width: 25,
                                                                                                                  decoration: BoxDecoration(
                                                                                                                    image: DecorationImage(
                                                                                                                      image: AssetImage('assets/images/projects/st-photo.png'),
                                                                                                                      fit: BoxFit.contain,
                                                                                                                      alignment: Alignment.bottomCenter,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ),
                                                                                                              ),
                                                                                                              Text(
                                                                                                                " " + cPhotos.toString(),
                                                                                                                style: TextStyle(fontSize: 14, fontFamily: 'Muli', color: AppColor.grey, fontWeight: FontWeight.w200),
                                                                                                              ),
                                                                                                            ],
                                                                                                          ),
                                                                                                          Row(
                                                                                                            children: <Widget>[
                                                                                                              Opacity(
                                                                                                                opacity: 1,
                                                                                                                child: Container(
                                                                                                                  alignment: Alignment.bottomCenter,
                                                                                                                  height: 25,
                                                                                                                  width: 25,
                                                                                                                  decoration: BoxDecoration(
                                                                                                                    image: DecorationImage(
                                                                                                                      image: AssetImage('assets/images/projects/st-video.png'),
                                                                                                                      fit: BoxFit.contain,
                                                                                                                      alignment: Alignment.bottomCenter,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ),
                                                                                                              ),
                                                                                                              Text(
                                                                                                                " " + cVideos.toString(),
                                                                                                                style: TextStyle(fontSize: 14, fontFamily: 'Muli', color: AppColor.grey, fontWeight: FontWeight.w200),
                                                                                                              ),
                                                                                                            ],
                                                                                                          ),
                                                                                                          Row(
                                                                                                            children: <Widget>[
                                                                                                              Opacity(
                                                                                                                opacity: 1,
                                                                                                                child: Container(
                                                                                                                  alignment: Alignment.bottomCenter,
                                                                                                                  height: 25,
                                                                                                                  width: 25,
                                                                                                                  decoration: BoxDecoration(
                                                                                                                    image: DecorationImage(
                                                                                                                      image: AssetImage('assets/images/projects/st-comment.png'),
                                                                                                                      fit: BoxFit.contain,
                                                                                                                      alignment: Alignment.bottomCenter,
                                                                                                                    ),
                                                                                                                  ),
                                                                                                                ),
                                                                                                              ),
                                                                                                              Text(
                                                                                                                " " + cComments.toString(),
                                                                                                                style: TextStyle(fontSize: 14, fontFamily: 'Muli', color: AppColor.grey, fontWeight: FontWeight.w200),
                                                                                                              ),
                                                                                                            ],
                                                                                                          )
                                                                                                        ])))
                                                                                                  ]);
                                                                                                })))
                                                                              ])),
                                                                        ])),
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child:
                                                                      Padding(
                                                                    padding: EdgeInsets.only(
                                                                        left:
                                                                            12,
                                                                        right:
                                                                            10,
                                                                        top: 15,
                                                                        bottom:
                                                                            10),
                                                                    child: Text(
                                                                        currentLocale == 'fr'
                                                                            ? "description"
                                                                                .toUpperCase()
                                                                            : "description"
                                                                                .toUpperCase(),
                                                                        style: TextStyle(
                                                                            color: AppColor
                                                                                .grey,
                                                                            fontFamily:
                                                                                'Muli',
                                                                            letterSpacing:
                                                                                1,
                                                                            fontWeight: FontWeight
                                                                                .bold,
                                                                            fontSize:
                                                                                11.0),
                                                                        textAlign:
                                                                            TextAlign.left),
                                                                  ),
                                                                ),
                                                                Padding(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          left:
                                                                              15,
                                                                          right:
                                                                              15,
                                                                          top:
                                                                              0,
                                                                          bottom:
                                                                              5),
                                                                  child: Column(
                                                                      children: <
                                                                          Widget>[
                                                                        Text(
                                                                            widget.document.data['description'] ??
                                                                                "",
                                                                            style: TextStyle(
                                                                                color: Colors.white,
                                                                                fontFamily: 'Muli',
                                                                                fontSize: 14.0),
                                                                            textAlign: TextAlign.left)
                                                                      ]),
                                                                ),
                                                                Container(
                                                                  alignment:
                                                                      Alignment
                                                                          .centerLeft,
                                                                  child:
                                                                      Padding(
                                                                    padding:
                                                                        EdgeInsets
                                                                            .only(
                                                                      left: 12,
                                                                      right: 10,
                                                                      top: 15,
                                                                      bottom:
                                                                          10,
                                                                    ),
                                                                    child: Text(
                                                                        currentLocale == 'fr'
                                                                            ? "mes contributions"
                                                                                .toUpperCase()
                                                                            : "my contributions"
                                                                                .toUpperCase(),
                                                                        style: TextStyle(
                                                                            color: AppColor
                                                                                .red,
                                                                            fontFamily:
                                                                                'Muli',
                                                                            letterSpacing:
                                                                                1,
                                                                            fontWeight: FontWeight
                                                                                .bold,
                                                                            fontSize:
                                                                                11.0),
                                                                        textAlign:
                                                                            TextAlign.left),
                                                                  ),
                                                                ),
                                                                widget.document.data['allowGuestToKeepCopy'] ==
                                                                            null ||
                                                                        !widget.document.data[
                                                                            'allowGuestToKeepCopy']
                                                                    ? Padding(
                                                                        padding: EdgeInsets.only(
                                                                            left:
                                                                                15,
                                                                            right:
                                                                                15,
                                                                            top:
                                                                                0,
                                                                            bottom:
                                                                                5),
                                                                        child: Column(
                                                                            children: <Widget>[
                                                                              Text(currentLocale == 'fr' ? "L'organisateur de ce projet n'a pas souhaité que les shooterz conservent une copie de leurs contributions sur leurs smartphones. C’est lui qui reviendra vers vous avec le résultat final !" : "Host of this project didn't want shooterz keep a copy of their contributions on their smartphones. He will come back to you with the final result!", style: TextStyle(color: Colors.white, fontFamily: 'Muli', fontSize: 14.0), textAlign: TextAlign.left)
                                                                            ]),
                                                                      )
                                                                    : Padding(
                                                                        padding: EdgeInsets.only(
                                                                            left:
                                                                                15,
                                                                            right:
                                                                                15,
                                                                            top:
                                                                                0,
                                                                            bottom:
                                                                                5),
                                                                        child: Container(
                                                                            child: StreamBuilder<QuerySnapshot>(
                                                                                stream: Firestore.instance.collection('contributions').where("userid", isEqualTo: repositoryUser.userId).where("eventid", isEqualTo: widget.document.documentID).snapshots(),
                                                                                builder: (context, snapshot) {
                                                                                  if (!snapshot.hasData) {
                                                                                    return Center(child: const Text('Loading events...'));
                                                                                  }
                                                                                  return GridView.count(
                                                                                      crossAxisCount: 4,
                                                                                      crossAxisSpacing: 10,
                                                                                      mainAxisSpacing: 10,
                                                                                      shrinkWrap: true,
                                                                                      primary: false,
                                                                                      children: snapshot.data.documents.map((media) {
                                                                                        if (media['type'] == "comment") {
                                                                                          return GestureDetector(
                                                                                              onTap: () async {
                                                                                                Navigator.of(context).push(
                                                                                                  MaterialPageRoute(builder: (context) {
                                                                                                    return MediaViewerPage(media: media);
                                                                                                  }),
                                                                                                );
                                                                                              },
                                                                                              child: Container(
                                                                                                  decoration: BoxDecoration(
                                                                                                    color: AppColor.green,
                                                                                                    borderRadius: BorderRadius.circular(15.0),
                                                                                                  ),
                                                                                                  child: Center(child: Text(media["comment"], style: TextStyle(fontSize: 7, fontFamily: "Muli", color: Colors.white), textAlign: TextAlign.center))));
                                                                                        }

                                                                                        return media['thumbnailURL'] != null
                                                                                            ? GestureDetector(
                                                                                                onTap: () async {
                                                                                                  Navigator.of(context).push(
                                                                                                    MaterialPageRoute(builder: (context) {
                                                                                                      return MediaViewerPage(media: media);
                                                                                                    }),
                                                                                                  );
                                                                                                },
                                                                                                child: ClipRRect(
                                                                                                    borderRadius: BorderRadius.circular(15),
                                                                                                    child: CachedNetworkImage(
                                                                                                      fit: BoxFit.cover,
                                                                                                      imageUrl: media['thumbnailURL'],
                                                                                                    )))
                                                                                            : Text("");
                                                                                      }).toList());
                                                                                }))),
                                                                /**
																									SizedBox(height: 25),
																									ConstrainedBox(
																											constraints: BoxConstraints(
																												minWidth: MediaQuery.of(context).size.width,
																												minHeight: 100
																											),
																											child:Container(
																											decoration: BoxDecoration(
																												color: AppColor.grey.withOpacity(0.8)
																											),
																											//alignment: Alignment.centerLeft,
																											child: Padding(
																												padding: EdgeInsets.only(
																														left: 12,
																														right: 10,
																														top: 15,
																														bottom: 10
																												),
																												child: Column(
																													children: <Widget>[
																														Text("test", style: TextStyle(
																																	color: Colors.white
																															)
																														)
																													],
																												)
																											),
																										)
																									),
																									**/
                                                                /***************/
                                                              ]),
                                                        ],
                                                      ),
                                                    )),
                                                    uploadScreen &&
                                                            uploadScreenBlank
                                                        ? Container()
                                                        : Positioned(
                                                            bottom: 0,
                                                            child: Container(
                                                                child: Container(
                                                                    alignment: Alignment.bottomCenter,
                                                                    height: 158,
                                                                    width: MediaQuery.of(context).size.width,
                                                                    decoration: BoxDecoration(
                                                                      color: Colors
                                                                          .transparent,
                                                                      image:
                                                                          DecorationImage(
                                                                        image: AssetImage(
                                                                            'assets/images/projects/waveform.png'),
                                                                        fit: BoxFit
                                                                            .fill,
                                                                        alignment:
                                                                            Alignment.bottomCenter,
                                                                      ),
                                                                    ),
                                                                    child: commentSubmitting
                                                                        ? Padding(
                                                                            padding: EdgeInsets.only(left: 5, right: 0, bottom: 0, top: 7),
                                                                            child: Container(
                                                                                alignment: Alignment.center,
                                                                                decoration: BoxDecoration(
                                                                                    //color: AppColor.grey
                                                                                    ),
                                                                                height: 155,
                                                                                child: Padding(
                                                                                  padding: EdgeInsets.only(left: 27, top: 47, right: 5, bottom: 1),
                                                                                  child: Column(
                                                                                    children: <Widget>[
                                                                                      Column(
                                                                                        children: <Widget>[
                                                                                          Container(
                                                                                            margin: EdgeInsets.only(
                                                                                              left: 13,
                                                                                              top: 1,
                                                                                              right: 13,
                                                                                            ),
                                                                                            child: Row(
                                                                                              children: <Widget>[
                                                                                                Container(
                                                                                                    child: new CircularPercentIndicator(
                                                                                                  radius: 76.0,
                                                                                                  lineWidth: 8.0,
                                                                                                  percent: uploadProgressBar,
                                                                                                  animation: false,
                                                                                                  circularStrokeCap: CircularStrokeCap.round,
                                                                                                  center: new Text((uploadProgressBar * 100).toStringAsFixed(0) + "%", style: TextStyle(color: AppColor.green)),
                                                                                                  //Icon(Icons.backup, size: 32, color: AppColor.grey.withOpacity(0.55),),
                                                                                                  backgroundColor: AppColor.grey.withOpacity(0.2),
                                                                                                  progressColor: AppColor.green.withOpacity(0.85),
                                                                                                )),
                                                                                                SizedBox(width: 18),
                                                                                                Flexible(
                                                                                                  child: Column(
                                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                                    children: <Widget>[
                                                                                                      RichText(
                                                                                                        text: TextSpan(
                                                                                                          style: Theme.of(context).textTheme.title.apply(color: Colors.white),
                                                                                                          children: <TextSpan>[
                                                                                                            TextSpan(
                                                                                                              text: currentLocale == "fr" ? 'Enregistrement' : 'Submitting',
                                                                                                              style: Theme.of(context).textTheme.title.apply(
                                                                                                                    color: AppColor.green.withOpacity(1),
                                                                                                                    fontWeightDelta: 165,
                                                                                                                  ),
                                                                                                            ),
                                                                                                            TextSpan(text: '\r\n'),
                                                                                                            TextSpan(text: currentLocale == "fr" ? 'ajout de votre message \n\rà ce projet' : 'adding your message to the project', style: TextStyle(color: AppColor.grey.withOpacity(0.6), fontSize: 14))
                                                                                                          ],
                                                                                                        ),
                                                                                                      ),
                                                                                                    ],
                                                                                                  ),
                                                                                                ),
                                                                                              ],
                                                                                            ),
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                      Container(
                                                                                        margin: EdgeInsets.only(bottom: 0),
                                                                                        child: Container(),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                )))
                                                                        : uploadScreen
                                                                            ? Padding(
                                                                                padding: EdgeInsets.only(left: 5, right: 0, bottom: 0, top: 7),
                                                                                child: Container(
                                                                                    alignment: Alignment.center,
                                                                                    decoration: BoxDecoration(
                                                                                        //color: AppColor.grey
                                                                                        ),
                                                                                    height: 155,
                                                                                    child: uploadScreen
                                                                                        ? uploadScreenBlank
                                                                                            ? Container()
                                                                                            : Padding(
                                                                                                padding: EdgeInsets.only(left: 27, top: 47, right: 5, bottom: 1),
                                                                                                child: Column(
                                                                                                  children: <Widget>[
                                                                                                    Column(
                                                                                                      children: <Widget>[
                                                                                                        Container(
                                                                                                          margin: EdgeInsets.only(
                                                                                                            left: 13,
                                                                                                            top: 1,
                                                                                                            right: 13,
                                                                                                          ),
                                                                                                          child: Row(
                                                                                                            children: <Widget>[
                                                                                                              Container(
                                                                                                                  child: new CircularPercentIndicator(
                                                                                                                radius: 76.0,
                                                                                                                lineWidth: 8.0,
                                                                                                                percent: uploadProgressBar,
                                                                                                                animation: false,
                                                                                                                circularStrokeCap: CircularStrokeCap.round,
                                                                                                                center: new Text((uploadProgressBar * 100).toStringAsFixed(0) + "%", style: TextStyle(color: AppColor.green)),
                                                                                                                //Icon(Icons.backup, size: 32, color: AppColor.grey.withOpacity(0.55),),
                                                                                                                backgroundColor: AppColor.grey.withOpacity(0.2),
                                                                                                                progressColor: AppColor.green.withOpacity(0.85),
                                                                                                              )),
                                                                                                              SizedBox(width: 18),
                                                                                                              Flexible(
                                                                                                                child: Column(
                                                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                                                  children: <Widget>[
                                                                                                                    RichText(
                                                                                                                      text: TextSpan(
                                                                                                                        style: Theme.of(context).textTheme.title.apply(color: Colors.white),
                                                                                                                        children: <TextSpan>[
                                                                                                                          TextSpan(
                                                                                                                            text: currentLocale == "fr" ? (this.uploadCompleted || this.uploadSaved ? 'Uploading ...' : this.uploadFailed ? 'Echec' : this.uploadRetry ? 'Echec du transfert' : this.uploadProgressing ? 'Transfert en cours' : 'Connexion au serveur') : (this.uploadCompleted || this.uploadSaved ? 'Uploading ...' : this.uploadFailed ? 'Error' : this.uploadRetry ? 'Upload failed' : this.uploadProgressing ? 'Uploading file' : 'Connecting to server'),
                                                                                                                            style: Theme.of(context).textTheme.title.apply(
                                                                                                                                  color: AppColor.green.withOpacity(1),
                                                                                                                                  fontWeightDelta: 175,
                                                                                                                                  fontFamily: "Muli",
                                                                                                                                ),
                                                                                                                          ),
                                                                                                                          TextSpan(text: '\r\n'),
                                                                                                                          TextSpan(text: currentLocale == "fr" ? (this.uploadCompleted || this.uploadSaved ? ' ' : this.uploadFailed ? 'le transfert a échoué' : this.uploadRetry ? 'Voulez-vous réessayer ?' : this.uploadProgressing ? 'votre contribution est en cours de transfert' : 'préparation du fichier pour le transfert') : (this.uploadCompleted || this.uploadSaved ? ' ' : this.uploadFailed ? 'upload failed' : this.uploadRetry ? 'Do you want to retry ?' : this.uploadProgressing ? 'we\'re uploading your contribution' : 'Preparing file to transfert'), style: TextStyle(color: AppColor.grey.withOpacity(0.6), fontSize: 12, fontFamily: "Muli"))
                                                                                                                        ],
                                                                                                                      ),
                                                                                                                    ),
                                                                                                                  ],
                                                                                                                ),
                                                                                                              ),
                                                                                                            ],
                                                                                                          ),
                                                                                                        )
                                                                                                      ],
                                                                                                    ),
                                                                                                    Container(
                                                                                                      margin: EdgeInsets.only(bottom: 0),
                                                                                                      child: Container(),
                                                                                                    ),
                                                                                                  ],
                                                                                                ),
                                                                                              )
                                                                                        : Container()))
                                                                            : Padding(
                                                                                padding: EdgeInsets.only(top: 45),
                                                                                child: Container(
                                                                                    decoration: BoxDecoration(color: Colors.transparent),
                                                                                    child: StreamBuilder<DocumentSnapshot>(
                                                                                        stream: Firestore.instance.collection('contributionstats').document(widget.documentId + '-' + repositoryUser.userId).snapshots(),
                                                                                        builder: (BuildContext context, AsyncSnapshot<DocumentSnapshot> sSnapshot) {
                                                                                          int nbPhotos = 0;
                                                                                          int nbVideos = 0;
                                                                                          int nbComments = 0;
                                                                                          // TODO : bool photoEnabled = false; bool videoEnabled = false; bool commentEnabled = false;

                                                                                          if (sSnapshot.hasError) {
                                                                                          } else {
                                                                                            switch (sSnapshot.data) {
                                                                                              case null:
                                                                                                break;
                                                                                              default:
                                                                                                if (sSnapshot.data != null && sSnapshot.data.data != null && sSnapshot.data.data['nbvideos'] != null) {
                                                                                                  nbVideos = sSnapshot.data.data['nbvideos'];
                                                                                                }

                                                                                                if (sSnapshot.data != null && sSnapshot.data.data != null && sSnapshot.data.data['nbphotos'] != null) nbPhotos = sSnapshot.data.data['nbphotos'];

                                                                                                if (sSnapshot.data != null && sSnapshot.data.data != null && sSnapshot.data.data['nbcomments'] != null) nbComments = sSnapshot.data.data['nbcomments'];

                                                                                                if (widget.mediaByGuest > 0) {
                                                                                                  if (nbVideos + nbPhotos + nbComments > widget.mediaByGuest) {
                                                                                                    reachLimit = true;
                                                                                                    debugPrint("REACH LIMIT");
                                                                                                    //if (!reachLimit) setReachLimit(true);
                                                                                                  } else {
                                                                                                    reachLimit = false;
                                                                                                    //if (reachLimit) setReachLimit(false);
                                                                                                  }
                                                                                                }

                                                                                                break;
                                                                                            }
                                                                                          }

                                                                                          return Column(children: <Widget>[
                                                                                            !reachLimit
                                                                                                ? Container(
                                                                                                    child: Padding(
                                                                                                      padding: EdgeInsets.only(top: 3, bottom: 2),
                                                                                                    ),
                                                                                                  )
                                                                                                : Container(),
                                                                                            reachLimit
                                                                                                ? Container(
                                                                                                    //alignement: Alignement.center,
                                                                                                    child: Padding(
                                                                                                    padding: EdgeInsets.only(top: 25, bottom: 2, left: 15, right: 15),
                                                                                                    child: Text(currentLocale == "fr" ? "Vous avez atteint la limite de \n\rcontributions définie par l'organisateur." : "You've reach the limit of contributions \n\rallowed by the host of this project.", textAlign: TextAlign.center, style: TextStyle(color: Colors.white.withOpacity(0.6), fontWeight: FontWeight.bold, fontFamily: 'Muli', fontSize: 17)),
                                                                                                  ))
                                                                                                : Row(children: <Widget>[
                                                                                                    Expanded(
                                                                                                        flex: 1,
                                                                                                        child: Container(
                                                                                                            child: GestureDetector(
                                                                                                                onTap: () async {
                                                                                                                  final recordedFilePath = await Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                                                                                    return CameraPage(mode: CameraMode.picture, eventId: widget.documentId, invitationId: widget.invitationId, orientation: widget.mediaOrientation == "horizontal" ? CameraOrientation.landscape : CameraOrientation.portrait, tooltip: "", userID: repositoryUser.userId, videoDuration: widget.mediaDuration == null || widget.mediaDuration == 0 ? -1 : widget.mediaDuration);
                                                                                                                  }));
                                                                                                                  print(recordedFilePath);
                                                                                                                  if (recordedFilePath != null) {
                                                                                                                    await imageUpload(File(recordedFilePath));
                                                                                                                  }
                                                                                                                },
                                                                                                                child: Column(children: <Widget>[
                                                                                                                  Padding(padding: EdgeInsets.only(top: 0, bottom: 1), child: Container(width: 60, height: 60, decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/projects/photo.png'))))),
                                                                                                                  Padding(
                                                                                                                      padding: EdgeInsets.only(bottom: 15),
                                                                                                                      child: Text(currentLocale == 'fr' ? "photo (" + (nbPhotos > 999 ? "+999" : nbPhotos.toString()) + ")" : "photo (" + (nbPhotos > 999 ? "+999" : nbPhotos.toString()) + ")",
                                                                                                                          style: TextStyle(
                                                                                                                            color: Colors.white,
                                                                                                                            fontFamily: 'Muli',
                                                                                                                          ))),
                                                                                                                ])))),
                                                                                                    Expanded(
                                                                                                        flex: 1,
                                                                                                        child: Container(
                                                                                                            child: GestureDetector(
                                                                                                                onTap: () async {
                                                                                                                  final recordedFilePath = await Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                                                                                    return CameraPage(
                                                                                                                      mode: CameraMode.video,
                                                                                                                      eventId: widget.documentId,
                                                                                                                      invitationId: widget.invitationId,
                                                                                                                      orientation: widget.mediaOrientation == "horizontal" ? CameraOrientation.landscape : CameraOrientation.portrait,
                                                                                                                      tooltip: "",
                                                                                                                      userID: repositoryUser.userId,
                                                                                                                      videoDuration: widget.mediaDuration == null || widget.mediaDuration == 0 ? -1 : widget.mediaDuration,
                                                                                                                    );
                                                                                                                  }));
                                                                                                                  print(recordedFilePath);
                                                                                                                  if (recordedFilePath != null) {
                                                                                                                    await videoUpload(File(recordedFilePath));
                                                                                                                  }
                                                                                                                },
                                                                                                                child: Column(children: <Widget>[
                                                                                                                  Padding(padding: EdgeInsets.only(top: 0, bottom: 1), child: Container(width: 60, height: 60, decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/projects/video.png'))))),
                                                                                                                  Padding(padding: EdgeInsets.only(bottom: 15), child: Text(currentLocale == 'fr' ? "vidéo (" + (nbVideos > 999 ? "+999" : nbVideos.toString()) + ")" : "video (" + (nbVideos > 999 ? "+999" : nbVideos.toString()) + ")", style: TextStyle(color: Colors.white, fontFamily: 'Muli'))),
                                                                                                                ])))),
                                                                                                    Expanded(
                                                                                                        flex: 1,
                                                                                                        child: GestureDetector(
                                                                                                            onTap: () async {
                                                                                                              setState(() {
                                                                                                                commentScreen = true;
                                                                                                              });
                                                                                                            },
                                                                                                            child: Container(
                                                                                                                child: Column(children: <Widget>[
                                                                                                              Padding(padding: EdgeInsets.only(top: 0, bottom: 1), child: Container(width: 60, height: 60, decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/images/projects/comment.png'))))),
                                                                                                              Padding(padding: EdgeInsets.only(bottom: 15), child: Text(currentLocale == 'fr' ? "message (" + (nbComments > 999 ? "+999" : nbComments.toString()) + ")" : "message (" + (nbComments > 99 ? "+999" : nbComments.toString()) + ")", style: TextStyle(color: Colors.white, fontFamily: 'Muli'))),
                                                                                                            ])))),
                                                                                                  ])
                                                                                          ]);
                                                                                        })))))),
                                                  ],
                                                ),
                                              )),
                                        ));
                          }
                        }
                      /***********************/
                    }
                  }));
        });
  }
}
