import 'dart:math';

import 'package:flutter/foundation.dart';

class UploadRequest {
  int id;
  String file;
  String status;

  List<String> whoTags;
  List<String> whatTags;

  UploadRequest({
    this.id,
    this.file,
    this.status = 'uploading',
  });
}

class CameraBloc extends ChangeNotifier {
  int _counter = 0;
  Map<int, UploadRequest> _uploadRequests = {};

  int upload(String file) {
    int requestID = _counter++;

    var request = new UploadRequest(
      id: requestID,
      file: file,
    );

    Future.delayed(Duration(seconds: Random().nextInt(8) + 2), () {
      var request = _uploadRequests[requestID];

      if (request.whoTags != null && request.whatTags != null) {
        request.status = 'done';
      } else {
        request.status = 'uploaded';
      }

      notifyListeners();
    });

    _uploadRequests[requestID] = request;

    notifyListeners();

    return requestID;
  }

  void setTags(int id, List<String> whoTags, List<String> whatTags) {
    var request = _uploadRequests[id];

    request.whoTags = whoTags;
    request.whatTags = whatTags;

    if (request.status == 'uploaded') {
      request.status = 'done';
    }

    notifyListeners();
  }
}
