import 'dart:typed_data';

import 'package:Getinshoot/utils/app_colors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:video_player/video_player.dart';
import 'package:toast/toast.dart';

class MediaViewerPage extends StatefulWidget {
  final media;

  MediaViewerPage({Key key, this.media}) : super(key: key);

  @override
  _MediaViewerPageState createState() => _MediaViewerPageState();
}

class _MediaViewerPageState extends State<MediaViewerPage> {
  VideoPlayerController _controller;
  bool savingFile = false;

  @override
  void initState() {
    super.initState();
    if (widget.media['format'] == "mp4") {
      _controller = VideoPlayerController.network(widget.media['fileURL'])
        ..initialize().then((_) {
          print(_controller.value.duration);
          _controller.play();
          setState(() {});
        });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0, 0, 0, 0),
        ),
        body: Container(
          color: widget.media['type'] == "comment"
              ? AppColor.green
              : AppColor.blue,
          child: Center(
              child: widget.media['type'] == "comment"
                  ? Padding(
                      child: Text(
                        widget.media["comment"],
                        style: TextStyle(
                            fontSize: 30,
                            fontFamily: "Muli",
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                    )
                  : widget.media['format'] == "jpg"
                      ? CachedNetworkImage(
                          imageUrl: widget.media["fileURL"],
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        )
                      : _controller.value.initialized
                          ? GestureDetector(
                              onTap: () {
                                setState(() {
                                  _controller.value.isPlaying
                                      ? _controller.pause()
                                      : _controller.play();
                                });
                              },
                              child: AspectRatio(
                                child: VideoPlayer(_controller),
                                aspectRatio: _controller.value.aspectRatio,
                              ),
                            )
                          : CircularProgressIndicator()),
        ),
        floatingActionButton: widget.media['type'] != "comment"
            ? Padding(
                padding: const EdgeInsets.only(bottom: 30.0),
                child: FloatingActionButton(
                  onPressed: () async {
                    setState(() {
                      if (savingFile) {
                        savingFile = false;
                      } else {
                        savingFile = true;
                      }
                    });
                    PermissionStatus storagePermissionStatus =
                        await PermissionHandler()
                            .checkPermissionStatus(PermissionGroup.storage);
                    if (storagePermissionStatus.value == 0) {
                      await PermissionHandler()
                          .requestPermissions([PermissionGroup.storage]);
                    }
                    if (widget.media['type'] == "photo") {
                      var response = await Dio().get(widget.media['fileURL'],
                          options: Options(responseType: ResponseType.bytes));
                      final result = await ImageGallerySaver.saveImage(
                          Uint8List.fromList(response.data));
                      print('RESULT');
                      print(result);
                      Toast.show("Yeah !", context,
                          duration: Toast.LENGTH_LONG,
                          backgroundRadius: 15,
                          backgroundColor: AppColor.red,
                          gravity: Toast.CENTER);
                    } else {
                      var appDocDir = await getTemporaryDirectory();
                      String savePath = appDocDir.path + "/temp.mp4";
                      await Dio().download(widget.media['fileURL'], savePath);
                      await ImageGallerySaver.saveFile(savePath);
                      Toast.show("Yeah !", context,
                          duration: Toast.LENGTH_LONG,
                          backgroundRadius: 15,
                          backgroundColor: AppColor.red,
                          gravity: Toast.CENTER);
                    }
                    setState(() {
                      savingFile = false;
                    });
                  },
                  child: Icon(
                    savingFile ? Icons.check : Icons.file_download,
                    color: Colors.white,
                  ),
                  backgroundColor: savingFile ? AppColor.red : AppColor.red,
                ),
              )
            : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat);
  }
}
