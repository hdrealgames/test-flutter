/// Packages
//import 'dart:io';
//import 'dart:async';
//import 'dart:math';
//import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';

//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
//import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/pages/events/eventDetailPage.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

/// Bloc event
//import 'package:flutter_bloc/flutter_bloc.dart';


/// Class EventWidget
///   Display a 'item' widget on a list for a specific event
class EventWidget extends StatefulWidget {
  final String documentId;
  final String name;
  final String image;
  final String host; // name of the host
  final String startDate;
  final String endDate;
  final int guests; // counter

  final String imageURL;
  final String thumbnailURL;

  final DocumentSnapshot document;

  final String invitationId;
  final bool joined;
  final bool participated;

  final String mediaorientation;
  final int mediabyguest;
  final int mediaduration;

  EventWidget({Key key, @required this.name, @required this.documentId, this.image, this.host, this.startDate, this.endDate, this.guests, this.document, this.imageURL, this.thumbnailURL, this.invitationId, this.joined, this.participated, @required this.mediaorientation, @required this.mediaduration, @required this.mediabyguest}) : super(key: key);

  @override
  _EventWidgetState createState() => _EventWidgetState();
}

class _EventWidgetState extends State<EventWidget> with WidgetsBindingObserver {
  //GlobalKey _keyWidgetHomeMenu = GlobalKey();
  String _imageUrl;
  bool initialized = false;

  @override
  void initState() {
    super.initState();

    if(!initialized) {
      initialized = true;
      if(widget.thumbnailURL!=null && widget.thumbnailURL!=""){
          _imageUrl = widget.thumbnailURL;
      } else if ( widget.image != null && widget.image != "" && (_imageUrl == null || _imageUrl == "")) {
          //debugPrint("======================= IMAGE ========================");
          //debugPrint("events/" + widget.documentId + "/" + widget.image);
          //debugPrint("================================================");
        FirebaseStorage.instance.ref().child(
            "events/" + widget.documentId + "/" + widget.image).getDownloadURL().then((url) {
          //debugPrint(url);
          setState(() {
            _imageUrl = url;
          });
        }).catchError((e) {

        });
        //debugPrint("================================================");
        //debugPrint("================================================");
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child:Padding(
        padding: EdgeInsets.only(top: 10.0, bottom: 1.0),
        child:Container(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 175,
                  width: 175,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    image: DecorationImage(
                      image: _imageUrl!= null ? NetworkImage(_imageUrl) : AssetImage('assets/images/default.jpg'),
                      fit: BoxFit.cover
                    )
                  ),
                ),
                /**Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Text(
                          "CREATED BY "+widget.host.toUpperCase(),
                          style: TextStyle(color: AppColor.grey, fontSize: 11.0, fontFamily: 'Muli', ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          children: [
                            Icon(FontAwesomeIcons.users, color: AppColor.grey, size: 18,),
                            Text("  "+widget.guests.toString()+" ", style: TextStyle(color: AppColor.grey, fontSize: 18)),
                            Icon(FontAwesomeIcons.film, color: AppColor.grey, size: 18, ),
                            Text(" 57", style: TextStyle(color: AppColor.grey, fontSize: 18,)),
                          ]
                        )
                      ),
                    ],
                  )
                ),**/
                Container(
                  alignment: Alignment.center,
                  height: 37,
                  width: 175,
                  child: Padding(
                    padding: EdgeInsets.only(left: 2, right: 2, top: 2, bottom: 0),
                    child: Container(
                      child: Center( child: Text(widget.name, textAlign: TextAlign.center, style: TextStyle(fontSize: 15.0, fontFamily: 'Muli', color: Colors.white ))),
                    )
                  )
                ),
                /**Container(
                  child: new Text(
                    "END TIME",
                    textAlign: TextAlign.left,
                    style: TextStyle(color: AppColor.grey),
                  ),
                ),
                Container(
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "",
                      children: [
                        TextSpan(text: "00 j", style: TextStyle(color: AppColor.blue)),
                        TextSpan(text: "|", style: TextStyle(color: AppColor.grey)),
                        TextSpan(text: "00 h", style: TextStyle(color: AppColor.blue)),
                        TextSpan(text: "|", style: TextStyle(color: AppColor.grey)),
                        TextSpan(text: "00 min", style: TextStyle(color: AppColor.blue)),
                      ]
                    )
                  )
                ),**/
                /**
                Opacity(
                  opacity: 0,
                  child: Container(
                    child: RaisedButton(
                      child: Text("Participer"),
                      color: AppColor.green,
                      onPressed: (){

                      },
                    ),
                  )
                )**/
              ]
            )
          ),
        )
      ),
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return EventDetailPage(
              document: widget.document,
              documentId: widget.documentId,
              name: widget.name ?? '',
              host: widget.host ?? '',
              guests: widget.guests ?? 0,
              image: widget.image ?? '',
              imageURL: _imageUrl ?? widget.imageURL ?? null,
              thumbnailURL: widget.thumbnailURL ?? null,
              invitationId: widget.invitationId ?? null,
              joined: widget.joined ?? false,
              participated: widget.participated ?? false,
              mediaOrientation: widget.mediaorientation ?? 'both',
              mediaDuration: widget.mediaduration ?? 0,
              mediaByGuest: widget.mediabyguest ?? 0,
            );
          }),
        );
      }
    );
  }
}
