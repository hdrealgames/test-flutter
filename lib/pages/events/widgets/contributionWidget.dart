/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:Getinshoot/utils/app_colors.dart';

//import 'package:transparent_image/transparent_image.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';

class ContributionWidget extends StatefulWidget {
  final String image;
  final String path;
  final String fileURL;
  final String file;
  final String type;

  final DocumentSnapshot document;
  final String documentId;

  ContributionWidget({Key key, this.image, this.path, this.type, this.documentId,this.file, this.fileURL, this.document}) : super(key: key);

  @override
  _ContributionWidgetState createState() => _ContributionWidgetState();
}

class _ContributionWidgetState extends State<ContributionWidget> with WidgetsBindingObserver {
  //GlobalKey _keyWidgetHomeMenu = GlobalKey();
  String _imageUrl;
  bool initialized = false;

  @override
  void initState() {
    super.initState();

    if(!initialized) {
      initialized = true;
      if (widget.type!="video" && widget.documentId!=null && widget.image != "" &&
          (_imageUrl == null || _imageUrl == "")) {
        //debugPrint("======================= IMAGE CONTRIBUTION ========================");
        //debugPrint("events/" + widget.documentId + "/" + widget.image);
        //debugPrint("================================================");

        FirebaseStorage.instance.ref().child(
            "events/" + widget.documentId + "/photos/thumb_500x500_" + widget.image).getDownloadURL().then((
            url) {
          //debugPrint("URL CONTRIB : "+url);
          setState(() {
            _imageUrl = url;
          });
        }).catchError((e) {

        });
        //debugPrint("================================================");
        //debugPrint("================================================");

      }
    }

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child:Container(
          alignment: Alignment.center,
          /**decoration: BoxDecoration(
            color: Colors.blueGrey.withOpacity(0.75),
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),**/
          child: Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 1.0, color: AppColor.green)),
              /**image: DecorationImage(
                  image: widget.type!="video" && _imageUrl!= null ? NetworkImage(_imageUrl) : AssetImage("assets/icons/icon.png"),
                  fit: BoxFit.cover
              )**/
            ),
            child: Padding(
              padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
              child: Row(
                children: <Widget>[
                  Text("TEST")
                ],
              )
            )
          ),
        )
    );
  }
}
