import 'package:flutter/material.dart';

class Tag extends StatelessWidget {
  final String id;
  final IconData icon;
  final String name;
  final bool selected;
  final void Function(String id) action;

  const Tag({
    Key key,
    @required this.id,
    @required this.icon,
    @required this.name,
    this.selected = false,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        action(id);
      },
      child: Column(
        children: <Widget>[
          AnimatedContainer(
            duration: Duration(milliseconds: 150),
            padding: EdgeInsets.all(4),
            margin: EdgeInsets.only(top: 10, right: 6, left: 6, bottom: 2),
            decoration: BoxDecoration(
              border: Border.all(
                color: selected ? Colors.white.withOpacity(0.8) : Colors.transparent,
                width: 2,
              ),
              borderRadius: BorderRadius.circular(12),
            ),
            child: Icon(
              icon,
              size: 42,
              color: Colors.white,
            ),
          ),
          SizedBox(
            height: 0,
          ),
          Text(
            name,
            style: Theme.of(context).textTheme.caption.apply(color: Colors.white),
          )
        ],
      ),
    );
  }
}
