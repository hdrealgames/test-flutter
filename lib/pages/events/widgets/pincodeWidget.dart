/// Packages
//import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';

import 'package:Getinshoot/utils/app_colors.dart';

//import 'package:flutter/services.dart';
//import 'package:connectivity/connectivity.dart';

/// Pin code Widget
class PinCodeTextField extends StatefulWidget {
  /// length of how many cells there should be
  final int length;

  /// this stream will call _onData when something is added
  final Stream triggerStream;

  /// this callback will be triggered if there is no error and will return the complete string
  final ValueChanged<String> onDone;

  /// this callback will return value if there is any error or not
  final ValueChanged<bool> onError;

  /// Pincode Fields
  PinCodeTextField({ @required this.length, @required this.triggerStream, @required this.onDone, @required this.onError });

  @override
  _PinCodeTextFieldState createState() => _PinCodeTextFieldState();
}

class _PinCodeTextFieldState extends State<PinCodeTextField> {
  List<FocusNode> focusNodes;
  List<TextEditingController> controllers;
  List<String> errors;
  StreamSubscription streamSubscription;

  Widget _nodeAt(int index) {
    return Flexible(
      flex: 2,
      child: TextField(
        cursorColor: Colors.white,
        inputFormatters: [
          LengthLimitingTextInputFormatter(1),
        ],
        autofocus: index<1 ? true : false,
        onChanged: (value) {
          if (value.length == 0) {
            if (index != 0) {
              FocusScope.of(context).requestFocus(focusNodes[index - 1]);
              if(index<widget.length) controllers[index] = null;
            } else {
              FocusScope.of(context).requestFocus(FocusNode());
            }
          } else if (value.length == 1) {
            if (errors[index] != null) {
              setState(() {
                errors[index] = null;
              });
            }
            if (index != widget.length - 1) {
              FocusScope.of(context).requestFocus(focusNodes[index + 1]);
            } else {
              FocusScope.of(context).requestFocus(FocusNode());
            }
          }
        },
        controller: controllers[index],
        focusNode: focusNodes[index],
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 21, color: AppColor.green, fontWeight: FontWeight.bold),
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 2,
              color: AppColor.green.withOpacity(0.3),
            ),
          ),
          border: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 2,
              color: AppColor.green.withOpacity(0.3),
            ),
          ),
          errorText: errors[index],
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 2,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _nodes() {
    var nodes = <Widget>[];

    for (int i = 0; i < widget.length; i++) {
      nodes.add(_nodeAt(i));
      if (i != widget.length - 1) {
        nodes.add(Spacer());
      }
    }
    return nodes;
  }

  bool hasError() {
    bool errorFound = false;

    for (int i = 0; i < controllers.length; i++) {
      if (controllers[i].text == null || controllers[i].text.length == 0) {
        errorFound = true;
        setState(() {
          errors[i] = "";
        });
      } else {
        if (errors[i] != null) {
          setState(() {
            errors[i] = null;
          });
        }
      }
    }

    return errorFound;
  }

  String resultString() {
    String result = "";

    for (int i = 0; i < controllers.length; i++) {
      result += controllers[i].text;
    }

    return result;
  }

  void _onData(dynamic data) {
    if (hasError()) {
      widget.onError(true);
    } else {
      widget.onDone(resultString());
    }
  }

  @override
  void initState() {
    focusNodes = List<FocusNode>.generate(widget.length, (index) => FocusNode());
    controllers = List<TextEditingController>.generate(widget.length, (index) => TextEditingController());
    errors = List<String>.generate(widget.length, (index) => null);
    streamSubscription = widget.triggerStream.listen(_onData);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: _nodes(),
    );
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    for (int i = 0; i < controllers.length; i++) {
      controllers[i].dispose();
    }
    super.dispose();
  }
}
