/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/main.dart';

/// Colors
import 'package:Getinshoot/utils/app_colors.dart';

/// Navigation
import 'package:Getinshoot/widgets/headerWidget.dart';
//import 'package:Getinshoot/widgets/menuWidget.dart';

/// Pages
import 'package:Getinshoot/pages/events/widgets/eventWidget.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';


/// EventsPage
///   display events of a user
class EventsPage extends StatefulWidget {
  @override
  _EventsPageState createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  //GlobalKey _keyWidgetEventsMenu = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderWidget.get(context),
                //drawer: MenuWidget.get(context),
                body: Container(
                    decoration: BoxDecoration(color: AppColor.blue),
                    child: StreamBuilder<QuerySnapshot>(
                        stream: Firestore.instance.collection('events').where('hostid', isEqualTo: repositoryUser.userId ?? "34235seerez325sdfsf").snapshots(),
                        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasError) {
                            // TODO : Display message
                            return Text('Error: ${snapshot.error}');
                          }

                          switch (snapshot.data) {
                            case null:
                              return Container(); // TODO
                            default:
                              //if(snapshot.data.documents.length<=1){
                                // TODO
                              //}

                              return Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 20),
                                  child: ListView.builder(
                                    itemCount: snapshot.data.documents.length,
                                    itemBuilder: (context, index){
                                      return EventWidget(
                                            document: snapshot.data.documents[index],
                                            documentId: snapshot.data.documents[index].documentID,
                                            name: snapshot.data.documents[index]['name'] ?? '',
                                            host: snapshot.data.documents[index]['host'] ?? '',
                                            guests: snapshot.data.documents[index]['guests'] ?? 0,
                                            image: snapshot.data.documents[index]['image'] ?? '',
                                            mediaorientation: snapshot.data.documents[index]['mediaorientation'] ?? 'both',
                                            mediaduration: snapshot.data.documents[index]['mediaduration'] ?? 0,
                                            mediabyguest: snapshot.data.documents[index]['mediabyguest'] ?? 0,
                                      );
                                    },
                                  )
                              );
                          }
                        }
                    )
                )
            )
        )
    );
  }
}
