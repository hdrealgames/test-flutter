import 'package:Getinshoot/main.dart';
import 'package:Getinshoot/models/eventNotification.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:Getinshoot/widgets/headerWidget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter_launcher_icons/xml_templates.dart';
import 'package:Getinshoot/widgets/shooterzInfo.dart';

class NotificationHistoryPage extends StatefulWidget {
  @override
  _NotificationHistoryPageState createState() =>
      _NotificationHistoryPageState();
}

class _NotificationHistoryPageState extends State<NotificationHistoryPage>
    with WidgetsBindingObserver {
  GlobalKey<ScaffoldState> _keyScaffoldSettingsPage =
      GlobalKey<ScaffoldState>();
  bool loading = true;
  List<EventNotification> notifications = [];

  @override
  void initState() {
    this.setCurrentScreen();
    super.initState();
    List<EventNotification> tmpNotifications = [];
    Firestore.instance
        .collection("notifications")
        .where("recipients", arrayContains: repositoryUser.userId)
        .orderBy("createdDate", descending: true)
        .limit(15)
        .getDocuments()
        .then((snapshot) async {
      await Future.forEach(snapshot.documents, (document) async {
        EventNotification notification;
        if (document.data['senderId'] == "FdTnM07Vw5PGWxd9sACCU4tSraw2") {
          notification = EventNotification.fromFirebase(
              document.data,
              "Getinshoot",
              "https://lh3.googleusercontent.com/hj9bYR44AvUOtL4d0ENilRwoCP14zJXPi6YWddKDKFd4kptscm9WSr6PF5jw-aw12j0=s360-rw");
        } else {
          Map<String, dynamic> eventInfo =
              await fetchEventDetails(document.data['eventId']);
          notification = EventNotification.fromFirebase(
              document.data, eventInfo['name'], eventInfo['imageURL']);
        }
        tmpNotifications.add(notification);
      });
      setState(() {
        notifications = tmpNotifications;
      });
      setState(() {
        loading = false;
      });
    });
  }

  Future fetchEventDetails(eventId) async {
    var eventDocument =
        await Firestore.instance.collection("events").document(eventId).get();
    return eventDocument.data;
  }

  void setCurrentScreen() async {
    await analytics.setCurrentScreen(
        screenName: "Notifications History Page",
        screenClassOverride: 'notificationHistoryPage');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                backgroundColor: AppColor.blue,
                appBar: HeaderWidget.get(context),
                key: _keyScaffoldSettingsPage,
                body: Center(
                    child: Wrap(children: <Widget>[
                  ShooterzInfo(),
                  loading
                      ? CircularProgressIndicator()
                      : Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: ListView(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            children: notifications.length > 0
                                ? [
                                    Padding(
                                      child: Text(
                                        "notifications",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 24,
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.normal),
                                      ),
                                      padding:
                                          EdgeInsets.symmetric(vertical: 20),
                                    ),
                                    ...notifications.map((notification) {
                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          NotificationItem(
                                              eventName: notification.eventName,
                                              content: notification.content,
                                              createdDate:
                                                  notification.createdDate,
                                              imageURL: notification.imageURL),
                                          NotificationSeparator(),
                                        ],
                                      );
                                    })
                                  ]
                                : [
                                    Text(
                                      currentLocale == "fr"
                                          ? "Vous n'avez reçu aucune notification pour le moment."
                                          : "You didn't receive any notifications for now.",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Muli"),
                                    )
                                  ],
                          ),
                        )
                ])))));
  }
}

class NotificationSeparator extends StatelessWidget {
  const NotificationSeparator({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Divider(
      height: 25.0,
      color: Color.fromRGBO(255, 255, 255, 0.1),
    );
  }
}

class NotificationItem extends StatefulWidget {
  final eventName, content, imageURL;
  final DateTime createdDate;

  const NotificationItem(
      {Key key, this.eventName, this.content, this.createdDate, this.imageURL})
      : super(key: key);

  @override
  _NotificationItemState createState() => _NotificationItemState();
}

class _NotificationItemState extends State<NotificationItem> {
  String computeElapsedTime(DateTime then) {
    DateTime now = DateTime.now();
    final minutesDifference = now.difference(then).inMinutes;
    final hoursDifference = now.difference(then).inHours;
    final daysDifference = now.difference(then).inDays;
    if (minutesDifference == 0 && daysDifference == 0 && hoursDifference == 0) {
      return "1 min";
    } else if (hoursDifference == 0 && daysDifference == 0) {
      return "$minutesDifference min";
    } else if (daysDifference == 0) {
      return "$hoursDifference h";
    } else {
      return currentLocale == "fr"
          ? "$daysDifference jours"
          : "$daysDifference days";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 15, right: 10, top: 15, bottom: 15),
        decoration: new BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 0.1),
            borderRadius: new BorderRadius.circular(15)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: Container(
              width: 200.0,
              child: Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      shape: BoxShape.rectangle,
                      image: DecorationImage(
                          image: this.widget.imageURL != null
                              ? NetworkImage(this.widget.imageURL)
                              : AssetImage('assets/images/default.jpg'),
                          fit: BoxFit.fill)),
                ),
              ),
            )),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    this.widget.eventName,
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontFamily: 'Poppins'),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    this.widget.content,
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.white70,
                        fontFamily: 'Muli'),
                  ),
                ],
              ),
              flex: 4,
            ),
            Expanded(
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      computeElapsedTime(this.widget.createdDate),
                      style: TextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.normal,
                          color: Color.fromRGBO(255, 255, 255, 0.5),
                          fontFamily: 'Muli'),
                    ),
                  ],
                ),
              ),
              flex: 1,
            ),
          ],
        ));
  }
}
