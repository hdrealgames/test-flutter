import 'package:chewie/chewie.dart';
import 'package:firebase_storage/firebase_storage.dart';

/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/main.dart';

/// Menu
import 'package:Getinshoot/widgets/headerWidget.dart';

import 'package:Getinshoot/utils/app_colors.dart';

/// Database
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:video_player/video_player.dart';

class ContactPage extends StatefulWidget {
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> with WidgetsBindingObserver {
  final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _keyScaffoldContactPage = GlobalKey<ScaffoldState>();

  /// Form field controllers
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  /// Form field focus nodes
  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();

  bool _error = false;
  bool _submitting = false;
  bool _submitted = false;

  VideoPlayerController _controller;
  ChewieController chewieController;

  bool videoIsLoading = true;
  bool videoIsPlaying = false;

  void setCurrentScreen() async {
    await analytics.setCurrentScreen(
        screenName: "contact Page", screenClassOverride: 'contactPage');
  }

  @override
  void initState() {
    this.setCurrentScreen();
    super.initState();
    StorageReference ref =
        FirebaseStorage.instance.ref().child("mobileapp/presentation.mp4");
    ref.getDownloadURL().then((url) {
      _controller = VideoPlayerController.network(url.toString())
        ..initialize().then((_) {
          print(_controller.value.duration);
          chewieController = ChewieController(
            videoPlayerController: _controller,
            aspectRatio: _controller.value.aspectRatio,
          );
          setState(() {
            videoIsLoading = false;
          });
        });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderWidget.get(context),
                body: SingleChildScrollView(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 30.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                                currentLocale == "fr"
                                    ? "simple, rapide et astucieux"
                                    : "simple, fast and clever",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Poppins",
                                    fontSize: 24,
                                    fontWeight: FontWeight.normal)),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      color: AppColor.green,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Center(child: Text("1")),
                                ),
                                Expanded(
                                  child: Text(
                                      currentLocale == "fr"
                                          ? "Créer votre projet sur la plateforme Getinshoot"
                                          : "Create your project on Getinshoot's platform",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Muli",
                                        fontSize: 15,
                                      )),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      color: AppColor.green,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Center(child: Text("2")),
                                ),
                                Expanded(
                                  child: Text(
                                      currentLocale == "fr"
                                          ? "Envoyez le getin-code à vos invités"
                                          : "Share your getin-code to your guests",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Muli",
                                        fontSize: 15,
                                      )),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 20,
                                  height: 20,
                                  margin: EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      color: AppColor.green,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5))),
                                  child: Center(child: Text("3")),
                                ),
                                Expanded(
                                  child: Text(
                                      currentLocale == "fr"
                                          ? "Les shooterz utilisent l'appli pour contribuer"
                                          : "Shooterz will use the app to contribute",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Muli",
                                        fontSize: 15,
                                      )),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      chewieController != null
                          ? Chewie(
                              controller: chewieController,
                            )
                          : Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 50.0),
                              child: Center(child: CircularProgressIndicator()),
                            ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        child: Text(
                            currentLocale == "fr"
                                ? "nous contacter"
                                : "contact us",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Poppins",
                                fontSize: 25,
                                fontWeight: FontWeight.normal)),
                        padding: EdgeInsets.only(left: 30),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                              left: 30, right: 30, top: 10, bottom: 15),
                          child: Container(
                              decoration: BoxDecoration(
                                border:
                                    Border.all(width: 1, color: AppColor.green),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                              ),
                              child: Form(
                                  key: _formKey,
                                  child: Wrap(children: <Widget>[
                                    SizedBox(
                                      height: 20,
                                    ),
                                    !_submitted
                                        ? Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 0, horizontal: 25),
                                            child: TextFormField(
                                              cursorColor: Colors.white,
                                              style: new TextStyle(
                                                  color: Colors.white),
                                              keyboardType: TextInputType.text,
                                              controller: _firstNameController,
                                              textInputAction:
                                                  TextInputAction.next,
                                              focusNode: _firstNameFocus,
                                              onFieldSubmitted: (term) {
                                                _fieldFocusChange(
                                                    context,
                                                    _firstNameFocus,
                                                    _lastNameFocus);
                                              },
                                              decoration: InputDecoration(
                                                labelText: currentLocale == 'fr'
                                                    ? 'Nom'
                                                    : 'Firstname',
                                                labelStyle: TextStyle(
                                                    color: AppColor.grey,
                                                    fontFamily: "Muli"),
                                                hintStyle: TextStyle(
                                                    color: AppColor.grey),
                                                focusedBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                border: new UnderlineInputBorder(
                                                    borderSide: new BorderSide(
                                                        color: AppColor.grey
                                                            .withOpacity(0.8))),
                                                enabledBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: AppColor
                                                                    .grey
                                                                    .withOpacity(
                                                                        0.8))),
                                              ),
                                              autovalidate: true,
                                              validator: (value) {
                                                if (_error && value.isEmpty) {
                                                  return currentLocale == 'fr'
                                                      ? 'Veuillez saisir votre nom'
                                                      : 'Please enter your firstname';
                                                }
                                                return null;
                                                //return _error && _firstNameController.text.length<=0 ?  currentLocale =='fr' ? 'Veuillez saisir votre nom' : 'Please enter your firstname' : null;
                                              },
                                            ))
                                        : Container(),
                                    !_submitted
                                        ? Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 0, horizontal: 25),
                                            child: TextFormField(
                                              cursorColor: Colors.white,
                                              style: new TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: "Muli"),
                                              keyboardType: TextInputType.text,
                                              controller: _lastNameController,
                                              textInputAction:
                                                  TextInputAction.next,
                                              focusNode: _lastNameFocus,
                                              onFieldSubmitted: (term) {
                                                _fieldFocusChange(
                                                    context,
                                                    _lastNameFocus,
                                                    _phoneFocus);
                                              },
                                              decoration: InputDecoration(
                                                labelText: currentLocale == 'fr'
                                                    ? 'Prénom'
                                                    : 'Lastname',
                                                labelStyle: TextStyle(
                                                    color: AppColor.grey,
                                                    fontFamily: "Muli"),
                                                hintStyle: TextStyle(
                                                    color: AppColor.grey),
                                                focusedBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                border: new UnderlineInputBorder(
                                                    borderSide: new BorderSide(
                                                        color: AppColor.grey
                                                            .withOpacity(0.8))),
                                                enabledBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: AppColor
                                                                    .grey
                                                                    .withOpacity(
                                                                        0.8))),
                                              ),
                                              autovalidate: true,
                                              validator: (value) {
                                                if (_error && value.isEmpty) {
                                                  return currentLocale == 'fr'
                                                      ? 'Veuillez saisir votre prénom'
                                                      : 'Please enter your lastname';
                                                }
                                                return null;
                                                //return _error && _lastNameController.text.length<=0 ?  currentLocale =='fr' ? 'Veuillez saisir votre prénom' : 'Please enter your lastname' : null;
                                              },
                                            ))
                                        : Container(),
                                    !_submitted
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                top: 0,
                                                bottom: 15,
                                                right: 25,
                                                left: 25),
                                            child: TextFormField(
                                              cursorColor: Colors.white,
                                              style: new TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: "Muli"),
                                              keyboardType: TextInputType.phone,
                                              controller: _phoneController,
                                              textInputAction:
                                                  TextInputAction.done,
                                              focusNode: _phoneFocus,
                                              onFieldSubmitted: (term) {
                                                _phoneFocus.unfocus();
                                              },
                                              decoration: InputDecoration(
                                                labelText: currentLocale == 'fr'
                                                    ? 'Téléphone'
                                                    : 'Phone number',
                                                labelStyle: TextStyle(
                                                    color: AppColor.grey,
                                                    fontFamily: "Muli"),
                                                hintStyle: TextStyle(
                                                    color: AppColor.grey),
                                                focusedBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                border: new UnderlineInputBorder(
                                                    borderSide: new BorderSide(
                                                        color: AppColor.grey
                                                            .withOpacity(0.8))),
                                                enabledBorder:
                                                    new UnderlineInputBorder(
                                                        borderSide:
                                                            new BorderSide(
                                                                color: AppColor
                                                                    .grey
                                                                    .withOpacity(
                                                                        0.8))),
                                              ),
                                              autovalidate: true,
                                              validator: (value) {
                                                if (_error && value.isEmpty) {
                                                  return currentLocale == 'fr'
                                                      ? 'Veuillez saisir votre numéro'
                                                      : 'Please enter your phone number';
                                                }
                                                return null;
                                                //return _error && _phoneController.text.length<=0 ?   : null;
                                              },
                                            ))
                                        : Container(),
                                    _submitting && !_submitted
                                        ? Center(
                                            child: Padding(
                                            padding: EdgeInsets.only(
                                                top: 10,
                                                bottom: 18,
                                                right: 25,
                                                left: 25),
                                            child: CircularProgressIndicator(),
                                          ))
                                        : Container(),
                                    _submitted
                                        ? Center(
                                            child: Padding(
                                            padding: EdgeInsets.only(
                                                top: 2,
                                                bottom: 25,
                                                right: 25,
                                                left: 25),
                                            child: Text(
                                                currentLocale == "fr"
                                                    ? "Votre demande a été transmise à notre équipe."
                                                    : "Your request have been send to Getinshoot team.",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 17,
                                                    fontFamily: "Muli",
                                                    fontWeight:
                                                        FontWeight.bold)),
                                          ))
                                        : Container(),
                                    !_submitted && !_submitting
                                        ? Center(
                                            child: Padding(
                                                padding: EdgeInsets.only(
                                                    top: 10,
                                                    bottom: 18,
                                                    right: 25,
                                                    left: 25),
                                                child: Container(
                                                  child: Center(
                                                      child: GestureDetector(
                                                          onTap: () {
                                                            _formKey
                                                                .currentState
                                                                .validate();
                                                            try {
                                                              _phoneFocus
                                                                  .unfocus();
                                                              _firstNameFocus
                                                                  .unfocus();
                                                              _lastNameFocus
                                                                  .unfocus();
                                                            } catch (e) {}

                                                            debugPrint(
                                                                "TAP MESSAGE");

                                                            if (_firstNameController.text.length > 0 &&
                                                                _lastNameController
                                                                        .text
                                                                        .length >
                                                                    0 &&
                                                                _phoneController
                                                                        .text
                                                                        .length >
                                                                    0) {
                                                              _error = false;
                                                              _submitting =
                                                                  true;

                                                              var message = {
                                                                'userId':
                                                                    repositoryUser
                                                                        .userId,
                                                                'to': 'Melanie',
                                                                'from':
                                                                    repositoryUser
                                                                        .email,
                                                                'message': 'CALL REQUEST : ' +
                                                                    _firstNameController
                                                                        .text +
                                                                    ' ' +
                                                                    _lastNameController
                                                                        .text +
                                                                    ' ( ' +
                                                                    _phoneController
                                                                        .text +
                                                                    ' )',
                                                                'createdAt':
                                                                    new DateTime
                                                                        .now(),
                                                              };

                                                              debugPrint(
                                                                  "PREPARE SEND MESSAGE");

                                                              // Save
                                                              Firestore.instance
                                                                  .collection(
                                                                      'messages')
                                                                  .add(message)
                                                                  .catchError(
                                                                      (e) {
                                                                debugPrint(
                                                                    "ERROR : " +
                                                                        e.toString());
                                                                _keyScaffoldContactPage
                                                                    .currentState
                                                                  ..hideCurrentSnackBar()
                                                                  ..showSnackBar(
                                                                    SnackBar(
                                                                      duration: Duration(
                                                                          seconds:
                                                                              4),
                                                                      content:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.spaceBetween,
                                                                        children: [
                                                                          Text(currentLocale == 'fr'
                                                                              ? 'Erreur inattendue. Veuillez réessayer'
                                                                              : 'An unexpected error occured. Please retry'),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  );
                                                              }).whenComplete(
                                                                      () {
                                                                debugPrint(
                                                                    "MESSAGE SENT");
                                                                _submitted =
                                                                    true;
                                                                setState(() {
                                                                  _submitted =
                                                                      true;
                                                                });
                                                              });
                                                            }
                                                          },
                                                          child: Container(
                                                            decoration: BoxDecoration(
                                                                color: AppColor
                                                                    .green,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            30)),
                                                            child: Container(
                                                              padding: const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      21,
                                                                  vertical: 12),
                                                              child: Text(
                                                                currentLocale ==
                                                                        'fr'
                                                                    ? 'Je souhaite être rappelé'
                                                                        .toUpperCase()
                                                                    : 'Call me'
                                                                        .toUpperCase(),
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 13,
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ),
                                                          ))),
                                                )),
                                          )
                                        : Container(),
                                  ])))),
                    ])))));
  }
}

_fieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}
