import 'dart:async';

import 'package:Getinshoot/auth/authentication_bloc/authentication_bloc.dart';
import 'package:Getinshoot/auth/authentication_bloc/authentication_event.dart';
import 'package:Getinshoot/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/main.dart';

/// Menu
import 'package:Getinshoot/widgets/headerWidget.dart';
// Colors
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import 'package:toast/toast.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage>
    with WidgetsBindingObserver {
  //final _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _keyScaffoldSettingsPage =
      GlobalKey<ScaffoldState>();

  static String defaultAvatar =
      "https://www.recia.fr/wp-content/uploads/2018/10/default-avatar-300x300.png";

  bool loading = true;
  bool changedUserTextInfo = false;
  User currentUser, updatedUser;
  String selectedLang = currentLocale.split('-')[0];
  String avatarUrl = defaultAvatar;

  @override
  void initState() {
    this.setCurrentScreen();
    Firestore.instance
        .collection('users')
        .document(repositoryUser.userId)
        .get()
        .then((snapshot) {
      currentUser = User.fromFirebase(snapshot.data);
      updatedUser = User.fromFirebase(snapshot.data);
      getAvatarUrl(currentUser.avatarUrl).then((url) => {
            this.setState(() {
              avatarUrl = url;
              loading = false;
            })
          });
    });
    super.initState();
  }

  void onTextInputChange(String attribute, value) {
    switch (attribute) {
      case 'username':
        this.updatedUser.username = value == "" ? null : value;
        break;
      case 'name':
        this.updatedUser.name = value == "" ? null : value;
        break;
      case 'email':
        this.updatedUser.email = value == "" ? null : value;
        break;
      default:
        print("onTextIputChange error...");
        break;
    }
    this.setState(() {
      changedUserTextInfo = !(currentUser.name == updatedUser.name &&
          currentUser.username == updatedUser.username &&
          currentUser.email == updatedUser.email);
    });
  }

  void onFabClickHandler() {
    this.saveUpdatedUser();
  }

  Future<void> saveUpdatedUser() async {
    try {
      await Firestore.instance
          .collection('users')
          .document(repositoryUser.userId)
          .updateData({
        "name": updatedUser.name,
        "username": updatedUser.username,
        "email": updatedUser.email
      });
      Toast.show("Yeah !", context,
          duration: Toast.LENGTH_LONG,
          backgroundRadius: 15,
          backgroundColor: AppColor.green,
          gravity: Toast.CENTER);
      currentUser = User(updatedUser.avatarUrl, updatedUser.username,
          updatedUser.name, updatedUser.email);
      this.setState(() {
        changedUserTextInfo = !(currentUser.name == updatedUser.name &&
            currentUser.username == updatedUser.username &&
            currentUser.email == updatedUser.email);
      });
    } catch (e) {
      Toast.show("Une erreur s'est produite.", context);
    }
  }

  Future<void> editAvatarImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    String fileName =
        image.path.split('/')[image.path.toString().split('/').length - 1];
    final StorageUploadTask uploadTask = FirebaseStorage()
        .ref()
        .child("/avatars/${repositoryUser.userId}/$fileName")
        .putFile(image);
    Toast.show("Uploading...", context);
    final StreamSubscription<StorageTaskEvent> streamSubscription =
        uploadTask.events.listen((event) {
      print('EVENT ${event.type}');
    });
    await uploadTask.onComplete;
    streamSubscription.cancel();
    await Firestore.instance
        .collection('users')
        .document(repositoryUser.userId)
        .updateData({
      "avatar": fileName,
    });
    getAvatarUrl(fileName).then((url) {
      setState(() {
        avatarUrl = url;
      });
      Toast.show("Avatar mis à jour", context);
    });
  }

  Future getAvatarUrl(String fileName) {
    if (fileName == "" || fileName == null) {
      return Future.value(defaultAvatar);
    }
    return FirebaseStorage()
        .ref()
        .child("/avatars/${repositoryUser.userId}/$fileName")
        .getDownloadURL();
  }

  void setCurrentScreen() async {
    await analytics.setCurrentScreen(
        screenName: "Settings Page", screenClassOverride: 'settingsPage');
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderWidget.get(context),
                key: _keyScaffoldSettingsPage,
                body: SingleChildScrollView(
                  child: Center(
                    child: loading
                        ? CircularProgressIndicator()
                        : Padding(
                            padding: const EdgeInsets.only(
                                top: 30.0, left: 30.0, right: 30.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () async {
                                        await storage.write(
                                            key: "storedLanguage", value: "fr");
                                        setState(() {
                                          if (selectedLang != "fr") {
                                            selectedLang = "fr";
                                            currentLocale = "fr";
                                          }
                                        });
                                        Toast.show("Bienvenue", context,
                                            duration: Toast.LENGTH_LONG,
                                            backgroundRadius: 15,
                                            backgroundColor: AppColor.green,
                                            gravity: Toast.TOP);
                                      },
                                      child: FlagSelector(
                                          selected: selectedLang == "fr",
                                          flagImage:
                                              "assets/images/lang/fr.png"),
                                    ),
                                    SizedBox(width: 10),
                                    GestureDetector(
                                        onTap: () async {
                                          await storage.write(
                                              key: "storedLanguage",
                                              value: "en");
                                          setState(() {
                                            if (selectedLang != "en") {
                                              selectedLang = "en";
                                              currentLocale = "en";
                                            }
                                          });
                                          Toast.show("Welcome", context,
                                              duration: Toast.LENGTH_LONG,
                                              backgroundRadius: 15,
                                              backgroundColor: AppColor.green,
                                              gravity: Toast.TOP);
                                        },
                                        child: FlagSelector(
                                            selected: selectedLang == "en",
                                            flagImage:
                                                "assets/images/lang/en.png")),
                                  ],
                                ),
                                SizedBox(height: 20),
                                GestureDetector(
                                    onTap: () {
                                      editAvatarImage();
                                    },
                                    child: AvatarEdit(currentUrl: avatarUrl)),
                                SizedBox(height: 40),
                                UserInfoInput(
                                    label: selectedLang == "fr"
                                        ? "pseudo"
                                        : "username",
                                    value: currentUser.username,
                                    onChanged: (value) {
                                      onTextInputChange("username", value);
                                    }),
                                SizedBox(height: 20),
                                UserInfoInput(
                                    label: selectedLang == "fr"
                                        ? "nom"
                                        : "full name",
                                    value: currentUser.name,
                                    onChanged: (value) {
                                      onTextInputChange("name", value);
                                    }),
                                SizedBox(height: 20),
                                UserInfoInput(
                                    label: "email",
                                    value: currentUser.email,
                                    onChanged: (value) {
                                      onTextInputChange("email", value);
                                    }),
                                SizedBox(height: 40),
                                UserSwitchParams(
                                    title: "notifications".toUpperCase(),
                                    content: selectedLang == "fr"
                                        ? "J'accepte de recevoir des notifications de l'application"
                                        : "I accept receiving notifications from this application"),
                                SizedBox(height: 20),
                                UserSwitchParams(
                                    title: selectedLang == "fr"
                                        ? "CGU".toUpperCase()
                                        : "TOS".toUpperCase(),
                                    content: selectedLang == "fr"
                                        ? "J'ai lu et j'accepte les conditions d'utilisation de Getinshoot"
                                        : "I read and I accept Getinshoot terms of service"),
                                SizedBox(height: 20),
                                LogOutButton(
                                    label: selectedLang == "fr"
                                        ? "Déconnexion"
                                        : "Log out")
                              ],
                            ),
                          ),
                  ),
                ),
                floatingActionButton: changedUserTextInfo
                    ? FloatingActionButton(
                        child: Icon(
                          Icons.save,
                          color: Colors.white,
                        ),
                        backgroundColor: AppColor.red,
                        onPressed: () {
                          onFabClickHandler();
                        },
                      )
                    : Container())));
  }
}

class LogOutButton extends StatefulWidget {
  final label;
  const LogOutButton({Key key, this.label}) : super(key: key);

  @override
  _LogOutButtonState createState() => _LogOutButtonState();
}

class _LogOutButtonState extends State<LogOutButton> {
  Future<void> logOut() async {
    BlocProvider.of<AuthenticationBloc>(context).dispatch(
      LoggedOut(),
    );
    Navigator.popUntil(
        context, ModalRoute.withName(Navigator.defaultRouteName));
  }

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: () async {
        await logOut();
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Icon(Icons.power_settings_new),
          SizedBox(width: 10),
          Text(widget.label),
        ],
      ),
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(18.0)),
    );
  }
}

class FlagSelector extends StatefulWidget {
  final selected, flagImage;
  const FlagSelector({Key key, this.selected, this.flagImage})
      : super(key: key);

  @override
  _FlagSelectorState createState() => _FlagSelectorState();
}

class _FlagSelectorState extends State<FlagSelector> {
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Image.asset(
        widget.flagImage,
        width: 24,
        height: 24,
        fit: BoxFit.cover,
      ),
      !widget.selected
          ? Container(
              width: 24,
              height: 24,
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Color.fromRGBO(0, 0, 0, 0.5),
              ),
            )
          : Container()
    ]);
  }
}

class UserSwitchParams extends StatefulWidget {
  final title, content;
  const UserSwitchParams({Key key, this.title, this.content}) : super(key: key);

  @override
  _UserSwitchParamsState createState() => _UserSwitchParamsState();
}

class _UserSwitchParamsState extends State<UserSwitchParams> {
  bool switchSate = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          widget.title,
          style: TextStyle(color: AppColor.grey),
        ),
        SizedBox(height: 10),
        Row(
          children: <Widget>[
            Switch(
              onChanged: (bool value) {
                setState(() {
                  switchSate = value;
                });
              },
              value: switchSate,
              activeColor: AppColor.grey,
              inactiveTrackColor: AppColor.grey,
            ),
            Expanded(
                child: Text(widget.content,
                    style: TextStyle(color: AppColor.grey)))
          ],
        )
      ],
      crossAxisAlignment: CrossAxisAlignment.start,
    );
  }
}

class UserInfoInput extends StatefulWidget {
  final label, value, onChanged;

  const UserInfoInput({Key key, this.label, this.value, this.onChanged})
      : super(key: key);

  @override
  _UserInfoInputState createState() => _UserInfoInputState();
}

class _UserInfoInputState extends State<UserInfoInput> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text(
            widget.label,
            style: TextStyle(color: AppColor.grey),
          ),
        ),
        Expanded(
          flex: 7,
          child: TextFormField(
            onChanged: widget.onChanged,
            initialValue: widget.value,
            style: TextStyle(color: Colors.white),
            cursorColor: AppColor.green,
            decoration: InputDecoration(
              labelStyle: TextStyle(color: AppColor.grey, fontFamily: "Muli"),
              hintStyle: TextStyle(color: AppColor.grey),
              focusedBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(color: AppColor.green)),
              border: new UnderlineInputBorder(
                  borderSide:
                      new BorderSide(color: AppColor.grey.withOpacity(0.8))),
              enabledBorder: new UnderlineInputBorder(
                  borderSide:
                      new BorderSide(color: AppColor.grey.withOpacity(0.8))),
            ),
          ),
        )
      ],
    );
  }
}

class AvatarEdit extends StatefulWidget {
  final currentUrl;
  const AvatarEdit({Key key, this.currentUrl}) : super(key: key);

  @override
  _AvatarEditState createState() => _AvatarEditState();
}

class _AvatarEditState extends State<AvatarEdit> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        CircleAvatar(
          backgroundImage: NetworkImage(widget.currentUrl),
          radius: 70,
        ),
        Positioned(
            bottom: 0,
            right: 0,
            child: Container(
              width: 40,
              height: 40,
              decoration: new BoxDecoration(
                  color: AppColor.red, borderRadius: BorderRadius.circular(40)),
              child: Icon(
                Icons.edit,
                color: Colors.white,
              ),
            ))
      ],
    );
  }
}
