/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:video_player/video_player.dart';

class Guideline extends StatefulWidget {
  final String video;
  final String image;
  final String title;
  final String subtitle;
  final VideoPlayerController controller;

  Guideline({
    Key key,
    @required this.video,
    @required this.title,
    @required this.image,
    this.subtitle,
    this.controller,
  }) : super(key: key);

  _GuidelineState createState() => _GuidelineState();
}

class _GuidelineState extends State<Guideline> {
  Widget _infos() {
    return Opacity(
        opacity: this.widget.controller!=null && this.widget.controller.value.isPlaying ? 0.9 : 0,
        child: Container(
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Material(
                    child: Container(
                      decoration: BoxDecoration(
                          color: AppColor.blue,
                          borderRadius: BorderRadius.circular(30),
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(left: 8, top: 2, bottom: 2, right: 8),
                        child: Text(
                          widget.title,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 31,
                            fontFamily: "Muli",
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Material(
                    child: widget.subtitle != null
                        ? Container(
                      decoration: BoxDecoration(color: AppColor.green),
                      child: Padding(
                        padding: EdgeInsets.only(left: 8, top: 2, bottom: 2, right: 8),
                        child: Text(
                          widget.subtitle,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            fontFamily: "Muli",
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ) : Container(
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    try {
      List<Widget> children;

      if (widget.controller != null && widget.controller.value.isPlaying) {
        children = [
          VideoPlayer(widget.controller),
          _infos(),
        ];
      } else {
        children = [
          Opacity(
            opacity: 0.39,
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/guidelines/videos/${widget.image}'),
                    fit: BoxFit.contain
                )
              )
            ),
          ),
        ];
      }

      return Container(
        margin: EdgeInsets.all(16),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Stack(
            children: children,
          ),
        ),
      );
    } catch (ex) {
      print(ex);
      return Container();
    }
  }
}
