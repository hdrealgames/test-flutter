/// Packages
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/widgets/headerWidget.dart';
//import 'package:Getinshoot/widgets/menuWidget.dart';

import 'package:Getinshoot/pages/guidelines/widgets/guideline.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
//import 'package:transformer_page_view/transformer_page_view.dart';
import 'package:video_player/video_player.dart';

import 'package:Getinshoot/main.dart';

class GuidelinesPage extends StatefulWidget {
  const GuidelinesPage({Key key}) : super(key: key);

  @override
  _GuidelinesPageState createState() => _GuidelinesPageState();
}

class _GuidelinesPageState extends State<GuidelinesPage> {

  final List<String> _videos = const ["Be-steady.mp4","Clean-it.mp4","Focus-on-the-quality.mp4","reminder.mp4","Short-video.mp4","Straight-landscape.mp4","Straight-portrait.mp4","Various-shot.mp4"];
  final List<String> _titles = currentLocale=="fr" ? const ["stabilité","nettoyez","qualité","rechargez","concis","horizontal", "vertical", "variété"] : const ["be steady","clean it","focus on the quality","recharge your phone","short videos","straight landscape", "straight portrait", "various shot"];
  final List<String> _subtitles = currentLocale=="fr" ? const ["Soyez le plus stable possible, privilégiez les mouvements lents et fluides",
  "Nettoyez régulièrement les objectifs de votre smartphone", "Soyez vigilants, ne mettez pas le doigt devant l’objectif et réglez votre appareil en HD/1080p ou 4k",
  "Rechargez complètement la batterie de votre téléphone et videz tant que possible sa mémoire",
  "Filmer de courtes séquences (entre 5 et 30 secondes), juste les meilleurs moments !",
  "Filmez le plus droit possible en mode paysage", "Filmez le plus droit possible en mode portrait",
  "Varier les angles et les valeurs des plans : de loin, de près, de profil, de face, etc",] : const ["Be the most steady, focus on smooth and slow movements",
    "Clean often the lenses of your smartphone", "Keep your fingers out of the lens and set preferably the quality to HD/1080p or 4k",
    "and empty its memory much as possible (filming consumes a lot of battery and storage space)",
    "Shoot short sequences (between 5 and 30 seconds) just the best moments",
    "Shoot straight as possible in landscape mode", "Shoot straight as possible in portrait mode",
    "Vary the angle and values of shots : shoot far, near, profile, front, etc",];

  List<VideoPlayerController> _controllers = [];

  Future<void> _play(int videoIndex) async {
    try {
      _controllers[videoIndex] = VideoPlayerController.asset('assets/guidelines/videos/${_videos[videoIndex]}');
      await _controllers[videoIndex].initialize();
      await _controllers[videoIndex].setVolume(0);
      await _controllers[videoIndex].setLooping(true);
      await _controllers[videoIndex].play();

      for (int i = 0; i < _videos.length; i++) {
        if (i != videoIndex && _controllers[i] != null) {
          await _controllers[i].pause();
          Future.delayed(Duration(milliseconds: 100)).then((_) async {
            await _controllers[i]?.dispose();
            _controllers[i] = null;
          });
        }
      }
    } catch (ex) {}
  }

  @override
  void initState() {
    for (int i = 0; i < _videos.length; i++) {
      _controllers.add(null);
    }

    _play(0).then((_) {
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
              appBar: HeaderWidget.get(context),
              //drawer: MenuWidget.get(context),
              body: Center(
                child: Container(
                  child: Swiper(
                    itemBuilder: (BuildContext context, int index) {
                      return Guideline(
                        video: _videos[index],
                        image: _videos[index].replaceAll('.mp4', '.jpg'),
                        title: _titles[index],
                        subtitle: _subtitles[index],
                        controller: _controllers[index],
                      );
                    },
                    itemCount: _videos.length,
                    viewportFraction: 0.6,
                    scale: 0.8,
                    onIndexChanged: (index) {
                      _play(index).then((_) {
                        setState(() {});
                      });
                    },
                    scrollDirection: Axis.vertical,
                  ),
                ),
              ),
            ))
    );
  }

  @override
  void dispose() {
    for (int i = 0; i < _videos.length; i++) {
      _controllers[i]?.dispose();
    }
    super.dispose();
  }
}