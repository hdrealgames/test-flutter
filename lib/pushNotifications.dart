import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';

Future<void> myBackgroundMessageHandler(Map<String, dynamic> message) async {
  print("_backgroundMessageHandler");
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
    print("_backgroundMessageHandler data: $data");
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
    print("_backgroundMessageHandler notification: $notification");
  }
}

Future<void> saveDeviceToken(String userId, fMessaging ) async {
  fMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        //  _showItemDialog(message);
      },
      onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        //  _navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        //  _navigateToItemDetail(message);
      },
    );
  // Get the token for this device
  String fcmToken = await fMessaging.getToken();

  print("Device token: " + fcmToken);

  var token = await Firestore.instance.collection('tokens').document(userId).get();

  var tokenAlreadySaved = fcmToken != null && token.exists && token.data['token'] == fcmToken;
  // Save it to Firestore
  if (!tokenAlreadySaved) {
    print("Adding token to DB...");
    await Firestore.instance.collection('tokens').document(userId).setData({
      'token': fcmToken,
      'createdAt': FieldValue.serverTimestamp(),
      'platform': Platform.operatingSystem
    });
  }
}
