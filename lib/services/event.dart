import 'package:cloud_firestore/cloud_firestore.dart';

import '../main.dart';

Stream<QuerySnapshot> listenToInvitation(userId, eventId) {
  return Firestore.instance
      .collection('invitations')
      .where('userid', isEqualTo: userId)
      .where("eventid", isEqualTo: eventId)
      .snapshots();
}

Future joinEvent(userId, inviteCode) async {
  // Check if the code
  var eventCode = await Firestore.instance
      .collection('eventcodes')
      .document(inviteCode)
      .get();
  if (eventCode.exists) {
    var eventId = eventCode['eventid'];

    // Ask to join the event
    var dataRequest = {
      'userid': userId,
      'eventid': eventId,
      'codeinvite': inviteCode,
      'created': Timestamp.now(),
    };

    try {
      await Firestore.instance.collection('eventcoderequests').add(dataRequest);
      try {
        analytics.logEvent(
          name: 'join_project',
          parameters: <String, dynamic>{
            'eventid': eventId,
            'codeinvite': inviteCode,
          },
        );
      } catch (e) {}
      return Firestore.instance.collection('events').document(eventId).get();
    } catch (e) {}

    return null;
  }
}
