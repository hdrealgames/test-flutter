import 'package:flutter/material.dart';
import 'package:Getinshoot/pages/settings/settingsPage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import '../main.dart';

class ShooterzInfo extends StatefulWidget {
  const ShooterzInfo({
    Key key,
  }) : super(key: key);

  @override
  _ShooterzInfoState createState() {
    var shooterzInfoState = _ShooterzInfoState();
    return shooterzInfoState;
  }
}

class _ShooterzInfoState extends State<ShooterzInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
            margin: EdgeInsets.only(top: 20, right: 10),
            width: 88,
            height: 23,
            child: IconButton(
              icon: Icon(MdiIcons.settingsOutline, color: AppColor.grey),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) {
                    return SettingsPage();
                  }),
                );
              },
            )),
        Center(
          child: Container(
            width: 86,
            height: 86,
            child: Image.asset(
              "assets/images/avatar.png",
              fit: BoxFit.none,
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            child: Text(
              "camdudu durand",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color.fromARGB(255, 255, 255, 255),
                fontFamily: "Poppins",
                fontWeight: FontWeight.normal,
                fontSize: 20,
              ),
            ),
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.only(top: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 100,
                  height: 50,
                  margin: EdgeInsets.only(top: 8),
                  child: Stack(
                    alignment: Alignment.topCenter,
                    children: [
                      Center(
                        child: Container(
                          width: 88,
                          height: 50,
                          margin: EdgeInsets.only(top: 1),
                          child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              Positioned(
                                top: 0,
                                child: Container(
                                  width: 88,
                                  height: 23,
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 234, 196, 53),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(8)),
                                  ),
                                  child: Container(),
                                ),
                              ),
                              Positioned(
                                top: 3,
                                child: Text(
                                  "Discovery",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: AppColor.blue,
                                    fontFamily: "Muli",
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 30),
                        width: 100,
                        height: 40,
                        child: Text(
                          "Jusqu'à 10 Shooterz",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColor.grey,
                            fontFamily: "Muli",
                            fontWeight: FontWeight.w400,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Center(
                  child: Container(
                    width: 200,
                    height: 55,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(
                          top: 0,
                          child: Container(
                            width: 200,
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                              color: Color.fromRGBO(255, 255, 255, 0.1),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 120,
                                  height: 55,
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 50,
                                        height: 44,
                                        child: Stack(
                                          alignment: Alignment.topCenter,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              child: Text(
                                                "1.5M",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: AppColor.grey,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: Opacity(
                                                opacity: 0.6,
                                                child: Text(
                                                  "Shoots",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    color: AppColor.grey,
                                                    fontFamily: "Muli",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Container(
                                        width: 50,
                                        height: 44,
                                        child: Stack(
                                          alignment: Alignment.topCenter,
                                          children: [
                                            Positioned(
                                              left: 0,
                                              top: 0,
                                              right: 0,
                                              child: Text(
                                                "24",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: AppColor.grey,
                                                  fontFamily: "Poppins",
                                                  fontWeight: FontWeight.w700,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ),
                                            Positioned(
                                              bottom: 0,
                                              child: Opacity(
                                                opacity: 0.6,
                                                child: Text(
                                                  "Projects",
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    color: AppColor.grey,
                                                    fontFamily: "Muli",
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        SizedBox(
          width: double.infinity,
          height: 3,
          child: Container(
            color: Color.fromRGBO(255, 255, 255, 0.3),
          ),
        )
      ],
    );
  }
}
