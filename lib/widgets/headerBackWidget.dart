import 'package:flutter/material.dart';
import 'package:Getinshoot/utils/app_colors.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HeaderBackWidget {
  static get(BuildContext context){
    return AppBar(
      backgroundColor: AppColor.blue,
      centerTitle: true,
      title: Padding(
          padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 12.0, bottom: 12.0),
          child: Image.asset('assets/logo/logo.png', width: 170,)
      ),
      iconTheme: new IconThemeData(color: AppColor.green),
    );
  }
}
