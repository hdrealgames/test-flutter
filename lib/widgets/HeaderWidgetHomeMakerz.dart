import 'package:Getinshoot/pages/notificationHistoryPage.dart';

/// Packages
import 'package:flutter/material.dart';

/// Colors
import 'package:Getinshoot/utils/app_colors.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

/// Pages / widgets
import 'package:Getinshoot/pages/home/homePage.dart';

class HeaderWidgetHomeMakerz {
  static get(BuildContext context, Function notificationClickCallback,
      {bool unreadNotifications = false}) {
    return AppBar(
      backgroundColor: Colors.white,
      centerTitle: true,
      title: Padding(
          padding:
              EdgeInsets.only(left: 5.0, right: 5.0, top: 12.0, bottom: 12.0),
          child: Image.asset(
            'assets/logo/logo_dark.png',
            width: 170,
          )),
      leading: IconButton(
        icon: unreadNotifications
            ? Stack(
                children: <Widget>[
                  Icon(MdiIcons.accountCircle, color: AppColor.grey),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                        width: 10,
                        height: 10,
                        decoration: new BoxDecoration(
                            color: AppColor.red,
                            borderRadius:
                                new BorderRadius.all(Radius.circular(40)))),
                  )
                ],
              )
            : Icon(
                MdiIcons.accountCircle,
                color: AppColor.grey,
              ),
        onPressed: () {
          notificationClickCallback();
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) {
              return NotificationHistoryPage();
            }),
          );
        },
      ),
      actions: [
        IconButton(
          padding: EdgeInsets.all(5.0),
          icon: Image.asset('assets/icons/switch-makerz.png'),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) {
                return HomePage();
              }),
            );
          },
        ),
      ],
    );
  }
}
