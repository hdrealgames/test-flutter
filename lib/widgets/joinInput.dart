import 'package:Getinshoot/pages/events/eventDetailPage.dart';
import 'package:Getinshoot/services/event.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../main.dart';

class JoinInput extends StatefulWidget {
  const JoinInput({
    Key key,
  }) : super(key: key);

  @override
  _JoinInputState createState() => _JoinInputState();
}

class _JoinInputState extends State<JoinInput> {
  String codeInput;
  String error;
  bool loading = false;
  ProgressDialog pr;

  @override
  void initState() {
    super.initState();
    initDynamicLinks();
  }

  void initDynamicLinks() async {
    final PendingDynamicLinkData data =
        await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null &&
        previousDeepLinkCode != deepLink.queryParameters['invite-code']) {
      print("Found a deep link");
      print("Invite code: ${deepLink.queryParameters['invite-code']}");
      previousDeepLinkCode = deepLink.queryParameters['invite-code'];
      setState(() {
        codeInput = deepLink.queryParameters['invite-code'];
      });
      await submitHandler();
    }

    FirebaseDynamicLinks.instance.onLink(
        onSuccess: (PendingDynamicLinkData dynamicLink) async {
      final Uri deepLink = dynamicLink?.link;

      if (deepLink != null &&
          previousDeepLinkCode != deepLink.queryParameters['invite-code']) {
        print("Found a deep link");
        print("Invite code: ${deepLink.queryParameters['invite-code']}");
        previousDeepLinkCode = deepLink.queryParameters['invite-code'];
        setState(() {
          codeInput = deepLink.queryParameters['invite-code'];
        });
        await submitHandler();
      }
    }, onError: (OnLinkErrorException e) async {
      print('onLinkError');
      print(e.message);
    });
  }

  submitHandler() async {
    setState(() {
      loading = true;
    });
    pr.style(
        message: currentLocale == "fr"
            ? "Validation du getin-code..."
            : "Validating the getin-code...",
        backgroundColor: AppColor.red,
        borderRadius: 15,
        messageTextStyle: TextStyle(color: Colors.white, fontFamily: "Muli"),
        progressWidget: CircularProgressIndicator());
    await pr.show();
    if (codeInput == null || codeInput.length != 6) {
      setState(() {
        error = currentLocale == "fr"
            ? "getin-code invalide"
            : "Invalid getin-code";
        loading = false;
      });
      Toast.show(error, context, gravity: Toast.TOP);
      await pr.hide();
    } else {
      var event = await joinEvent(repositoryUser.userId, codeInput);
      if (event == null) {
        error = currentLocale == "fr"
            ? "getin-code invalide"
            : "Invalid getin-code";
        Toast.show(error, context, gravity: Toast.TOP);
        setState(() {
          loading = false;
        });
        await pr.hide();
      } else {
        listenToInvitation(repositoryUser.userId, event.documentID)
            .listen((data) async {
          if (data.documents.length > 0) {
            setState(() {
              loading = false;
            });
            await pr.hide();
            var invitation = data.documents[0];
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return EventDetailPage(
                document: event,
                documentId: event.documentID,
                name: event.data['name'] ?? '',
                host: event.data['host'] ?? '',
                guests: event.data['guests'] ?? 0,
                image: event.data['image'] ?? null,
                imageURL: event.data['imageURL'] ?? null,
                thumbnailURL: event.data['imageURL'] ?? null,
                joined: invitation.data['joined'] ?? false,
                participated: invitation.data['participated'] ?? false,
                invitationId: invitation.documentID ?? null,
                mediaOrientation: event.data['mediaorientation'] ?? 'both',
                mediaDuration: event.data['mediaduration'] ?? 0,
                mediaByGuest: event.data['mediabyguest'] ?? 0,
              );
            }));
          }
        });
      }
    }
  }

  onTextInputChange(value) {
    print(currentLocale);
    codeInput = value;
  }

  @override
  Widget build(BuildContext context) {
    pr = new ProgressDialog(context);
    return Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Center(
            child: Text(
          currentLocale == "fr" ? "rejoindre" : "join",
          style: TextStyle(
              color: Colors.white, fontFamily: "Poppins", fontSize: 24),
        )),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 200,
              child: TextFormField(
                onChanged: (value) {
                  onTextInputChange(value);
                },
                onFieldSubmitted: (value) async {
                  await submitHandler();
                },
                keyboardType: TextInputType.number,
                style: TextStyle(color: AppColor.red),
                inputFormatters: [
                  LengthLimitingTextInputFormatter(6),
                ],
                decoration: InputDecoration(
                  labelText: currentLocale == "fr"
                      ? "entrer le getin-code"
                      : "type the getin-code",
                  labelStyle: TextStyle(
                      color: Color.fromRGBO(255, 255, 255, 0.3),
                      fontFamily: "Muli",
                      fontSize: 15),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: AppColor.red, width: 0.0),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(15),
                          bottomLeft: Radius.circular(15))),
                  enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color.fromRGBO(255, 255, 255, 0.1),
                          width: 0.0),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(15),
                          bottomLeft: Radius.circular(15))),
                  fillColor: Color.fromRGBO(255, 255, 255, 0.1),
                  filled: true,
                  hintStyle: TextStyle(color: AppColor.grey),
                ),
              ),
            ),
            SizedBox(
              width: 60,
              height: 60,
              child: RaisedButton(
                color: AppColor.red,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15),
                        bottomRight: Radius.circular(15))),
                child: loading
                    ? SizedBox(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator())
                    : Text(
                        "GO",
                        style: TextStyle(
                            color: Colors.white, fontFamily: "Poppins"),
                      ),
                onPressed: () async {
                  await submitHandler();
                },
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        SizedBox(
          width: double.infinity,
          height: 3,
          child: Container(
            color: Color.fromRGBO(255, 255, 255, 0.3),
          ),
        )
      ],
    );
  }
}

void onTextInputChange(String s, String value) {}
