import 'package:flutter/material.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:Getinshoot/pages/home/homePage.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:Getinshoot/pages/notificationHistoryPage.dart';
//import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
//import 'package:fonrepositoryUser.userIdt_awesome_flutter/font_awesome_flutter.dart';

class HeaderWidget {
  static get(BuildContext context) {
    return AppBar(
      backgroundColor: AppColor.blue,
      centerTitle: true,
      title: Padding(
          padding:
              EdgeInsets.only(left: 5.0, right: 5.0, top: 12.0, bottom: 12.0),
          child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) {
                    return HomePage();
                  }),
                );
              },
              child: Image.asset(
                'assets/logo/logo.png',
                width: 170,
              ))),
      leading: new Container(
          child: IconButton(
        icon: Icon(
          MdiIcons.accountCircle,
          color: AppColor.grey,
        ),
        onPressed: () {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) {
              return NotificationHistoryPage();
            }),
          );
        },
      )),
      iconTheme: new IconThemeData(color: AppColor.green),
      actions: <Widget>[
        /*IconButton(
          icon: Icon(FontAwesomeIcons.bars, color: AppColor.green,),
          onPressed: () {

          },
        )*/
      ],
    );
  }
}
