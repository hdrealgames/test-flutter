import 'package:flutter/material.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';

import 'package:Getinshoot/pages/home/homePage.dart';
//import 'package:Getinshoot/pages/profile/profilePage.dart';
import 'package:Getinshoot/pages/guidelines/guidelinesPage.dart';

import 'package:Getinshoot/main.dart';

class MenuWidget {
  static get(BuildContext context) {
    return Drawer(
      child: Stack(children: <Widget>[
        Container(
          decoration: BoxDecoration(color: AppColor.green),
        ),
        Container(
          height: 110,
          decoration: BoxDecoration(color: AppColor.blue),
        ),
        Container(
          decoration: BoxDecoration(color: AppColor.blue),
          margin: new EdgeInsets.only(left: 25),
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              new SizedBox(
                height: 110.0,
                child: DrawerHeader(
                    decoration: BoxDecoration(color: AppColor.blue),
                    child: Row(children: <Widget>[
                      Expanded(
                          flex: 10,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  left: 0.0,
                                  top: 0.0,
                                  bottom: 1.0,
                                  right: 10.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/logo/logo.png'),
                                        fit: BoxFit.contain)),
                              ))),
                      Expanded(
                          flex: 3,
                          child: Container(
                              child: IconButton(
                            iconSize: 22.0,
                            icon: Icon(Icons.clear, color: AppColor.green),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ))),
                    ])),
              ),
              ListTile(
                title: Padding(
                    padding: EdgeInsets.only(left: 10, right: 2),
                    child: Text(
                      currentLocale == "fr" ? 'projets' : 'projects',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 26,
                          fontFamily: 'Muli',
                          fontWeight: FontWeight.bold),
                    )),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) {
                      return HomePage();
                    }),
                  );
                },
              ),
              ListTile(title: Text("")),
              /**
                    ListTile(
                    title: Padding(
                    padding: EdgeInsets.only(left: 10, right: 2),
                    child: Opacity(opacity:0.2,child:Text('profile', style: TextStyle(color: Colors.white, fontSize: 28, fontFamily: 'Muli', fontWeight: FontWeight.bold),)),
                    ),
                    onTap: () {
                    return ProfilePage();
                    },
                    ),
                 **/
              ListTile(
                title: Padding(
                  padding: EdgeInsets.only(left: 10, right: 2),
                  child: Text(
                    currentLocale == "fr" ? 'astuces' : 'guidelines',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 26,
                        fontFamily: 'Muli',
                        fontWeight: FontWeight.bold),
                  ),
                ),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) {
                      return GuidelinesPage();
                    }),
                  );
                },
              ),
              ListTile(
                title: Padding(
                  padding: EdgeInsets.only(left: 10, right: 2),
                  child: Text(
                    currentLocale == "fr" ? 'déconnexion' : 'logout',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 26,
                        fontFamily: 'Muli',
                        fontWeight: FontWeight.bold),
                  ),
                ),
                onTap: () {
                  BlocProvider.of<AuthenticationBloc>(context).dispatch(
                    LoggedOut(),
                  );
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
