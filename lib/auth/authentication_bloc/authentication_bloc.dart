import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserRepository _userRepository;

  AuthenticationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  AuthenticationState get initialState => Uninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LoggedIn) {
      yield* _mapLoggedInToState();
    } else if (event is LoggedOut) {
      yield* _mapLoggedOutToState();
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    try {
      final isSignedIn = await _userRepository.isSignedIn();
      if (isSignedIn) {
        final name = await _userRepository.getUser();
        final userId = await _userRepository.getUserId();
        final email = await _userRepository.getUserEmail();
        _userRepository.userId = userId;
        _userRepository.name = name;
        _userRepository.email = email;
        yield Authenticated(name,userId);
      } else {
        yield Unauthenticated();
      }
    } catch (_) {
      yield Unauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedInToState() async* {
    final name = await _userRepository.getUser();
    final email = await _userRepository.getUserEmail();
    final userId = await _userRepository.getUserId();
    _userRepository.userId = userId;
    _userRepository.name = name;
    _userRepository.email = email;

    yield Authenticated(name, userId);
  }

  Stream<AuthenticationState> _mapLoggedOutToState() async* {
    yield Unauthenticated();
    _userRepository.signOut();
    _userRepository.userId = null;
    _userRepository.name = null;
    _userRepository.email = null;
  }
}
