import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// BLoc
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';

/// Colors
import 'package:Getinshoot/utils/app_colors.dart';

/// Pages / Widgets
import 'package:Getinshoot/main.dart';
import 'package:Getinshoot/auth/login/login.dart';


class ResetPasswordForm extends StatefulWidget {
  final UserRepository _userRepository;

  ResetPasswordForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  State<ResetPasswordForm> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPasswordForm> {
  final TextEditingController _emailController = TextEditingController();

  LoginBloc _loginBloc;
  bool isSubmitting = false;
  bool isFormValid = true;
  bool isEmailValid = true;

  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated => _emailController.text.isNotEmpty;

  bool isResetButtonEnabled() {
    return isFormValid && isPopulated && isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: EdgeInsets.all(20.0),
            child: Form(
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.only(top: 20,bottom: 10),
                          child: Text(currentLocale=="fr" ? "Mot de passe perdu" : "Reset password",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: AppColor.red, fontFamily: "Muli", fontSize: 26),
                          )
                      )
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  TextFormField(
                    style: new TextStyle(color: Colors.white),
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email, color: AppColor.grey),
                      labelText: 'Email',
                      labelStyle: TextStyle(color: AppColor.grey),
                      hintStyle: TextStyle(color: AppColor.grey),
                      focusedBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                    ),
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return isEmailValid ? currentLocale=="fr" ? 'Email invalide' : 'Invalid Email' : null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        LoginButton(
                          onPressed: isResetButtonEnabled()
                              ? _onFormSubmitted : null,
                        ),
                        //GoogleLoginButton(),
                        //CreateAccountButton(userRepository: _userRepository),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Opacity(
                      opacity: 0.7,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: CreateAccountButton(userRepository: _userRepository),
                      )
                  ),
                  /*Opacity(
                      opacity: 0.7,
                      child: Padding(
                        padding: EdgeInsets.only(top: 0, bottom: 10),
                        child: LostPasswordButton(userRepository: _userRepository),
                      )
                  ),*/
                ],
              ),
            ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.dispatch(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onFormSubmitted() {
    // TODO
  }
}
