import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// BLoc
import 'package:Getinshoot/auth/user_repository.dart';

/// Pages
import 'package:Getinshoot/main.dart';
//import 'package:Getinshoot/auth/lost/resetpassword_screen.dart';

/// Launch browser
import 'package:url_launcher/url_launcher.dart';


class ResetPasswordButton extends StatelessWidget {
  //final UserRepository _userRepository;

  ResetPasswordButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        //_userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: RichText(
          text: TextSpan(
              style: TextStyle(color: Colors.white),
              children: [
                TextSpan(text: currentLocale=="fr" ? "Mot de passe perdu" : "Reset your password", style: TextStyle(decoration: TextDecoration.underline))
              ]
          )
      ),
      onPressed: () async {
        const url = "https://admin.getinshoot.com/reminder";
        // Open Browser
        if (await canLaunch(url)) {
        await launch(url);
        } else {
        throw 'Could not launch $url';
        }
      },
    );
  }
}
