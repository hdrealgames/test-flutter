import 'package:flutter/material.dart';

/// Bloc
//import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';

/// Colors
import 'package:Getinshoot/utils/app_colors.dart';

/// Menu
import 'package:Getinshoot/widgets/headerBackWidget.dart';

/// Reset password
import 'package:Getinshoot/auth/lost/resetpassword_form.dart';

class ResetPasswordScreen extends StatelessWidget {
  final UserRepository _userRepository;

  ResetPasswordScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderBackWidget.get(context),
                body: Container(
                  decoration: BoxDecoration(color: AppColor.blue),
                  child: Center(
                    child: ResetPasswordForm(userRepository: _userRepository),
                  ),
                )
            )
        )
    );
  }
}
