import 'package:flutter/material.dart';
//import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:Getinshoot/auth/login/login.dart';
import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/login/loginEmailPage.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/main.dart';

class EmailLoginButton extends StatelessWidget {

  final UserRepository _userRepository;

  EmailLoginButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    //double width = MediaQuery.of(context).size.width;

    return RaisedButton(
      child: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Text(currentLocale=="fr" ? 'Connexion Email'.toUpperCase() : 'Sign in with Email'.toUpperCase(), style: TextStyle(color: Colors.white))
        )
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      onPressed: () {
        /*BlocProvider.of<LoginBloc>(context).dispatch(
          LoginWithGooglePressed(),
        );*/
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return LoginEmailPage(userRepository: _userRepository);
          }),
        );
      },
      color: AppColor.grey,
    );
  }
}
