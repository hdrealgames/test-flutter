import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// BLoc
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';
import 'package:Getinshoot/auth/login/login.dart';

/// Colours
import 'package:Getinshoot/utils/app_colors.dart';

/// Widgets / Pages
import 'package:Getinshoot/auth/lost/resetpassword_button.dart';
import 'package:Getinshoot/main.dart';


class LoginForm extends StatefulWidget {
  final UserRepository _userRepository;

  LoginForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _loginBloc,
      listener: (BuildContext context, LoginState state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(currentLocale=="fr" ? 'Echec de connexion' : 'Login Failure'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(currentLocale=="fr" ? 'Connexion en cours ...' : 'Logging In...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).dispatch(LoggedIn());
          Navigator.of(context).pop();
        }
      },
      child: BlocBuilder(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          return Padding(
            padding: EdgeInsets.all(20.0),
            child: Form(
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    style: new TextStyle(color: Colors.white),
                    keyboardType: TextInputType.emailAddress,
                    controller: _emailController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email, color: AppColor.grey),
                      labelText: 'Email',
                      labelStyle: TextStyle(color: AppColor.grey),
                      hintStyle: TextStyle(color: AppColor.grey),
                      focusedBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                    ),
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isEmailValid ? currentLocale=="fr" ? 'Email invalide' : 'Invalid Email' : null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    style: new TextStyle(color: Colors.white),
                    controller: _passwordController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock, color: AppColor.grey),
                      labelText: currentLocale=="fr" ? 'Mot de passe' : 'Password',
                      labelStyle: TextStyle(color: AppColor.grey),
                      hintStyle: TextStyle(color: AppColor.grey),
                      focusedBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Colors.white)
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                      enabledBorder: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: AppColor.grey.withOpacity(0.8))
                      ),
                    ),
                    obscureText: true,
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isPasswordValid ? currentLocale=="fr" ?  'Mot de passe invalide. 6 caractères minimum' : 'Invalid Password. At least 6 caracters' : null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        LoginButton(
                          onPressed: isLoginButtonEnabled(state)
                              ? _onFormSubmitted
                              : null,
                        ),
                        //GoogleLoginButton(),
                        //CreateAccountButton(userRepository: _userRepository),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Opacity(
                    opacity: 0.7,
                    child: Padding(
                      padding: EdgeInsets.only(top: 11, bottom: 1),
                      child: CreateAccountButton(userRepository: _userRepository),
                    )
                  ),
                  Opacity(
                      opacity: 0.7,
                      child: Padding(
                        padding: EdgeInsets.only(top: 4, bottom: 4),
                        child: ResetPasswordButton(userRepository: _userRepository),
                      )
                  ),
                  /*Opacity(
                      opacity: 0.7,
                      child: Padding(
                        padding: EdgeInsets.only(top: 0, bottom: 10),
                        child: LostPasswordButton(userRepository: _userRepository),
                      )
                  ),*/
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.dispatch(
      EmailChanged(email: _emailController.text),
    );
  }

  void _onPasswordChanged() {
    _loginBloc.dispatch(
      PasswordChanged(password: _passwordController.text),
    );
  }

  void _onFormSubmitted() {
    _loginBloc.dispatch(
      LoginWithCredentialsPressed(
        email: _emailController.text,
        password: _passwordController.text,
      ),
    );
  }
}
