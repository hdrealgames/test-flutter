import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/register/register.dart';

import 'package:Getinshoot/main.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: RichText(
        text: TextSpan(
          text: currentLocale=="fr" ? "Pas encore de compte ? " : "Don\'t have an account ? ",
          style: TextStyle(color: Colors.white),
          children: [
            TextSpan(text: currentLocale=="fr" ? "Rejoignez-nous !" : "Sign up", style: TextStyle(decoration: TextDecoration.underline))
          ]
        )
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) {
            return RegisterScreen(userRepository: _userRepository);
          }),
        );
      },
    );
  }
}
