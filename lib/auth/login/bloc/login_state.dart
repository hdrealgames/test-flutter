import 'package:meta/meta.dart';

@immutable
class LoginState {
  final bool isEmailValid;
  final bool isPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isConnecting;
  final bool isGoogleChecking;
  final bool isFacebookChecking;
  final bool isLinkedInChecking;
  final bool isEmailAlreadyUsed;

  bool get isFormValid => isEmailValid && isPasswordValid;

  LoginState(
      {@required this.isEmailValid,
      @required this.isPasswordValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      @required this.isConnecting,
      @required this.isGoogleChecking,
      @required this.isFacebookChecking,
      @required this.isLinkedInChecking,
      @required this.isEmailAlreadyUsed});

  factory LoginState.empty() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.loading() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: true,
      isSuccess: false,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.failure() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: true,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.emailAlreadyUsed() {
    return LoginState(
        isEmailValid: true,
        isPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        isConnecting: false,
        isGoogleChecking: false,
        isFacebookChecking: false,
        isLinkedInChecking: false,
        isEmailAlreadyUsed: true);
  }

  factory LoginState.success() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: true,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.checkGoogle() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: true,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.checkFacebook() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: true,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.submitGoogle() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: true,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.submitFacebook() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: true,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  factory LoginState.checkLinkedIn() {
    return LoginState(
      isEmailValid: true,
      isPasswordValid: true,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: true,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
      isEmailAlreadyUsed: false,
    );
  }

  LoginState update({
    bool isEmailValid,
    bool isPasswordValid,
  }) {
    return copyWith(
      isEmailValid: isEmailValid,
      isPasswordValid: isPasswordValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      isConnecting: false,
      isGoogleChecking: false,
      isFacebookChecking: false,
      isLinkedInChecking: false,
    );
  }

  LoginState copyWith({
    bool isEmailValid,
    bool isPasswordValid,
    bool isSubmitEnabled,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    bool isConnecting,
    bool isGoogleChecking,
    bool isFacebookChecking,
    bool isLinkedInChecking,
  }) {
    return LoginState(
      isEmailValid: isEmailValid ?? this.isEmailValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      isConnecting: isConnecting ?? this.isConnecting,
      isGoogleChecking: isGoogleChecking ?? this.isGoogleChecking,
      isFacebookChecking: isFacebookChecking ?? this.isFacebookChecking,
      isLinkedInChecking: isLinkedInChecking ?? this.isLinkedInChecking,
      isEmailAlreadyUsed: isEmailAlreadyUsed ?? this.isEmailAlreadyUsed,
    );
  }

  @override
  String toString() {
    return '''LoginState {
      isEmailValid: $isEmailValid,
      isPasswordValid: $isPasswordValid,      
      isSubmitting: $isSubmitting,
      isSuccess: $isSuccess,
      isFailure: $isFailure,
      isConnecting: $isConnecting,
      isGoogleChecking: $isGoogleChecking,
      isFacebookChecking: $isFacebookChecking,
      isLinkedChecking: $isLinkedInChecking,
      isEmailAlreadyUsed: $isEmailAlreadyUsed
    }''';
  }
}
