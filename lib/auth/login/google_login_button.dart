import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/login/login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/main.dart';

class GoogleLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      icon: Icon(FontAwesomeIcons.google, color: Colors.black54),
      onPressed: () {
        BlocProvider.of<LoginBloc>(context).dispatch(
          LoginWithGooglePressed(),
        );
      },
      label: Text(currentLocale=="fr" ? 'Connexion Google'.toUpperCase() : 'Sign in with Google'.toUpperCase(), style: TextStyle(color: AppColor.blue)),
      color: Colors.white,
    );
  }
}
