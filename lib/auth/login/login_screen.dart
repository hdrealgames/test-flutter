import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/login/login.dart';
import 'package:Getinshoot/utils/app_colors.dart';

class LoginScreen extends StatelessWidget {
  final UserRepository _userRepository;

  LoginScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(title: Text('Login')),
      body: BlocProvider<LoginBloc>(
        builder: (context) => LoginBloc(userRepository: _userRepository),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage('assets/images/open.jpg')
            )
          ),
          child: Container(
            decoration: BoxDecoration(
                color: AppColor.blue.withOpacity(0.4)
            ),
            child: SafeArea(
              child: Center(
                  child:LoginSelect(userRepository: _userRepository)
              )
            )
          )
        )
      ),
    );
  }
}
