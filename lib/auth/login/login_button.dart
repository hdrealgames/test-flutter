import 'package:flutter/material.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/main.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback _onPressed;

  LoginButton({Key key, VoidCallback onPressed})
      : _onPressed = onPressed,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: RaisedButton(
        color: AppColor.green,
        textColor: AppColor.blue,
        disabledColor: AppColor.green.withOpacity(0.3),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        onPressed: _onPressed,
        child: Text(currentLocale=="fr" ? 'Connexion' : 'Login'),
      )
    );
  }
}
