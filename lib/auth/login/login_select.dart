import 'package:Getinshoot/auth/login/facebook_login_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/authentication_bloc/bloc.dart';
import 'package:Getinshoot/auth/login/login.dart';

import 'package:Getinshoot/main.dart';

class LoginSelect extends StatefulWidget {
  final UserRepository _userRepository;

  LoginSelect({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  State<LoginSelect> createState() => _LoginSelectState();
}

class _LoginSelectState extends State<LoginSelect> {
  LoginBloc _loginBloc;

  UserRepository get _userRepository => widget._userRepository;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _loginBloc,
      listener: (BuildContext context, LoginState state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(currentLocale=="fr" ? 'Echec de la connexion': 'Login Failure'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isEmailAlreadyUsed) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [Text(currentLocale=="fr" ? 'Vous avez déjà un compte associé à cette email': 'You already have an account linked to this email'), Icon(Icons.error)],
                ),
                backgroundColor: Colors.red,
              ),
            );
        }
        if (state.isGoogleChecking) {
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                duration: Duration(seconds: 15),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(currentLocale=="fr" ? 'Connexion à Google...' : 'Connecting to Google...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isFacebookChecking){
          Scaffold.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                duration: Duration(seconds: 15),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(currentLocale=="fr" ? 'Connexion à Facebook...' : 'Connecting to Facebook...'),
                    CircularProgressIndicator(),
                  ],
                ),
              ),
            );
        }
        if (state.isSubmitting) {
          Scaffold.of(context)..hideCurrentSnackBar()..showSnackBar(
            SnackBar(
              duration: Duration(seconds: 15),
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(currentLocale=="fr" ? 'Connexion en cours...' : 'Logging In...'),
                  CircularProgressIndicator(),
                ],
              ),
            ),
          );
        }
        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).dispatch(LoggedIn());
        }
      },
      child: BlocBuilder(
        bloc: _loginBloc,
        builder: (BuildContext context, LoginState state) {
          return Padding(
            padding: EdgeInsets.only(top: 10, left:20, right: 20, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container()
                      ),
                      Expanded(
                        flex: 7,
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                fit: BoxFit.contain,
                                image: AssetImage('assets/logo/logo.png')
                              )
                            ),
                          )
                        )
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                            //child: new CircularProgressIndicator()
                        )
                      )
                    ]
                  )
                ),
                Expanded(
                  flex: 2,
                  child: Center(
                    child: new Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: EmailLoginButton(userRepository: _userRepository)
                        ),
                        Padding(
                            padding: EdgeInsets.symmetric(vertical: 10),
                            child: Text(currentLocale=="fr" ? 'OU' : 'OR', style: TextStyle(color: Colors.white)),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          child: GoogleLoginButton(),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          child: FacebookLoginButton(),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          child: CreateAccountButton(userRepository: _userRepository),
                        )
                      ],
                    )
                  )
                ),
              ],
            )
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
