import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/login/login.dart';
import 'package:Getinshoot/utils/app_colors.dart';
import 'package:Getinshoot/widgets/headerBackWidget.dart';

class LoginEmailPage extends StatelessWidget {
  final UserRepository _userRepository;

  LoginEmailPage({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: AppColor.blue,
          image: DecorationImage(
            image: AssetImage('assets/images/GetHome.png'),
            fit: BoxFit.contain,
          )
        ),
        child: SafeArea(
          child: Scaffold(
            appBar: HeaderBackWidget.get(context),
            body: Container(
              decoration: BoxDecoration(color: AppColor.blue),
              child: Center(
                child: BlocProvider<LoginBloc>(
                  builder: (context) =>
                      LoginBloc(userRepository: _userRepository),
                  child: LoginForm(userRepository: _userRepository),
                ),
              ),
            )
          )
        )
    );
  }

}
