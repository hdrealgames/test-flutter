import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:Getinshoot/auth/user_repository.dart';
import 'package:Getinshoot/auth/register/register.dart';
import 'package:Getinshoot/utils/app_colors.dart';

import 'package:Getinshoot/widgets/headerBackWidget.dart';


class RegisterScreen extends StatelessWidget {
  final UserRepository _userRepository;

  RegisterScreen({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: AppColor.blue),
        child: SafeArea(
            child: Scaffold(
                appBar: HeaderBackWidget.get(context),
                body: Container(
                    decoration: BoxDecoration(color: AppColor.blue),
                    child: Center(
                      child: BlocProvider<RegisterBloc>(
                        builder: (context) => RegisterBloc(userRepository: _userRepository),
                        child: RegisterForm(),
                      ),
                    ),
                )
            )
        )
    );
  }
}
