import 'package:flutter/widgets.dart';

class AppColor {
  static const Color blue = const Color(0xff161032);
  static const Color green = const Color(0xff17BEBB);
  static const Color grey = const Color(0xffCBC8D7);
  static const Color red = const Color(0xffF9664C);
  static const Color yellow = const Color(0xffEAC435);
}
