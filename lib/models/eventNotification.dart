class EventNotification {
  String eventName, content, imageURL;
  DateTime createdDate;

  EventNotification(
      this.eventName, this.content, this.createdDate, this.imageURL);

  EventNotification.fromFirebase(
      Map<String, dynamic> data, String eventName, String imageURL)
      : eventName = eventName,
        content = data['content'],
        createdDate = data['createdDate'] != null
            ? DateTime.parse(data['createdDate'].toDate().toString())
            : new DateTime.utc(2020, 03, 21),
        imageURL = imageURL;

  @override
  String toString() {
    return '$eventName - $content - $createdDate - $imageURL';
  }
}
