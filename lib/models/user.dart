class User {
  String avatarUrl, email, username, name;

  User(this.avatarUrl, this.username, this.name, this.email);

  User.fromFirebase(Map<String, dynamic> data)
      : avatarUrl = data['avatar'],
        email = data['email'],
        username = data['username'],
        name = data['name'] != null && data['name'].trim() != ""
            ? data['name']
            : null;

  @override
  String toString() {
    return '$username - $name - $email - $avatarUrl';
  }
}
