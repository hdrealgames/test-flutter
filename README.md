# Getinshoot

## Prerequisites

1. [Install Flutter](https://flutter.dev/docs/get-started/install)
2. [Setup your editor](https://flutter.dev/docs/get-started/editor?tab=vscode)
3. Make sure to modify `./android/key.properties` with your key informations. You can learn more about how to build your key [here](https://flutter.dev/docs/deployment/android#create-a-keystore)
4. Install any Android or iOS SimuMulir or make sure you can plug your physical device on your machine

You can run `flutter doctor` to check if your installation is complete.

## Build & Launch project

---
Before building the android release, you need to fix the camera plugin manually, located in your .pub-cache folder. ([The current issue](https://github.com/flutter/flutter/issues/38787))
Modify the file `Camera.java` located in `<FLUTTER_DIRECTORY>/.pub-cache/hosted/pub.dartlang.org/camera-0.5.7+4/android/src/main/java/io/flutter/plugins/camera/`. In my computer for example: `Users/baptistearnaud/Library/.pub-cache/hosted/pub.dartlang.org/camera-0.5.7+4/android/src/main/java/io/flutter/plugins/camera/`.

Add this line: `if (enableAudio) mediaRecorder.setAudioEncodingBitRate(recordingProfile.audioBitRate);` like so :
```
if (enableAudio) mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
if (enableAudio) mediaRecorder.setAudioEncodingBitRate(recordingProfile.audioBitRate);
mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);	    mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
mediaRecorder.setOutputFormat(recordingProfile.fileFormat);	    mediaRecorder.setOutputFormat(recordingProfile.fileFormat);
```
---

On VS Code, you can simply hit F5. More info [here](https://flutter.dev/docs/get-started/test-drive?tab=vscode)
